**GDivAn**

**GDivAn** is an measurement based execution time and side-channel analysis tool for GPU kernels (CUDA programs) that uses symbolic execution(SE) and genetic algorithm(GA) to navigate the possible input space and direct the search toward the worst case execution time(WCET) and to detect the side-channel leakage capacity.

The provided code has two versions:

1. Version1 - WCET analysis
2. Version2 - WCET analysis and side-channel leakage analysis

---

## 1. Version1
The code provided for this version was used in the paper: "Horga, A., Chattopadhyay, S., Eles, P. and Peng, Z., 2018, August. Measurement based execution time analysis of GPGPU programs via SE+ GA. In 2018 21st Euromicro Conference on Digital System Design (DSD) (pp. 30-37). IEEE." [https://doi.org/10.1109/DSD.2018.00021](https://doi.org/10.1109/DSD.2018.00021).

---

## 2. Version2
The code from this version was used in the paper: "Horga, A., Chattopadhyay, S., Eles, P. and Peng, Z., 2019. Genetic Algorithm Based Estimation of Non–Functional Properties for GPGPU Programs. Journal of Systems Architecture, p.101697." [https://doi.org/10.1016/j.sysarc.2019.101697](https://doi.org/10.1016/j.sysarc.2019.101697)

The code provided for this version is the updated version of the one in Version1.
The **GKLEE** implementation from Version1 needs to be used for the *WCET_Analysis* part. We have avoided providing a duplicate of the *Gklee* folder in the *Version2* folder as well.

---

## Usage of GDivAn
Check each version on how to work with GDivAn.

---

### Bugs & issues

For reporting bugs or any other issues please contact [Adrian Horga](mailto:adrian.horga@liu.se).

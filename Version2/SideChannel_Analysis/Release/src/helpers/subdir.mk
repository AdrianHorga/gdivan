################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/helpers/DoubleLogger.cc \
../src/helpers/Statistics.cc \
../src/helpers/TimerHelper.cc 

CC_DEPS += \
./src/helpers/DoubleLogger.d \
./src/helpers/Statistics.d \
./src/helpers/TimerHelper.d 

OBJS += \
./src/helpers/DoubleLogger.o \
./src/helpers/Statistics.o \
./src/helpers/TimerHelper.o 


# Each subdirectory must supply rules for building sources it contributes
src/helpers/%.o: ../src/helpers/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++1y -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/dataClasses/EncryptionSetupSettings.cc \
../src/dataClasses/ProfilerMetrics.cc 

CC_DEPS += \
./src/dataClasses/EncryptionSetupSettings.d \
./src/dataClasses/ProfilerMetrics.d 

OBJS += \
./src/dataClasses/EncryptionSetupSettings.o \
./src/dataClasses/ProfilerMetrics.o 


# Each subdirectory must supply rules for building sources it contributes
src/dataClasses/%.o: ../src/dataClasses/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++1y -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



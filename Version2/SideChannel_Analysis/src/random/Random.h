
#ifndef RANDOM_H_
#define RANDOM_H_

#include <map>
#include <set>
#include <vector>
#include <string>
#include <fstream>

#include "../dataClasses/ProfilerMetrics.h"
#include "../helpers/DoubleLogger.h"
#include "../dataClasses/SetupSettings.h"
#include "../dataClasses/SetupSettings.h"

template <typename chromType = unsigned char>
std::vector<std::vector<chromType>> generateRandomList(int n, int size){
	std::vector<std::vector<chromType>> lists(n);
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < size; ++j) {
			chromType ch = rand() % 256;
			lists[i].push_back(ch);
		}
	}
	return lists;
}


template <typename chromType = char, typename fitType = double>
void quickRandom(int argc, char** argv,
		EncryptionSetupSettings *setupSettings,
		FITNESS_TEMPLATE fitnessFunction){


	ProfilerMetrics<fitType>* pm;
	std::map<int, std::set<fitType>> map;

	int randoms = 1000;
	if (argc > 2){
		randoms = std::atoi(argv[argc - 1]);
	}

	std::vector<std::vector<chromType>> lists = generateRandomList<chromType>(randoms, setupSettings->getKeyByteSize());
	int exec = 0;
	int index = 0;
	for (std::vector<chromType> key : lists){
		//GlobalLogger::getLogger() << "--------------------------------------- Execution " << exec << std::endl;
		fitType fitness = fitnessFunction(key, &pm, setupSettings, 0);
		GlobalLogger::getLogger().logLine(fitness);
		map[index].insert(fitness);
		delete pm;
		exec++;
	}
}

template <typename chromType = char, typename fitType = double>
void quickExhaustive(int argc, char** argv,
		EncryptionSetupSettings *setupSettings,
		FITNESS_TEMPLATE fitnessFunction){

	ProfilerMetrics<fitType>* pm;
	std::map<int, std::set<fitType>> map;

	int randoms = 7;

	//	std::vector<std::vector<chromType>> lists = generateRandoms<chromType>(randoms, settings->getInputSize());
	int exec = 0;
	int index = 0;
	for (unsigned int i = 0 ; i < setupSettings->getInputSize(); i++){
		std::vector<std::vector<chromType>> lists = generateRandomList<chromType>(randoms, setupSettings->getInputSize());
		for (int j = 0 ; j < 1 ; j++){
//			std::vector<std::vector<chromType>> lists = generateRandoms<chromType>(randoms, settings->getInputSize());
			for (std::vector<chromType> key : lists){
				//GlobalLogger::getLogger() << "------------------------------ Execution " << exec << std::endl;
				chromType c = j;
				key[i] = c;
				fitType fitness = fitnessFunction(key, &pm, setupSettings, 0);
				GlobalLogger::getLogger().logLine(fitness);
				map[index].insert(fitness);
				delete pm;
				exec++;
			}
			index++;
		}
	}
	GlobalLogger::getLogger().logLine("Map of fitness per key");
	std::ofstream results;
	std::string path = setupSettings->getPlotPath() + ".perKey";
	results.open(path);
	for (auto key : map){
		GlobalLogger::getLogger() << key.first << " " << key.second.size() << std::endl;
		results << key.first << " " << key.second.size() << std::endl;
	}
	results.close();
}


#endif /* RANDOM_H_ */


#ifndef RADAMSA_H_
#define RADAMSA_H_

#include <map>
#include <set>
#include <vector>
#include <string>
#include <fstream>

#include "../dataClasses/ProfilerMetrics.h"
#include "../helpers/DoubleLogger.h"
#include "../dataClasses/SetupSettings.h"
#include "../dataClasses/SetupSettings.h"

template <typename chromType = unsigned char>
std::vector<std::vector<chromType>> generateRadamsaList(int n, int size, EncryptionSetupSettings *setupSettings){
	std::vector<std::vector<chromType>> lists(n);
	int samples = 10;

	for (int i = 0 ; i < samples; i++){
		std::string samplePath = setupSettings->getSamplePath(i);
		std::ofstream keySample;
		keySample.open(samplePath);
		for (int j = 0; j < size; ++j) {
			chromType ch = rand() % 256;
			keySample << ch;
		}
		keySample.close();
	}

	for (int i = 1 ; i <= n; i++){ //we use the input ID = 0 to run the code, might need to fix later
		std::string keyFilePath = setupSettings->getKeyPath(i);
		std::string sampleFilePath = setupSettings->getSamplesFormula();
		std::string command = "radamsa -o " + keyFilePath + " " + sampleFilePath;
		system(command.c_str());
	}

	for (int i = 1; i <= n; ++i) {
		std::ifstream keyFile;
		keyFile.open(setupSettings->getKeyPath(i));
		for (int j = 0; j < size; ++j) {
			chromType ch;
			keyFile >> ch;
			lists[i-1].push_back(ch);
		}
		keyFile.close();
	}
	return lists;
}


template <typename chromType = char, typename fitType = double>
void quickRadamsa(int argc, char** argv,
		EncryptionSetupSettings *setupSettings,
		FITNESS_TEMPLATE fitnessFunction){


	ProfilerMetrics<fitType>* pm;
	std::map<int, std::set<fitType>> map;

	int randoms = 1000;
	if (argc > 2){
		randoms = std::atoi(argv[argc - 1]);
	}

	std::vector<std::vector<chromType>> lists =
			generateRadamsaList<chromType>(randoms, setupSettings->getKeyByteSize(), setupSettings);
	int exec = 0;
	int index = 0;
	for (std::vector<chromType> key : lists){
		//GlobalLogger::getLogger() << "--------------------------------------- Execution " << exec << std::endl;
		fitType fitness = fitnessFunction(key, &pm, setupSettings, 0);
		GlobalLogger::getLogger().logLine(fitness);
		map[index].insert(fitness);
		delete pm;
		exec++;
	}
}


#endif /* RADAMSA_H_ */

/*
 * execution.h
 *
 *  Created on: Nov 5, 2018
 *      Author: adrianh
 */

#ifndef ISPASS_EXECUTION_H_
#define ISPASS_EXECUTION_H_

template <typename fitType = double>
ProfilerMetrics<fitType>* executeAES(const std::string &encryptionType, const int keyBits,
		const std::string &keyPath, const std::string &inputPath, const std::string &resultPath,
		const std::string &metricName){

	ProfilerMetrics<fitType>* km = NULL;

	auto startTimer = TimerHelper::startTimer();

	if (encryptionType == "e"){
		std::string command = "nvprof --log-file " + resultPath +" --csv -m " + metricName + " " +
				"./AES/AES.x  " + encryptionType + " "
				+ std::to_string(keyBits) + " " + inputPath + " " + keyPath;
		GlobalLogger::getLogger().logLine(command);
		int returnCode = system(command.c_str());
		if (returnCode == -1){
			std::cerr << "Problems executing command: " << command << std::endl;
			exit(-1);
		}
		km = new ProfilerMetrics<fitType>(keyPath, inputPath, resultPath);
	}else
		if (encryptionType == "d"){
			//first encrypt
			std::string command = "./AES/AES.x e " +
					std::to_string(keyBits) + " " + inputPath + " " + keyPath;
			GlobalLogger::getLogger().logLine(command);
			int returnCode = system(command.c_str());
			if (returnCode == -1){
				std::cerr << "Problems executing command: " << command << std::endl;
				exit(-1);
			}
			//	decrypt the encrypted file
			command = "nvprof --log-file " + resultPath +" --csv -m " + metricName + " " +
					"./AES/AES.x  " + encryptionType + " "
					+ std::to_string(keyBits) + "  output_0.dat " + keyPath;
			GlobalLogger::getLogger().logLine(command);
			returnCode = system(command.c_str());
			if (returnCode == -1){
				std::cerr << "Problems executing command: " << command << std::endl;
				exit(-1);
			}
			km = new ProfilerMetrics<fitType>(keyPath, inputPath, resultPath);
		}

	auto endTimer = TimerHelper::startTimer();
	Statistics::addExecutionSeconds(TimerHelper::getSeconds(startTimer, endTimer));

	return km;
}





#endif /* ISPASS_EXECUTION_H_ */

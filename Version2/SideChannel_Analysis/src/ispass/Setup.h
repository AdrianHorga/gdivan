/*
 * Setup.h
 *
 *  Created on: Nov 5, 2018
 *      Author: adrianh
 */

#ifndef ISPASS_SETUP_H_
#define ISPASS_SETUP_H_

#include <vector>
#include "../dataClasses/SetupSettings.h"


std::vector<EncryptionSetupType> getPossibleISPASSExecutions(){
	std::vector<EncryptionSetupType> ispass;
	const std::string benchmark = "ispass"; //ispass aes
	const std::string enc = "e";
	const std::string dec = "d";


	//AES versions
	ispass.push_back(pack(benchmark, "aes-128", enc, 128));
	ispass.push_back(pack(benchmark, "aes-256", enc, 256));
	ispass.push_back(pack(benchmark, "aes-128", dec, 128));
	ispass.push_back(pack(benchmark, "aes-256", dec, 256));

	return ispass;
}



#endif /* ISPASS_SETUP_H_ */

/*
 * GenericFitnessFunctions.h
 *
 *  Created on: Nov 5, 2018
 *      Author: adrianh
 */

#ifndef ISPASS_GENERICFITNESSFUNCTIONS_H_
#define ISPASS_GENERICFITNESSFUNCTIONS_H_

#include "../helpers/Statistics.h"
#include "execution.h"


template <typename chromType = char, typename fitType = double>
fitType ispass(std::vector<chromType> chromosomes,
		ProfilerMetrics<fitType> **metrics, EncryptionSetupSettings *setup,
		const int keyID = 0){

	Statistics::addExecutions(1); //TODO make it local to each population
	GlobalLogger::getLogger() << ">>>>>>>>>>>>>>>>>>>>>>>>>>   " <<
			"Execution number " << Statistics::getTotalExecutions() <<
			"   <<<<<<<<<<<<<<<<<<<<<<<<<<<<" <<
			std::endl;

	//generating the ikeyFile
	std::ofstream keyFile;
	std::ostringstream keyValue;
	chromType ch;

	std::string keyPath = setup->getKeyPath(keyID);
	keyFile.open(keyPath.c_str());
	//GlobalLogger::getLogger().logLine(keyPath);

	auto flags = std::cout.flags();
	for (unsigned i = 0; i < setup->getKeyByteSize(); i++) {
		ch = chromosomes[i];
		if (i < setup->getKeyByteSize() - 1){
			keyFile << std::setw(2) << std::setfill('0') << std::hex << (unsigned)ch << " ";
		}else{
			keyFile << std::setw(2) << std::setfill('0') << std::hex << (unsigned)ch;
		}
		keyValue << std::setw(2) << std::setfill('0') << std::hex << (unsigned)ch;
	}
	keyFile.close();
	std::cout.flags(flags);
	//GlobalLogger::getLogger().logLine(keyValue.str());

	(*metrics) = executeAES<fitType>(setup->getEncryptionType(),
			setup->getKeyBits(), keyPath, setup->getInputPath(0), setup->getResultPath(keyID),
			setup->getMetricName());

	fitType fitness = (*(metrics))->getTotal(setup->getMetricName());

	Statistics::addEntry(keyValue.str());
	std::ofstream keysFile;
	std::string keysPath = setup->getPlotPath() + ".keys";
	keysFile.open(keysPath, std::iostream::app);
	keysFile << keyValue.str() << " " << std::to_string(fitness) << std::endl;
	keysFile.close();

	//write to plot file
	std::ofstream plotFile;
	plotFile.open(setup->getPlotPath(), std::ofstream::app);
	plotFile << fitness << std::endl;
	Statistics::addObservation(fitness);
	plotFile.close();

	return fitness;
}


#endif /* ISPASS_GENERICFITNESSFUNCTIONS_H_ */

/*
 * EncryptionSetupSettings.h
 *
 *  Created on: Oct 26, 2018
 *      Author: adrianh
 */

#ifndef DATACLASSES_ENCRYPTIONSETUPSETTINGS_H_
#define DATACLASSES_ENCRYPTIONSETUPSETTINGS_H_

#include "SetupSettings.h"

class EncryptionSetupSettings : public SetupSettings{
public:
	EncryptionSetupSettings(const std::string benchmark, const std::string encryption,
			const std::string encryptionType, const	unsigned inputBytes,
			const unsigned keyBits, const std::string runType,
			const std::string metricName, bool createFolders = true);
	virtual std::string getKeyPath(int ID);
	virtual std::string getInputPath(int ID);
	virtual std::string getResultPath(int ID);
	virtual std::string getOutputPath(int ID);
	virtual std::string getPlotPath();
	virtual std::string getLogPath();
	virtual std::string getSamplePath(int ID);
	virtual std::string getSamplesFormula();
	virtual void setKeyFolder(std::string folder);
	virtual void setInputFolder(std::string folder);
	virtual void setResultFolder(std::string folder);
	virtual void setOutputFolder(std::string folder);
	virtual void setPlotFolder(std::string folder);
	virtual void setSampleFolder(std::string folder);
	virtual void loadFromFile(std::string path);
	~EncryptionSetupSettings();

	unsigned getInputSize()  const;
	unsigned getKeyByteSize() const;
	unsigned getKeyBits() const;
	std::string getBenchmark() const;
	std::string getEncryption() const;
	std::string getEncryptionType() const;
	std::string getMetricName() const;
	std::string getRunType() const;

private:
	void createFolder(std::string folder);
	std::string keyFolder;
	std::string inputFolder;
	std::string resultFolder;
	std::string outputFolder;
	std::string plotFolder;
	std::string sampleFolder;
	std::string sampleFolderFormula;
	const std::string bench;
	const std::string encr;
	const std::string encrT;
	const unsigned inputSize;
	const unsigned keyBytes;
	const unsigned keyBits;
	const std::string searchType;
	const std::string metricName;
};



#endif /* DATACLASSES_ENCRYPTIONSETUPSETTINGS_H_ */

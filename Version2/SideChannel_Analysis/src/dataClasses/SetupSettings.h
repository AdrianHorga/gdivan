/*
 * SetupSettings.h
 *
 *  Created on: Oct 26, 2018
 *      Author: adrianh
 */

#ifndef DATACLASSES_SETUPSETTINGS_H_
#define DATACLASSES_SETUPSETTINGS_H_

#include <string>

#define FITNESS_TEMPLATE std::function<fitType (std::vector<chromType>, ProfilerMetrics<fitType>**, EncryptionSetupSettings*, const int)>


class SetupSettings{
public:
	virtual std::string getKeyPath(int ID) = 0;
	virtual std::string getInputPath(int ID) = 0;
	virtual std::string getResultPath(int ID) = 0;
	virtual std::string getOutputPath(int ID) = 0;
	virtual std::string getPlotPath() = 0;
	virtual std::string getLogPath() = 0;
	virtual void setKeyFolder(std::string path) = 0;
	virtual void setInputFolder(std::string path) = 0;
	virtual void setResultFolder(std::string path) = 0;
	virtual void setOutputFolder(std::string path) = 0;
	virtual void setPlotFolder(std::string path) = 0;
	virtual void loadFromFile(std::string path) = 0;
//	virtual void createFolders() = 0;
	virtual ~SetupSettings(){};

private:

};


struct EncryptionSetupType{
	std::string benchmark;
	std::string encr;
	std::string encrT;
	unsigned keyBits;
};

inline EncryptionSetupType pack(std::string benchmark, std::string encr, std::string encrT, unsigned bits){
	EncryptionSetupType execution;
	execution.benchmark = benchmark;
	execution.encr = encr;
	execution.encrT = encrT;
	execution.keyBits = bits;
	return execution;
}


#endif /* DATACLASSES_SETUPSETTINGS_H_ */

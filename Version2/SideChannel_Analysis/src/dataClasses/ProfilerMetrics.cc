/*
 * ProfilerMetrics.cc
 *
 *  Created on: Oct 26, 2018
 *      Author: adrianh
 */

#include "ProfilerMetrics.h"

//template <typename fitType = double>
//std::string savePlotFile(std::vector<ProfilerMetrics<fitType>*> keyInfo, std::string metricName,
//		int bits, int bytes, int size, std::string plotFolder, bool LOG){
//
//	std::ofstream plotFile;
//	std::string cmd = "mkdir -p " + plotFolder;
//	system(cmd.c_str());
//
//	std::string plotFilePath = getPlotPath(plotFolder, metricName, bits, bytes, size);
//	plotFile.open(plotFilePath);
//
//	if (LOG)
//		std::cout << "Writing to plot file " << plotFilePath << std::endl;
//
//	int key = 0;
//	for (ProfilerMetrics<fitType>* metric : keyInfo){
//		//plotFile << metric->getMin(metricName) << " " << metric->getAvg(metricName) << " " << metric->getMax(metricName) << std::endl;
//		plotFile << metric->getTotal(metricName) << std::endl;
//
//		if (LOG){
//			std::cout << "Key " << key << "|"
//					<< " metric " << metricName << "|"
//					<< " min " << metric->getMin(metricName) << "|"
//					<< " avg " << metric->getAvg(metricName) << "|"
//					<< " max " << metric->getMax(metricName) << "|"
//					<< " inv " << metric->getInv(metricName) << "|"
//					<< " total " << metric->getTotal(metricName) << "|"
//					<< std::endl;
//		}
//		key++;
//	}
//
//	plotFile.close();
//
//	if (LOG)
//		std::cout << "Done writing to plot file " << plotFilePath << std::endl;
//
//	return plotFilePath;
//}

std::vector<std::string> split(std::string str, const char delim) {
	std::vector<std::string> v;
	std::string tmp;

	bool stringer = false;
	int count = 0;;
	for(std::string::const_iterator i = str.begin(); i <= str.end(); ++i) {
		if((*i == '"') && (!stringer)){
			stringer = true;
			count = 0;
		}

		if ((stringer) && (count > 0)){
			if((*i != '"') && (i != str.end())){
				tmp += *i;
			}else{
				v.push_back(tmp);
				tmp = "";
				stringer = false;
				count = 0;
				if (i <= str.end())
					++i; //skip the delimiter
			}
		}else
			if (!stringer){
				if((*i != delim) && (i != str.end())) {
					tmp += *i;
				} else {
					v.push_back(tmp);
					tmp = "";
					count = 0;
				}
			}

		count++;
	}


	return v;
}

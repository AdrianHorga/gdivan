#include "EncryptionSetupSettings.h"

inline std::string packFolderPath(const std::string benchmark, const std::string encryption,
		const std::string encryptionType, const std::string metric, const	unsigned inputBytes,
		const unsigned keyBits,	const std::string runType, const std::string destination){
	std::string packed = "./" + benchmark + "_" + encryption + "_" + encryptionType + "/" +
			metric + "_" +
			"_i" + std::to_string(inputBytes) + "_k" + std::to_string(keyBits) +
			"_" + runType + "_" + destination + "/";
	return packed;
}

inline std::string packDestinationPath(const std::string folder, const std::string destination,
		const unsigned size, const int ID, std::string extension = ""){
	std::string packed = folder + destination + "_" + std::to_string(size) + "_" + std::to_string(ID) + extension;
	return packed;
}

inline std::string packDestinationPath(const std::string folder, const std::string destination,
		const unsigned size1, const unsigned size2, const int ID, std::string extension = ""){
	std::string packed = folder + destination + "_" + std::to_string(size1) +
			"_" + std::to_string(size2) + "_" + std::to_string(ID) + extension;
	return packed;
}

EncryptionSetupSettings::EncryptionSetupSettings(const std::string benchmark, const std::string encryption,
		const std::string encryptionType, const unsigned inputBytes, const unsigned keyBits,
		const std::string runType, const std::string metricName, bool createFolders) : SetupSettings(),
				bench(benchmark), encr(encryption), encrT(encryptionType),
				inputSize(inputBytes), keyBytes(keyBits / 8), keyBits(keyBits),
				searchType(runType), metricName(metricName){

	keyFolder = packFolderPath(benchmark, encryption, encryptionType,
			metricName, inputBytes, keyBits, runType, "keys");
	inputFolder = packFolderPath(benchmark, encryption, encryptionType,
			metricName, inputBytes, keyBits, runType, "inputs");
	resultFolder = packFolderPath(benchmark, encryption, encryptionType,
			metricName, inputBytes, keyBits, runType, "results");
	outputFolder = packFolderPath(benchmark, encryption, encryptionType,
			metricName, inputBytes, keyBits, runType, "outputs");
	plotFolder = packFolderPath(benchmark, encryption, encryptionType,
			metricName, inputBytes, keyBits, runType, "plots");
	sampleFolder = packFolderPath(benchmark, encryption, encryptionType,
				metricName, inputBytes, keyBits, runType, "samples");
	sampleFolderFormula = sampleFolder + "*.sample";

	if (createFolders){
		createFolder(keyFolder);
		createFolder(inputFolder);
		createFolder(resultFolder);
		createFolder(outputFolder);
		createFolder(plotFolder);
		createFolder(sampleFolder);
	}
}

std::string EncryptionSetupSettings::getKeyPath(int ID){
	return packDestinationPath(keyFolder, "key", keyBits, ID);
}
std::string EncryptionSetupSettings::getInputPath(int ID){
	return packDestinationPath(inputFolder, "input", inputSize, ID);
}
std::string EncryptionSetupSettings::getResultPath(int ID){
	return packDestinationPath(resultFolder, "result", inputSize, keyBits, ID);
}
std::string EncryptionSetupSettings::getOutputPath(int ID){
	return packDestinationPath(outputFolder, "output", inputSize, keyBits, ID);
}
std::string EncryptionSetupSettings::getPlotPath(){
	return packDestinationPath(plotFolder, searchType, inputSize, keyBits, 0, ".dat");
}
std::string EncryptionSetupSettings::getLogPath(){
	return packDestinationPath(plotFolder, searchType, inputSize, keyBits, 0, ".log");
}
void EncryptionSetupSettings::loadFromFile(std::string path){
	//TODO read settings from file
}

std::string EncryptionSetupSettings::getSamplePath(int ID){
	return packDestinationPath(sampleFolder, searchType, inputSize, keyBits, ID, ".sample");
}

std::string EncryptionSetupSettings::getSamplesFormula(){
	return sampleFolderFormula;
}

EncryptionSetupSettings::~EncryptionSetupSettings(){

}
void EncryptionSetupSettings::setKeyFolder(std::string folder){
	keyFolder = folder;
	createFolder(keyFolder);
}
void EncryptionSetupSettings::setInputFolder(std::string folder){
	inputFolder = folder;
	createFolder(inputFolder);
}
void EncryptionSetupSettings::setResultFolder(std::string folder){
	resultFolder = folder;
	createFolder(resultFolder);
}
void EncryptionSetupSettings::setOutputFolder(std::string folder){
	outputFolder = folder;
	createFolder(outputFolder);
}
void EncryptionSetupSettings::setPlotFolder(std::string folder){
	plotFolder = folder;
	createFolder(plotFolder);
}

void EncryptionSetupSettings::setSampleFolder(std::string folder){
	sampleFolder = folder;
	sampleFolderFormula = sampleFolder + "*.sample";
	createFolder(sampleFolder);
}

void EncryptionSetupSettings::createFolder(std::string folder){
	std::string mkdir = "mkdir -p ";
	std::string command;
	command = mkdir + folder;
	system(command.c_str());
}

unsigned EncryptionSetupSettings::getInputSize() const {
	return inputSize;
}
unsigned EncryptionSetupSettings::getKeyByteSize() const{
	return keyBytes;
}
unsigned EncryptionSetupSettings::getKeyBits() const{
	return keyBits;
}
std::string EncryptionSetupSettings::getBenchmark() const{
	return bench;
}
std::string EncryptionSetupSettings::getEncryption() const{
	return encr;
}
std::string EncryptionSetupSettings::getEncryptionType() const{
	return encrT;
}
std::string EncryptionSetupSettings::getMetricName() const{
	return metricName;
}
std::string EncryptionSetupSettings::getRunType() const{
	return searchType;
}

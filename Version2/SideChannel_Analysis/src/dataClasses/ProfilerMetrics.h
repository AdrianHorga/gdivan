/*
 * ProfilerMetrics.h
 *
 *  Created on: Oct 26, 2018
 *      Author: adrianh
 */

#ifndef DATACLASSES_PROFILERMETRICS_H_
#define DATACLASSES_PROFILERMETRICS_H_

#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>

std::vector<std::string> split(std::string str, const char delim);

template <typename fitType = double>
class Values{
public:
	fitType min;
	fitType avg;
	fitType max;
	int inv;
	Values(fitType minimum, fitType average, fitType maximum, int invocations) :
		min(minimum), avg(average), max(maximum), inv(invocations) {
	}
	Values():min(-1),avg(-1), max(-1), inv(0) {}
private:
};

template <typename fitType = double>
class ProfilerMetrics{
private:
	std::string keyLocation;
	std::string inputLocation;
	std::string resultLocation;
	std::map<std::string, Values<fitType>> metrics;

public:
	ProfilerMetrics(std::string keyPath, std::string inputPath, std::string resultPath):
		keyLocation(keyPath), inputLocation(inputPath), resultLocation(resultPath){

		std::ifstream resultFile;
		resultFile.open(resultPath);

		// std::cout << "///////////////////////////////////////////////creating new metric for key/////////////////////////////////" << std::endl;

		//skip the first 6 lines
		std::string line;
		int count = 0;
		while(!resultFile.eof()){
			std::getline(resultFile, line);
			count++;
			if ((count > 5) && (!resultFile.eof())){
				//process(&line);
				// std::cout << "LINE: " << line << std::endl;
				std::vector<std::string> tokens = split(line, ',');
				//                std::cout  << tokens.size() << " "
				//                            << tokens[0] << "|"
				//                            << tokens[1] << "|"
				//                            << tokens[2] << "|"
				//                            << tokens[3] << "|"
				//                            << tokens[4] << "|"
				//                            << tokens[5] << "|"
				//                            << tokens[6] << "|"
				//                            << tokens[7]
				//                            << std::endl;
				std::string name = tokens[3];
				double min = atof(tokens[5].c_str());
				double max = atof(tokens[6].c_str());
				double avg = atof(tokens[7].c_str());
				int inv = atoi(tokens[2].c_str());
				Values<fitType> val(min, avg, max, inv);
				metrics[name] = val;
				//metrics.insert( std::pair<std::string ,Values>(name,val) );
			}

		}

		resultFile.close();
	}

	ProfilerMetrics (const ProfilerMetrics &obj){
		keyLocation = obj.keyLocation;
		inputLocation = obj.inputLocation;
		resultLocation = obj.resultLocation;
		metrics = obj.metrics;
	}

	ProfilerMetrics (){
		keyLocation = "";
		inputLocation = "";
		resultLocation = "";
	}


	fitType getMin(std::string metricName){
		Values<fitType> val = metrics.at(metricName);
		return val.min;
	}

	fitType getAvg(std::string metricName){
		Values<fitType> val = metrics.at(metricName);
		return val.avg;
	}

	fitType getMax(std::string metricName){
		Values<fitType> val = metrics.at(metricName);
		return val.max;
	}

	fitType getInv(std::string metricName){
		Values<fitType> val = metrics.at(metricName);
		return val.inv;
	}

	fitType getTotal(std::string metricName){
		Values<fitType> val = metrics.at(metricName);
		fitType total = 0;
		if (val.inv == 1){
			total = val.avg;
		}else
			if (val.inv == 2){
				total = val.min + val.max;
			}else
				if (val.inv >= 3){
					total = val.min + val.max + ((val.inv - 2) * val.avg);
				}
		return total;
	}

};

//template <typename fitType = double>
//std::string savePlotFile(std::vector<ProfilerMetrics<fitType>*> keyInfo, std::string metricName,
//		int bits, int bytes, int size, std::string plotFolder, bool LOG);

#endif /* DATACLASSES_PROFILERMETRICS_H_ */

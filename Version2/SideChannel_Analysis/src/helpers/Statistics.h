
#ifndef HELPERS_STATISTICS_H_
#define HELPERS_STATISTICS_H_

#include <map>
#include <string>
#include <iostream>

class Statistics{
public:
	static long int getTotalExecutions();
	static void addExecutions(int count);
	static void initializeExecutions(int initialValue);
	static double getExecutionSeconds();
	static void addExecutionSeconds(double runSeconds);
	static void addEntry(std::string entry);
	static void printEntries(std::string keyLogPath);
	static void addObservation(double obs); //TODO make it generic
	static void printObservations();
	static void reset();
private:
	static long int executions;
	static double executionTime;
	static std::map<std::string, unsigned> entries;
	static std::map<double, long> observations;
};


#endif /* HELPERS_STATISTICS_H_ */

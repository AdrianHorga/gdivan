
#ifndef HELPERS_DOUBLELOGGER_H_
#define HELPERS_DOUBLELOGGER_H_

#include <iostream>
#include <fstream>
#include <string>
#include "TimerHelper.h"

enum class DualLoggerMode {NONE, FILE, COUT, BOTH};

class DualLogger{
public:


	DualLogger(std::string path): shouldLog(DualLoggerMode::BOTH), logPath(path){	}
	DualLogger(std::string logFolder, std::string logFileName): shouldLog(DualLoggerMode::BOTH){
		std::string path = logFolder + logFileName;
		logPath = path;
	}
	~DualLogger(void) {coss.close();}

	void open(bool append = true){
		if (append == true){
			coss.open(logPath, std::ofstream::app);
		}else{
			coss.open(logPath);
		}
	}
	void clear(){
		coss.open(logPath, std::ofstream::trunc);
		coss.close();
	}
	void close(){
		coss.close();
	}

	template <class T>
	DualLogger& operator<< (T val)
	{
		if (checkFileWrite()){
			coss  << val;
		}
		if (checkCoutWrite()){
			std::cout << val;
		}
		return *this;
	}

	template <class T>
	void logLine(T line){
		if (shouldLog != DualLoggerMode::NONE){
			std::string timeHeader = "<" + TimerHelper::getCurrentTime() + ">" + ": ";
			if (checkFileWrite()){
				coss << timeHeader << line << std::endl;
			}
			if (checkCoutWrite()){
				std::cout << timeHeader << line << std::endl;
			}
		}
	}

	DualLogger& operator<< (std::ostream& (*pfun)(std::ostream&))
	{
		if (checkFileWrite()){
			pfun(coss);
		}
		if (checkCoutWrite()){
			pfun(std::cout);
		}
		return *this;
	}

	void setStatus(DualLoggerMode status){
		shouldLog = status;
	}
private:
	DualLoggerMode shouldLog;
	std::string logPath;
	std::ofstream coss;

	bool checkFileWrite(){
		if ((shouldLog == DualLoggerMode::BOTH)||(shouldLog == DualLoggerMode::FILE)){
			return true;
		}
		return false;
	}

	bool checkCoutWrite(){
		if ((shouldLog == DualLoggerMode::BOTH)||(shouldLog == DualLoggerMode::COUT)){
			return true;
		}
		return false;
	}
};




class GlobalLogger{
public:
	static void setLogging(DualLoggerMode status){
		if (logger){
			logger->setStatus(status);
		}
	}
	static DualLogger& getLogger(){
		return *logger;
	}
	static void setLogger(DualLogger *log){
		logger = log;
	}

	static void initializeLogger(std::string path){
		initializeLogger(new DualLogger(path));
	}
	static void initializeLogger(DualLogger *log){
		setLogger(log);
		logger->clear();
		logger->open();
	}
	static void cleanUp(){
		delete logger;
	}

private:
	static DualLogger *logger;
};

#endif /* HELPERS_DOUBLELOGGER_H_ */

#include "Statistics.h"
#include "DoubleLogger.h"

#include <fstream>

long int Statistics::executions = 0;
double Statistics::executionTime = 0;
std::map<std::string, unsigned> Statistics::entries;
std::map<double, long> Statistics::observations;

long int Statistics::getTotalExecutions(){
	return executions;
}

void Statistics::addExecutions(int count){
	executions += count;
}

void Statistics::initializeExecutions(int initialValue){
	executions = initialValue;
}

double Statistics::getExecutionSeconds(){
	return executionTime;
}
void Statistics::addExecutionSeconds(double runSeconds){
	executionTime += runSeconds;
}

void Statistics::addEntry(std::string entry){
	if (entries.find(entry) == entries.end()){
		entries[entry] = 1;
	}else{
		entries[entry]++;
	}
}

void Statistics::printEntries(std::string keyLogPath){
	GlobalLogger::getLogger() << "\n------------------------------------------------------\n";
	GlobalLogger::getLogger() << "Keys          <->          Appearances\n";
	std::ofstream histoKeys;
	histoKeys.open(keyLogPath);
	for (auto value : entries){
		GlobalLogger::getLogger() << value.first << " " << std::to_string(value.second) << std::endl;
		histoKeys << value.first << " " << std::to_string(value.second) << std::endl;
	}
	GlobalLogger::getLogger() << "Unique entries : " << entries.size() << std::endl;
	histoKeys.close();
}

void Statistics::addObservation(double obs){
	//TODO make it generic
	if (observations.find(obs) == observations.end()){
		observations[obs] = 1;
	}else{
		observations[obs]++;
	}
}
void Statistics::printObservations(){
	GlobalLogger::getLogger() << "\n------------------------------------------------------\n";
	GlobalLogger::getLogger() << "Observations          <->          Appearances\n";
	for (auto value : observations){
		GlobalLogger::getLogger() << value.first << " " << value.second << std::endl;
	}
	GlobalLogger::getLogger() << "Unique entries : " << observations.size() << std::endl;
}

void Statistics::reset(){
	executions = 0;
	executionTime = 0;
	entries.clear();
	observations.clear();
}

/*
 * execution.h
 *
 *  Created on: Oct 8, 2018
 *      Author: adrianh
 */

#ifndef ENGINE_CUDA_EXECUTION_H_
#define ENGINE_CUDA_EXECUTION_H_

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iomanip>

template <typename fitType = double>
ProfilerMetrics<fitType>* executeEC(const std::string &encryption, const std::string &encryptionType,
		const int bits, const std::string &keyPath, const std::string &inputPath,
		const std::string &resultPath, const std::string &outputPath, const std::string &metricName = "all"){

	auto startTimer = TimerHelper::startTimer();

	const int bytes = bits/8;
	std::vector<unsigned> keyArray(bytes);
	std::ifstream inStream;
	inStream>>std::hex;
	inStream.open( keyPath );

	for (unsigned cnt=0; cnt<keyArray.size(); ++cnt){
		inStream >> keyArray[cnt];
		if ( ( inStream.eof() ) && ( cnt != keyArray.size()-1 ) )
			throw std::string("cannot use a key with less than 32 or 16 elements ");
		if ( ( !inStream.eof() ) && ( cnt == keyArray.size()-1 ) )
			std::cout << "WARNING: your key file has more than 32 or 16 elements. It will be cut down to this threshold\n";
		if ( inStream.fail() )
			throw std::string("Check that your key file elements are in hexadecimal format separated by blanks");
	}
	inStream.close();
	std::ostringstream keyValue;
	for (unsigned cnt=0; cnt<keyArray.size(); ++cnt){
		keyValue << std::setw(2) << std::setfill('0') << std::hex << keyArray[cnt];
	}


	static const std::string iv_key="AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";

	//if we do decryption the do the encryption of  the file
	if (encryptionType == "d"){
		std::string tempFile = inputPath + ".enc";
		std::string prelimEncComm = "/home/adrianh/tools/openssl-cuda/bin/openssl enc -e " +
				("-" + encryption + " ") +
				"-in " + inputPath + " -out " + tempFile + " " +
				"-nosalt -md md5 -p -bufsize 4096 " +
				"-K " + keyValue.str() + " -iv " + iv_key ;
		GlobalLogger::getLogger() << prelimEncComm << std::endl;
		int returnCode = system(prelimEncComm.c_str());
		if (returnCode == -1){
			std::cerr << "Problems executing command: " << prelimEncComm << std::endl;
			exit(-1);
		}

		std::string command = "nvprof --log-file " + resultPath +" --csv -m " + metricName + " " +
				"/home/adrianh/tools/openssl-cuda/bin/openssl enc -engine cudamrg " +
				"-" + encryptionType + " " +
				"-" + encryption + " " +
				"-in " + tempFile + " -out " + outputPath + " " +
				"-nosalt -md md5 -p -bufsize 4096 " +
				"-K " + keyValue.str() + " -iv " + iv_key ;
		GlobalLogger::getLogger() << command << std::endl;
		returnCode = system(command.c_str());
		if (returnCode == -1){
			std::cerr << "Problems executing command: " << command << std::endl;
			exit(-1);
		}
	}else{
		std::string command = "nvprof --log-file " + resultPath +" --csv -m " + metricName + " " +
				"/home/adrianh/tools/openssl-cuda/bin/openssl enc -engine cudamrg " +
				"-" + encryptionType + " " +
				"-" + encryption + " " +
				"-in " + inputPath + " -out " + outputPath + " " +
				"-nosalt -md md5 -p -bufsize 4096 " +
				"-K " + keyValue.str() + " -iv " + iv_key ;
		GlobalLogger::getLogger() << command << std::endl;
		int returnCode = system(command.c_str());
		if (returnCode == -1){
			std::cerr << "Problems executing command: " << command << std::endl;
			exit(-1);
		}
	}
	auto endTimer = TimerHelper::startTimer();
	ProfilerMetrics<fitType>* km = new ProfilerMetrics<fitType>(keyPath, inputPath, resultPath);

	Statistics::addExecutionSeconds(TimerHelper::getSeconds(startTimer, endTimer));

	return km;
}



#endif /* ENGINE_CUDA_EXECUTION_H_ */

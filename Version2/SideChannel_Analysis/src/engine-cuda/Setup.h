/*
 * Setup.h
 *
 *  Created on: Oct 26, 2018
 *      Author: adrianh
 */

#ifndef ENGINE_CUDA_SETUP_H_
#define ENGINE_CUDA_SETUP_H_

#include <vector>
#include "../dataClasses/SetupSettings.h"


std::vector<EncryptionSetupType> getPossibleEngineCUDAExecutions(){
	std::vector<EncryptionSetupType> engine_cuda;
	const std::string benchmark = "ec"; //engine cuda
	const std::string enc = "e";
	const std::string dec = "d";


	//ecb encryption
	engine_cuda.push_back(pack(benchmark, "aes-128-ecb", enc, 128));
	engine_cuda.push_back(pack(benchmark, "aes-256-ecb", enc, 256));
	engine_cuda.push_back(pack(benchmark, "bf-ecb", enc, 128));
	engine_cuda.push_back(pack(benchmark, "camellia-128-ecb", enc, 128));
	engine_cuda.push_back(pack(benchmark, "cast5-ecb", enc, 128));
	engine_cuda.push_back(pack(benchmark, "des-ecb", enc, 64));
	engine_cuda.push_back(pack(benchmark, "aes-192-ecb", enc, 192));


	//ecb decryption
	engine_cuda.push_back(pack(benchmark, "aes-128-ecb", dec, 128));
	engine_cuda.push_back(pack(benchmark, "bf-ecb", dec, 128));
	engine_cuda.push_back(pack(benchmark, "camellia-128-ecb", dec, 128));
	engine_cuda.push_back(pack(benchmark, "cast5-ecb", dec, 128));
	engine_cuda.push_back(pack(benchmark, "des-ecb", dec, 64));
	engine_cuda.push_back(pack(benchmark, "aes-192-ecb", dec, 192));
	engine_cuda.push_back(pack(benchmark, "aes-256-ecb", dec, 256));

	//cbc decryption
	engine_cuda.push_back(pack(benchmark, "aes-128-cbc", dec, 128));
	engine_cuda.push_back(pack(benchmark, "bf-cbc", dec, 128));
	engine_cuda.push_back(pack(benchmark, "camellia-128-cbc", dec, 128));
	engine_cuda.push_back(pack(benchmark, "cast5-cbc", dec, 128));
	engine_cuda.push_back(pack(benchmark, "des-cbc", dec, 64));
	engine_cuda.push_back(pack(benchmark, "aes-192-cbc", dec, 192));
	engine_cuda.push_back(pack(benchmark, "aes-256-cbc", dec, 256));

	return engine_cuda;
}



#endif /* ENGINE_CUDA_SETUP_H_ */

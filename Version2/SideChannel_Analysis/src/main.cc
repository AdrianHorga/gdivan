#include <iostream>
#include <string>
#include "helpers/TimerHelper.h"
#include "helpers/DoubleLogger.h"
#include "GA/GASettings.h"
#include "GA/scenarios.h"
#include "engine-cuda/GenericFitnessFunctions.h"
#include "ispass/GenericFitnessFunctions.h"
#include "dataClasses/EncryptionSetupSettings.h"
#include "dataClasses/SetupSettings.h"
#include "engine-cuda/Setup.h"
#include "ispass/Setup.h"
#include "random/Random.h"
#include "random/Radamsa.h"

#ifndef BENCH
#define BENCH 1
#endif

template <typename chromType = char, typename fitType = double>
void runExperiments(const std::string& runType, int argc,
		const std::string& settingFile, char** argv,
		EncryptionSetupSettings* setupSettings,
		FITNESS_TEMPLATE fitnessFunction) {

	GlobalLogger::initializeLogger(setupSettings->getLogPath());

	timer_point start = TimerHelper::startTimer();
	std::string logLine = runType + " " +
			setupSettings->getBenchmark() + " " + setupSettings->getEncryption() + " " +
			setupSettings->getEncryptionType() + " " + std::to_string(setupSettings->getKeyBits()) + " " +
			std::to_string(setupSettings->getInputSize());
	GlobalLogger::getLogger().logLine("Starting program " + logLine);

	if (runType == "antiga") {
		AntiGA_run<chromType, fitType>(argc, argv, settingFile, setupSettings, fitnessFunction);
	} else if (runType == "quick_exhaustive") {
		quickExhaustive<chromType, fitType>(argc, argv, setupSettings, fitnessFunction);
	} else if (runType == "random") {
		quickRandom<chromType, fitType>(argc, argv, setupSettings, fitnessFunction);
	} else if (runType == "radamsa") {
		quickRadamsa<chromType, fitType>(argc, argv, setupSettings, fitnessFunction);
	}

	GlobalLogger::getLogger() << "Total engine-cuda executions: " << Statistics::getTotalExecutions() <<
			"\nTotal execution time: " << Statistics::getExecutionSeconds() << " seconds "<<
			"\nAverage execution time:" << (Statistics::getExecutionSeconds()/Statistics::getTotalExecutions()) <<
			std::endl;

	timer_point stop = TimerHelper::stopTimer();
	GlobalLogger::getLogger().logLine("Total execution time " +
			std::to_string(TimerHelper::getSeconds(start, stop)) + " seconds");
	GlobalLogger::getLogger().logLine("Ending program " + logLine);
	Statistics::reset();
	GlobalLogger::cleanUp();

	delete setupSettings;
}

int main(int argc, char **argv){
	const unsigned int inputSize = 16;
	//	const std::string settingFile = "settings128.txt";
	std::string settingFile = "settings256.txt";
	const std::string metricName = "shared_load_transactions";
	std::string runType = "random";


	if (argc > 1){
		if (argv[1][0] == 'r'){
			runType = "random";
		}else
			if (argv[1][0] == 'e'){
				runType = "quick_exhaustive";
			}else
				if (argv[1][0] == 'a'){
					runType = "antiga";
				}else
					if (argv[1][0] == 'x'){
						runType = "radamsa";
					}
	}

	std::vector<EncryptionSetupType> execs;
	//	execs = getPossibleISPASSExecutions();
	//	auto fitnesFunction = ispass<unsigned char, int>;

	//	std::string inputFilePath = "./input7/";
	//	unsigned i;
	//
	//	settingFile = "settings128.txt";
	//	execs = getPossibleEngineCUDAExecutions();
	//	auto fitnesFunction = engineCUDA<unsigned char, int>;
	//
	//	i = 0; //AES 128 ecb
	//	EncryptionSetupSettings *setupSettings = new EncryptionSetupSettings(execs[i].benchmark,
	//			execs[i].encr, execs[i].encrT,
	//			inputSize, execs[i].keyBits, runType, metricName);
	//	setupSettings->setInputFolder(inputFilePath);
	//	runExperiments<unsigned char, int>(runType, argc, settingFile, argv, setupSettings,
	//			fitnesFunction);
	//
	//	//-------------------------------------------------------------------
	//
	//	settingFile = "settings128.txt";
	//	execs = getPossibleISPASSExecutions();
	//	fitnesFunction = ispass<unsigned char, int>;
	//	i = 0; //AES 128
	//
	//	setupSettings = new EncryptionSetupSettings(execs[i].benchmark,
	//			execs[i].encr, execs[i].encrT,
	//			inputSize, execs[i].keyBits, runType, metricName);
	//	setupSettings->setInputFolder(inputFilePath);
	//	runExperiments<unsigned char, int>(runType, argc, settingFile, argv, setupSettings,
	//			fitnesFunction);
	//
	//	//-------------------------------------------------------------------
	//
	//
	//	settingFile = "settings256.txt";
	//	execs = getPossibleEngineCUDAExecutions();
	//	fitnesFunction = engineCUDA<unsigned char, int>;
	//
	//	i = 1; //AES 256 ecb
	//	setupSettings = new EncryptionSetupSettings(execs[i].benchmark,
	//			execs[i].encr, execs[i].encrT,
	//			inputSize, execs[i].keyBits, runType, metricName);
	//	setupSettings->setInputFolder(inputFilePath);
	//	runExperiments<unsigned char, int>(runType, argc, settingFile, argv, setupSettings,
	//			fitnesFunction);
	//
	//	//-------------------------------------------------------------------
	//
	//	settingFile = "settings256.txt";
	//	execs = getPossibleISPASSExecutions();
	//	fitnesFunction = ispass<unsigned char, int>;
	//	i = 1; //AES 256
	//
	//	setupSettings = new EncryptionSetupSettings(execs[i].benchmark,
	//			execs[i].encr, execs[i].encrT,
	//			inputSize, execs[i].keyBits, runType, metricName);
	//	setupSettings->setInputFolder(inputFilePath);
	//	runExperiments<unsigned char, int>(runType, argc, settingFile, argv, setupSettings,
	//			fitnesFunction);





	//	execs = getPossibleEngineCUDAExecutions();
	//	auto fitnesFunction = engineCUDA<unsigned char, int>;
	//
	//	for (unsigned i = 0; i < 5; ++i) {
	//		EncryptionSetupSettings *setupSettings = new EncryptionSetupSettings(execs[i].benchmark,
	//				execs[i].encr, execs[i].encrT,
	//				inputSize, execs[i].keyBits, runType, metricName);
	//		setupSettings->setInputFolder("./input1/");
	//		runExperiments<unsigned char, int>(runType, argc, settingFile, argv, setupSettings,
	//				fitnesFunction);
	//	}


	execs = getPossibleISPASSExecutions();
	auto fitnesFunction = ispass<unsigned char, int>;

	for (unsigned i = 0; i < 2; ++i) {
		EncryptionSetupSettings *setupSettings = new EncryptionSetupSettings(execs[i].benchmark,
				execs[i].encr, execs[i].encrT,
				inputSize, execs[i].keyBits, runType, metricName);
		setupSettings->setInputFolder("./input1/");
		runExperiments<unsigned char, int>(runType, argc, settingFile, argv, setupSettings,
				fitnesFunction);
	}

	return 0;
}




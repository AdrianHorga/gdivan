
#include "GASettings.h"
#include "../helpers/DoubleLogger.h"

//GASettings::GASettings(){}

GASettings::GASettings(int populationSize, int maximumIterations,
		float diversificationRate, float eliteRate, float mutationRate,
		int tournament, int renewPopulationStep, int variationThreshold):
		populationSize(populationSize), maximumIterations(maximumIterations),
		specialDiversificationRate(diversificationRate), eliteRate(eliteRate),
		mutationRate(mutationRate), tournamentSize(tournament),
		renewPopulationStep(renewPopulationStep), variationThreshold(variationThreshold){
}

GASettings* readSettings(const std::string &filename){
	std::ifstream infile;
	int populationSize;
	int maximumIterations;
	float diversificationRate;
	float eliteRate;
	float mutationRate;
	int tournamentSize;
	int renewPopulationStep;
	int variationThreshold;
	std::string parameterName;

	infile.open(filename.c_str());

	if (infile.is_open()){
		infile >> parameterName >> populationSize;
		//		std::cout<< "populationSize = " << populationSize << "\n";
		GlobalLogger::getLogger()<< parameterName << " = " << populationSize << "\n";

		infile >> parameterName >> maximumIterations;
		//		std::cout<< "maximumIterations = " << maximumIterations << "\n";
		GlobalLogger::getLogger()<< parameterName << " = " << maximumIterations << "\n";

		infile >>  parameterName >> diversificationRate;
		GlobalLogger::getLogger()<< parameterName << " = " << diversificationRate << "\n";

		infile >>  parameterName >> eliteRate;
		GlobalLogger::getLogger()<< parameterName << " = " << eliteRate << "\n";

		infile >>parameterName >> mutationRate;
		GlobalLogger::getLogger()<< parameterName << " = " << mutationRate << "\n";

		infile >> parameterName >> tournamentSize;
		GlobalLogger::getLogger()<< parameterName << " = " << tournamentSize << "\n";

		infile >>parameterName >> renewPopulationStep;
		GlobalLogger::getLogger()<< parameterName << " = " << renewPopulationStep << "\n";

		infile >> parameterName >> variationThreshold;
		GlobalLogger::getLogger()<< parameterName << " = " << variationThreshold << "\n";

		infile.close();
	}else{
		std::cerr << "Could not open file : " + filename;
	}

	return (new GASettings(populationSize, maximumIterations, diversificationRate, eliteRate, mutationRate, tournamentSize, renewPopulationStep, variationThreshold));
}

int GASettings::getPopulationSize() const{
	return (populationSize);
}

int GASettings::getMaximumIterations() const{
	return (maximumIterations);
}

int GASettings::getRenewStep() const{
	return (renewPopulationStep);
}

float GASettings::getDiversificationRate() const{
	return (specialDiversificationRate);
}

float GASettings::getEliteRate() const{
	return (eliteRate);
}

float GASettings::getMutationRate() const{
	return (mutationRate);
}

int GASettings::getTournamentSize() const{
	return (tournamentSize);
}

int GASettings::getVariationThreshold() const{
	return (variationThreshold);
}



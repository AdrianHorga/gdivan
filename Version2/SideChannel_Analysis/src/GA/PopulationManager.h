
#ifndef GENETIC_ALGORITHM_POPULATIONMANAGER_H_
#define GENETIC_ALGORITHM_POPULATIONMANAGER_H_
#include <vector>
#include <string>

#include "Individual.h"

#define BIT_FLIP(a,b) ((a) ^= (1U<<(b)))

template <typename chromType = char, typename fitType = double>
class PopulationManager{
public:
	~PopulationManager(){
		for (unsigned i = 0 ; i < pool.size(); i++){
			delete pool[i];
		}
	}
	void crossover(Individual<chromType, fitType>* parent1, Individual<chromType, fitType>* parent2,
			Individual<chromType, fitType>* child1, Individual<chromType, fitType>* child2){

		unsigned length = parent1->getGenotypeSize();

		unsigned crossoverPoint = 1 + rand() % (length - 1);
		//unsigned crossoverPoint = length / 2;

		for (unsigned i = 0 ; i < length; i++){
			chromType from1 = parent1->getChromosome(i);
			chromType from2 = parent2->getChromosome(i);
			if (i < crossoverPoint){
				child1->addChromosome(from1);
				child2->addChromosome(from2);
			}else{
				child1->addChromosome(from2);
				child2->addChromosome(from1);
			}
		}

	}

	void crossover(Individual<chromType, fitType>* parent1, Individual<chromType, fitType>* parent2,
			Individual<chromType, fitType>** child){

		auto child1 = generateIndividual(0);
		auto child2 = generateIndividual(0);


		unsigned length = parent1->getGenotypeSize();

		unsigned crossoverPoint = 1 + rand() % (length - 1);
		//unsigned crossoverPoint = length / 2;

		for (unsigned i = 0 ; i < length; i++){
			chromType from1 = parent1->getChromosome(i);
			chromType from2 = parent2->getChromosome(i);
			if (i < crossoverPoint){
				child1->addChromosome(from1);
				child2->addChromosome(from2);
			}else{
				child1->addChromosome(from2);
				child2->addChromosome(from1);
			}
		}

		if (rand() % 2 == 0){
			*child = child1;
		}else{
			*child = child2;
		}

	}

	//mutate only one bit
	void mutate(Individual<chromType, fitType>* individual){
		unsigned pointOfMutation  = rand() % individual->getGenotypeSize();

		chromType newSolution = individual->getChromosome(pointOfMutation);//Individual<chromType, fitType>::getRandChrom();
		char randomBit = rand() % (sizeof(chromType) * 8);
		BIT_FLIP(newSolution, randomBit);

		individual->setChromosome(newSolution, pointOfMutation);
	}

	Individual<chromType, fitType>* generateIndividual(unsigned size){
		return generateRandomIndividual(size);
	}
	Individual<chromType, fitType>* generateRandomIndividual(unsigned size){

		std::vector<chromType> data;
		if (size != 0){
			//data = job->generateData(runType);
			for (unsigned i = 0; i < size; i++){
				chromType chrom = Individual<chromType, fitType>::getRandChrom();
				data.push_back(chrom);
			}
		}
		Individual<chromType, fitType>* ind = newIndividual();
		ind->setChromosomes(data);
		return (ind);
	}

	Individual<chromType, fitType>* newIndividual(){
		Individual<chromType, fitType>* ind;
		if (pool.size() > 0){
			ind = pool.back();//pool[pool.size() - 1];
			ind->setReborn();
			pool.pop_back();
		}else{
			ind = new Individual<chromType, fitType>();
		}
		return ind;
	}

	void deleteIndividuals(std::vector<Individual<chromType, fitType>*> &individuals){
		pool.insert(pool.end(), individuals.begin(), individuals.end());
		individuals.clear();
	}

	void deleteIndividual(Individual<chromType, fitType>* individual){
		pool.push_back(individual);
	}


private:
	std::vector<Individual<chromType, fitType>*> pool;
};



#endif /* GENETIC_ALGORITHM_POPULATIONMANAGER_H_ */

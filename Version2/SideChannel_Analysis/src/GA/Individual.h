
#ifndef GENETIC_ALGORITHM_INDIVIDUAL_H_
#define GENETIC_ALGORITHM_INDIVIDUAL_H_

#include "../helpers/Statistics.h"
#include "../dataClasses/ProfilerMetrics.h"


template <typename chromType = char, typename fitType = double>
class Individual{
public:
	Individual(const std::vector<chromType> &data):fitness(0), neighbors(0), histoValue(0){
		chromosomes = data;
		metrics = NULL;
	}
	Individual():fitness(0), neighbors(0), histoValue(0){
		metrics = NULL;
	};
	Individual(const Individual<chromType, fitType>* ind): fitness(0), neighbors(0), histoValue(0), chromosomes(ind->chromosomes){
		metrics = NULL;
	}

	virtual void setChromosomes(const std::vector<chromType> data){
		chromosomes.clear();
		chromosomes.insert(chromosomes.begin(), data.begin(), data.end());
	}
	virtual const std::vector<chromType> getChromosomes(){
		return chromosomes;
	}

	virtual fitType getFitness() const{
		return (fitness);
	}
	virtual int getNeighbors() const{
		return (neighbors);
	}
	virtual void setNeighbors(int neighbors){
		this->neighbors = neighbors;
	}
	virtual int getChromosome(unsigned pos){
		return chromosomes[pos];
	}
	virtual void addChromosome(chromType value){
		chromosomes.push_back(value);
	}
	virtual void setChromosome(chromType chromosome, unsigned pos){
		chromosomes[pos] = chromosome;
	}

	virtual unsigned getGenotypeSize() const{
		return chromosomes.size();
	}
	virtual void computeFitness(FITNESS_TEMPLATE fitnessFunction, EncryptionSetupSettings *settings){
		if (isNewlyBorn()){ // don't run again if we executed this before
			if (metrics != NULL){
				delete metrics;
			}
			//			Statistics::addExecutions(1); //TODO make it local to each population
			//			GlobalLogger::getLogger() << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>   " <<
			//					"Execution number " << Statistics::getTotalExecutions() <<
			//					"   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" <<
			//					std::endl;
			fitness = fitnessFunction(chromosomes, &metrics, settings, 0);
			setOld();
		}
	}
	virtual ~Individual(){delete metrics;};
	bool isNewlyBorn(){
		return (compute);
	}
	void setOld(){
		compute = false;
	}
	void setReborn(){
		compute = true;
	}

	static chromType getRandChrom(){
		return (rand() % 256); //maybe get a randGenerator there
	}

	virtual fitType getPoolFitness() const{
		return (poolFitness);
	}

	virtual void setPoolFitness(int poolFit){
		poolFitness = poolFit;
	}

	virtual fitType getHistogramValue() const{
		return (histoValue);
	}

	virtual void setHistogramValue(unsigned int histogramValue){
		histoValue = histogramValue;
	}

protected:
	fitType fitness;
	int neighbors;
	unsigned int histoValue;
	bool compute = true;
	int poolFitness = 0;

private:
	std::vector<chromType> chromosomes;
	ProfilerMetrics<fitType> *metrics = NULL;

};



#endif /* GENETIC_ALGORITHM_INDIVIDUAL_H_ */


#ifndef GA_SCENARIOS_H_
#define GA_SCENARIOS_H_

#include <string>
#include <vector>
#include <random>
#include <algorithm>
#include <functional>
#include <set>
#include "GASettings.h"
#include "AntiGAPopulation.h"
#include "PopulationManager.h"
#include "FitnessFunctions.h"
#include "../dataClasses/SetupSettings.h"


template <typename chromType = char, typename fitType = double>
void AntiGA_run(int argc, char** argv,
		//GASettings *settings, EncryptionSetupSettings *setup,
		const std::string &GA_settings_path, EncryptionSetupSettings *setupSettings,
		FITNESS_TEMPLATE fitnessFunction){

	GASettings* settingsGA = readSettings(GA_settings_path);

	PopulationManager<chromType, fitType>* manager = new PopulationManager<chromType, fitType>();
	AntiGAPopulation<chromType, fitType>* population;
	population = new AntiGAPopulation<chromType, fitType>(manager, settingsGA, setupSettings);

	population->runGeneticAlgorithm(setupSettings->getKeyByteSize(), fitnessFunction);

	Statistics::printEntries(setupSettings->getPlotPath()+".keysHistogram");
	Statistics::printObservations();

	delete population;
}


#endif /* GA_SCENARIOS_H_ */

/*
 * FitnessFunctions.h
 *
 *  Created on: Sep 21, 2018
 *      Author: adrianh
 */

#ifndef GA_FITNESSFUNCTIONS_H_
#define GA_FITNESSFUNCTIONS_H_

#include <vector>
#include <iomanip>
#include <iostream>
#include "../dataClasses/ProfilerMetrics.h"

template <typename chromType = char, typename fitType = double>
fitType mock(std::vector<chromType> chromosomes, ProfilerMetrics<fitType> *metrics){
	fitType fitness;

	metrics = new ProfilerMetrics<fitType>();

	fitness = 1+ rand() % 99;
	return fitness;

}



#endif /* GA_FITNESSFUNCTIONS_H_ */

#ifndef GENETIC_ALGORITHM_ANTIGAPOPULATION_H_
#define GENETIC_ALGORITHM_ANTIGAPOPULATION_H_

#include <vector>
#include <random>
#include <algorithm>
#include <functional>
#include <set>
#include <map>

#include "../helpers/TimerHelper.h"
#include "../helpers/Statistics.h"
#include "../dataClasses/EncryptionSetupSettings.h"
#include "GASettings.h"
#include "Individual.h"
#include "PopulationManager.h"
#include "../helpers/DoubleLogger.h"


template <typename chromType = char, typename fitType = double>
inline bool antigaPopComparatorDescending(Individual<chromType, fitType>* x, Individual<chromType, fitType>* y){
	return (x->getPoolFitness() > y->getPoolFitness());
}

template <typename chromType = char, typename fitType = double>
inline bool antigaPopComparatorAscending(Individual<chromType, fitType>* x, Individual<chromType, fitType>* y){
	return (x->getPoolFitness() < y->getPoolFitness());
}

template <typename chromType = char, typename fitType = double>
inline bool antigaNeighborComparatorAscending(Individual<chromType, fitType>* x, Individual<chromType, fitType>* y){
	return (x->getNeighbors() < y->getNeighbors());
}

template <typename chromType = char, typename fitType = double>
inline bool antigaHistoComparatorAscending(Individual<chromType, fitType>* x, Individual<chromType, fitType>* y){
	return (x->getHistogramValue() < y->getHistogramValue());
}

template <typename chromType = char, typename fitType = double>
class AntiGAPopulation{
public:
	AntiGAPopulation(PopulationManager<chromType, fitType>* manager, GASettings* settings,
			EncryptionSetupSettings *setup):
				manager(manager), populationSettings(settings), setupSettings(setup){
	}
	~AntiGAPopulation(){
		delete manager;
		//delete populationSettings;
		//delete individuals
		for (Individual<chromType, fitType> *ind : individuals){
			delete ind;
		}
		individuals.clear();
	}
	void computeFitness(FITNESS_TEMPLATE fitnessFunction){
		for (auto indv : individuals){
			if (indv->isNewlyBorn()){
				indv->computeFitness(fitnessFunction, setupSettings);
			}
		}
	}
	void computeGroupFitness(std::vector<Individual<chromType, fitType>*> &group, FITNESS_TEMPLATE fitnessFunction){

		for (auto indv : group){
			if (indv->isNewlyBorn()){
				indv->computeFitness(fitnessFunction);
			}
		}
	}

	Individual<chromType, fitType>* getTopIndividual(){
		return (individuals[0]);
	}
	void sortByPoolFitness(){
		std::sort(individuals.begin(), individuals.end(), antigaPopComparatorDescending<chromType, fitType>);
	}


	unsigned findZeroPoint(const std::vector<Individual<chromType, fitType>*> &individuals){
		unsigned point = 0;
		for(auto ind : individuals){
			if (ind->getPoolFitness() == 0){
				break;
			}
			point++;
		}
		return point;
	}

	unsigned findAlphaEndPoint(const std::vector<Individual<chromType, fitType>*> &individuals){
		unsigned point = 0;
		for(auto ind : individuals){
			if (ind->getHistogramValue() > 0){
				break;
			}
			point++;
		}
		return point;
	}

	int randomBut(int notThis){
		if (populationSettings->getPopulationSize() == 1){
			return 0;
		}
		int value = rand() % populationSettings->getPopulationSize();
		while ((value == notThis) || (individuals[notThis] == individuals[value])){
			value = rand() % populationSettings->getPopulationSize();
		}
		return value;
	}

	int poolNeighbors(fitType value){
		int neighs = 0;
		//lower neighbor
		if (existingValues.find(value - 1) != existingValues.end()){
			neighs++;
		}
		//top neighbor
		if (existingValues.find(value + 1) != existingValues.end()){
			neighs++;
		}

		return neighs;
	}

	void mutate(Individual<chromType, fitType>* individual, bool force = false){
		if (force == false){
			double mutationChance = distribution(generator);
			GlobalLogger::getLogger() << "Mutating - mutation chance : " << mutationChance << std::endl;
			if (mutationChance < populationSettings->getMutationRate()){
				manager->mutate(individual);
			}
		}else{
			manager->mutate(individual);
		}
	}




	void mate_and_select(FITNESS_TEMPLATE fitnessFunction){
		std::vector<Individual<chromType, fitType>* > children;
		std::map<Individual<chromType, fitType>*, std::vector<Individual<chromType, fitType>* > > families;
		childrenCreation(fitnessFunction, families, children);

		std::vector<Individual<chromType, fitType>*> newGeneration;
		eliteSelection(newGeneration);

		//put children with initial pool fitness different than zero (alpha children)
		childrenSelection(children, newGeneration);

		//delete unused individuals
		cleanUpFamilies(children, families);

		placeNewGeneration(newGeneration);
	}
	void renewPopulation(unsigned sizeOfChromosomes){
		//7) no new values appeared -> renew the population, keeping some previous individuals
		GlobalLogger::getLogger() << "Renewing population " << std::endl;
		std::vector<Individual<chromType, fitType>*> newRandomGeneration;

		//keep elite and anti elite
		//unsigned int elite = populationSettings->getEliteRate() * populationSettings->getPopulationSize();
		for (unsigned i = 0; i < currentTop; ++i) {
			newRandomGeneration.push_back(individuals[i]);
		}
		//keep elite and anti elite, delete the rest
		for (unsigned i = currentTop; i < individuals.size(); ++i) {
			delete individuals[i]; // Calls ~object and deallocates *individuals[i]
		}
		individuals.clear();

		//generate new random individuals
		unsigned popSize = populationSettings->getPopulationSize();
		while (newRandomGeneration.size() != popSize){
			Individual<chromType, fitType>* individual = manager->generateRandomIndividual(sizeOfChromosomes);
			newRandomGeneration.push_back(individual);
		}
		//put the new generation in place of the current population
		individuals.insert(individuals.begin(), newRandomGeneration.begin(), newRandomGeneration.end());
		iterationsSinceLastChange = 0;
	}

	void initializePopulation(unsigned sizeOfChromosomes){
		//create the population
		int popSize = populationSettings->getPopulationSize();

		//	random individuals
		for (int i = 0; i < popSize; i++ ){
			Individual<chromType, fitType>* individual = manager->generateIndividual(sizeOfChromosomes);
			individuals.push_back(individual);
		}
	}

	void computeHistoValues(std::vector<Individual<chromType, fitType>*> &individuals){
		for (auto individual : individuals){
			auto ret = existingValues.find(individual->getFitness());
			if (ret == existingValues.end()){
				individual->setHistogramValue(0);
			}else{
				individual->setHistogramValue(histogram[individual->getFitness()]);
			}
		}
	}

	void saveToPool(std::vector<Individual<chromType, fitType>*> &individuals){
		//check if the value is in the pool
		for (auto individual : individuals){
			auto ret = existingValues.find(individual->getFitness());
			if (ret == existingValues.end()){
				individual->setPoolFitness(1); //new to the set
				histogram[individual->getFitness()] = 0;
			}else{
				individual->setPoolFitness(0); //exists in the set
			}
		}

		computeHistoValues(individuals);

		//put the individual in the pool
		for (auto individual : individuals){
			existingValues.insert(individual->getFitness());
			histogram[individual->getFitness()]++;
		}
	}

	int compareToPool(std::vector<Individual<chromType, fitType>*> &individuals){
		int newValues = 0;
		for (auto individual : individuals){
			auto it = existingValues.find(individual->getFitness());
			if (it == existingValues.end()){
				newValues++;
			}
		}
		return newValues;
	}

	int compareToFamily(std::vector<Individual<chromType, fitType>*> &individuals){
		std::set<fitType> familyValues;
		//		familyValues.clear();
		int newValues = 0;
		for (auto individual : individuals){
			familyValues.insert(individual->getFitness());
		}
		newValues = familyValues.size();

		return newValues;
	}

	bool inPool(Individual<chromType, fitType>* individual){
		if (existingValues.find(individual->getFitness()) == individual->end()){
			return false;
		}else{
			return true;
		}
	}

	void runGeneticAlgorithm(unsigned sizeOfChromosomes,
			FITNESS_TEMPLATE fitnessFunction){

		//1)init population
		initializePopulation(sizeOfChromosomes);

		//2)compute the single fitness values for the individuals
		computeFitness(fitnessFunction);
		saveToPool(individuals);
		//sortByFitness(); //group fitness

		iterations = 0;
		TimerHelper::startTimer();
		while(!stopCondition()){
			computeHistoValues(individuals);
			mate_and_select(fitnessFunction);
			updateVariation();
			if (!variationDetected()){
				renewPopulation(sizeOfChromosomes);
				computeFitness(fitnessFunction);
				saveToPool(individuals);
			}
			iterations++;
		}

		GlobalLogger::getLogger() << "AntiGA -> Total number of executed individuals " <<
				Statistics::getTotalExecutions() << std::endl;


		GlobalLogger::getLogger() << "AntiGA -> Set of distinct values " << std::endl;
		//print set values
		for (auto value : existingValues){
			GlobalLogger::getLogger() << value << std::endl;
		}
		GlobalLogger::getLogger() << "AntiGA -> Total number of distinct values " <<
				existingValues.size() << std::endl;

		GlobalLogger::getLogger() << "AntiGA -> Histogram values " << std::endl;
		//print histogram values
		for (auto pair : histogram){
			std::cout << pair.first << " -> " << pair.second << std::endl;
		}
		GlobalLogger::getLogger() << "AntiGA -> Total number of histogram keys " <<
				histogram.size() << std::endl;
	}

	bool stopCondition(){
		return iterationThreshhold();
		//		return timeThreshold();
		//		return variationThreshold();
	}

	bool iterationThreshhold(){
		if (iterations >= populationSettings->getMaximumIterations()){
			return true;
		}else{
			return false;
		}
	}

	bool timeThreshold(){
		double stopTimeThresholdSeconds = 60 * 60; //60 * 60 = 1 hour
		if (TimerHelper::getElapsedSeconds() >= stopTimeThresholdSeconds){
			return true;
		}else{
			return false;
		}
	}

	void updateVariation(){
		if (iterations == 0){
			iterationsSinceLastChange = 0;
			distinctValues = existingValues.size();
		}else
			if (distinctValues < existingValues.size()){
				iterationsSinceLastChange = 0;
				distinctValues = existingValues.size();
			}else
				if (iterationsSinceLastChange < populationSettings->getVariationThreshold()){
					iterationsSinceLastChange++;
				}
	}

	bool variationThreshold(){
		if (iterationsSinceLastChange < populationSettings->getVariationThreshold()){
			return false;
		}else{
			return true;
		}
	}


	bool variationDetected(){
		if (iterationsSinceLastChange >= populationSettings->getRenewStep()){
			return false;
		}else{
			return true;
		}
	}

private:
	std::vector<Individual<chromType, fitType>*> individuals;
	PopulationManager<chromType, fitType> *manager;
	GASettings *populationSettings;
	EncryptionSetupSettings * setupSettings;

	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution;


	std::set<fitType> existingValues;
	std::map<fitType, unsigned int> histogram;
	unsigned currentTop = 0;
	//std::set<fitType> familyValues;
	long int iterations = 0;
	long int iterationsSinceLastChange = 0;
	unsigned int distinctValues = 0;

	void eliteSelection(
			std::vector<Individual<chromType, fitType> *>& newGeneration) {
		std::cout << "Elite selection stage : " << std::endl;
		unsigned elite = populationSettings->getEliteRate()	* populationSettings->getPopulationSize();
		sortByPoolFitness(); // sort parents based on their family
		//print by pool fitness
		GlobalLogger::getLogger() << "Sorted individuals by pool : " << std::endl;
		for (auto ind : individuals) {
			GlobalLogger::getLogger() << ind->getPoolFitness() << std::endl;
		}
		std::set<fitType> eliteValues;
		unsigned i = 0;
		while (i < elite) {
			newGeneration.push_back(individuals[0]);
			individuals[0]->setPoolFitness(0); //reset the parent pool since it was already put there
			eliteValues.insert(individuals[0]->getFitness());
			individuals.erase(individuals.begin());
			i++;
		}
		//anti elite selection
		GlobalLogger::getLogger() << "Anti elite selection stage : " << std::endl;
		i = 0;
		for (unsigned j = 0; j < individuals.size(); j++) {
			if (i < elite) {
				if (eliteValues.find(individuals[j]->getFitness())
						== eliteValues.end()) {
					eliteValues.insert(individuals[j]->getFitness());
					individuals[j]->setPoolFitness(0); //reset the anti elite pool since it was already put there
					newGeneration.push_back(individuals[j]);
					individuals.erase(individuals.begin() + j);
					j--;
					i++;
				}
			}
		}
		eliteValues.clear();
		//
		//selection based on neighbours
		//another elite% taken for top individuals based on neighbors
		std::cout << "Neighborless selection stage : " << std::endl;
		for (unsigned i = 0; i < individuals.size(); i++) {
			int neighs = poolNeighbors(individuals[i]->getFitness());
			individuals[i]->setNeighbors(neighs);
		}
		std::sort(individuals.begin(), individuals.end(),
				antigaNeighborComparatorAscending<chromType, fitType>);
		GlobalLogger::getLogger() << "Individuals sorted on neighbors : " << std::endl; // print them
		for (unsigned i = 0; i < individuals.size(); i++) {
			GlobalLogger::getLogger() << individuals[i]->getNeighbors() << std::endl;
		}
		i = 0;
		while (i < elite) {
			newGeneration.push_back(individuals[0]);
			individuals[0]->setPoolFitness(0); //reset the neighborless pool since it was already put there
			individuals.erase(individuals.begin());
			i++;
		}
		GlobalLogger::getLogger() << "Neighborless individuals inserted : " << i << std::endl;
		//
		//selection of elite based on histo values
		//		computeHistoValues(individuals);
		std::sort(individuals.begin(), individuals.end(),
				antigaHistoComparatorAscending<chromType, fitType>);
		i = 0;
		while (i < elite) {
			newGeneration.push_back(individuals[0]);
			individuals[0]->setPoolFitness(0); //reset the neighborless pool since it was already put there
			individuals.erase(individuals.begin());
			i++;
		}
		GlobalLogger::getLogger() << "Low histogram individuals inserted : " << i << std::endl;
		//
		currentTop = newGeneration.size();
		GlobalLogger::getLogger() << "Individuals selected so far : " << currentTop
				<< std::endl;
	}

	void childrenCreation(
			FITNESS_TEMPLATE fitnessFunction,
			std::map<Individual<chromType, fitType> *,
			std::vector<Individual<chromType, fitType> *> >& families,
			std::vector<Individual<chromType, fitType> *>& children) {
		int index = 0;
		for (auto parent : individuals) {
			GlobalLogger::getLogger() << "Creating family for individual : " << index
					<< std::endl;
			for (int i = 0; i < populationSettings->getTournamentSize(); i++) {
				auto concubine = individuals[randomBut(index)];
				Individual<chromType, fitType>* child1;
				manager->crossover(parent, concubine, &child1);
				mutate(child1);
				child1->computeFitness(fitnessFunction, setupSettings);
				families[parent].push_back(child1);
				children.push_back(child1);
			}
			//mutate the parent and place the result in the family
			auto* mutated1 = manager->newIndividual();
			mutated1->setChromosomes(parent->getChromosomes());//new Individual<chromType, fitType>(parent);
			mutate(mutated1, true);
			mutated1->computeFitness(fitnessFunction, setupSettings);
			families[parent].push_back(mutated1);
			children.push_back(mutated1);
			//			auto* mutated2 = manager->newIndividual();
			//			mutated2->setChromosomes(parent->getChromosomes());//new Individual<chromType, fitType>(parent);
			//			mutate(mutated2, true);
			//			mutated2->computeFitness(fitnessFunction, setupSettings);
			//			families[parent].push_back(mutated2);
			//			children.push_back(mutated2);
			GlobalLogger::getLogger() << "Parent fitness : " << parent->getFitness()
									<< std::endl;
			//			GlobalLogger::getLogger() << "Mutated1 fitness : " << mutated1->getFitness()
			//			<< std::endl;
			//			GlobalLogger::getLogger() << "Mutated2 fitness : " << mutated2->getFitness()
			//			<< std::endl;

			//int familyFitness = compareToFamily(family);
			//int poolFitness = compareToPool(family);
			int familyFitness = compareToFamily(families[parent]);
			int poolFitness = compareToPool(families[parent]);
			int initialFitness = parent->getPoolFitness();
			int totalParentFitness = initialFitness + poolFitness + familyFitness;
			GlobalLogger::getLogger() << "Family fitness : " << familyFitness
					<< " Pool fitness : " << poolFitness
					<< " Initial fitness : " << initialFitness << std::endl;
			parent->setPoolFitness(totalParentFitness);
			index++;
		}
		GlobalLogger::getLogger() << "Saving children to pool : " << std::endl;
		saveToPool(children); //save the values to the pool set
	}

	void childrenSelection(
			std::vector<Individual<chromType, fitType> *>& children,
			std::vector<Individual<chromType, fitType> *>& newGeneration) {
		//put children with initial pool fitness different than zero (alpha children)
		std::set<fitType> childrenSet;
		GlobalLogger::getLogger() << "Alpha children selection stage : " << std::endl;
		//		std::cout << "Before sort : " << std::endl;
		//		for (auto child : children){
		//			std::cout << child->getHistogramValue() << " " << child->getFitness() << std::endl;
		//		}
		std::sort(children.begin(), children.end(),
				antigaHistoComparatorAscending<chromType, fitType>);
		GlobalLogger::getLogger() << "After sort by histogram : " << std::endl;
		for (auto child : children){
			GlobalLogger::getLogger() << child->getHistogramValue() << " " << child->getFitness() << std::endl;
		}
		//		unsigned alphaEndPoint = std::min(
		//				populationSettings->getPopulationSize() - currentTop,
		//				findAlphaEndPoint(children));
		unsigned alphaEndPoint = findAlphaEndPoint(children);
		//find unique first
		unsigned index = 0;
		unsigned popSize = populationSettings->getPopulationSize();
		while ((index < alphaEndPoint) && (newGeneration.size() < popSize)){
			fitType fit = children[index]->getFitness();
			if (childrenSet.find(fit) == childrenSet.end()){
				newGeneration.push_back(children[index]);
				childrenSet.insert(fit);
				children.erase(children.begin() + index);
				alphaEndPoint--;
			}else{
				index++;
			}
		}
		GlobalLogger::getLogger() << "Unique alpha children inserted: "
				<< newGeneration.size() - currentTop << std::endl;
		int count = 0;
		for (auto val : childrenSet){
			GlobalLogger::getLogger() << count << " " << val << std::endl;
			count++;
		}
		//select the rest of the alpha children
		unsigned spaceLeft = populationSettings->getPopulationSize() - newGeneration.size();
		unsigned remainingAlpha = alphaEndPoint;
		alphaEndPoint = std::min(spaceLeft, remainingAlpha);
		if (alphaEndPoint > 0){
			newGeneration.insert(newGeneration.end(), children.begin(),
					children.begin() + alphaEndPoint);
			children.erase(children.begin(), children.begin() + alphaEndPoint);
		}
		GlobalLogger::getLogger() << "Total alpha children inserted : "
				<< newGeneration.size() - currentTop << std::endl;
		//currentTop = newGeneration.size();
		//put the next children in the rest of the generation
		//std::cout << "Non-alpha children selection stage : " << std::endl;
		int remaining = populationSettings->getPopulationSize() - newGeneration.size();
		GlobalLogger::getLogger() << "Non-alpha children left to insert : " << remaining
				<< std::endl;
		//		while (remaining > 0) {
		//			int pos = rand() % children.size();
		//			auto child = children[pos];
		//			newGeneration.push_back(child);
		//			children.erase(children.begin() + pos);
		//			remaining--;
		//		}
		if (remaining > 0){
			newGeneration.insert(newGeneration.end(), children.begin(),
					children.begin() + remaining);
			children.erase(children.begin(), children.begin() + remaining);
		}
	}

	void cleanUpFamilies(
			std::vector<Individual<chromType, fitType> *>& children,
			std::map<Individual<chromType, fitType> *,
			std::vector<Individual<chromType, fitType> *> >& families) {
		//delete unused individuals
		GlobalLogger::getLogger() << "Delete remaining " << individuals.size() << " individuals : " << std::endl;
		manager->deleteIndividuals(individuals);

		GlobalLogger::getLogger() << "Delete remaining " << children.size() << " children : " << std::endl;
		manager->deleteIndividuals(children);
		families.clear();
	}

	void placeNewGeneration(
			const std::vector<Individual<chromType, fitType> *>& newGeneration) {
		GlobalLogger::getLogger() << "Insert new generation : " << std::endl;
		individuals.insert(individuals.begin(), newGeneration.begin(),
				newGeneration.end());
	}
};

#endif /* GENETIC_ALGORITHM_ANTIGAPOPULATION_H_ */

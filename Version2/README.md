**GDivAn**

**GDivAn** is an measurement based execution time and side-channel analysis tool for GPU kernels (CUDA programs) that uses symbolic execution(SE) and genetic algorithm(GA) to navigate the possible input space and direct the search toward the worst case execution time(WCET) and to detect the side-channel leakage capacity.

The provided code is split into 2 main parts:

1. WCET_Analysis
2. SideChannel_Analysis

---

## 0. Radamsa and OpenSSL
The code for this version uses the tool [Radamsa](https://gitlab.com/akihe/radamsa). Download and install it on your machine before testing the *Radamsa* scenarios.

For the **SideChannel_Analysis**, the GPU implementation of the OpenSSL cryptographic algorithms has been used. The project is title [engine-cuda](https://github.com/heipei/engine-cuda). We have added the installation folder called *engine-cuda_installer*. We have used the openssl-1.0.1e version, which is provided in the folder. Please refer to the [engine-cuda](https://github.com/heipei/engine-cuda) page for information on how to install it.

---

## 1. WCET_Analysis
The updated code contains the added *Convolution* kernel test.

The updated code in this version (i.e. *Version2*) contains the addition of the **Radamsa** type scenario.

Please refer to *Version1* folder on how the WCET_Analysis works.

---

## 2. SideChannel_Analysis
For this type of analysis we have tested the implementation of AES from the *ISPASS* benchmark. The folder *AES* contains the implementations.
Also, the *OpenSSL* implementations of AES, BlowFish, Camilla and CAST5 are provided in the *engine-cuda* folder.

The *ISPASS* version can simply be built by running "make" in the *AES* folder.
The *OpenSSL* version needs to be installed in the system following the instructions from [engine-cuda](https://github.com/heipei/engine-cuda).

GDivAn currently provides 3 types scenarios (GA, random, Radamsa) for analyzing: AES_128_ISPASS, AES_256_ISPASS, AES_128_OpenSSL, AES_256_OpenSSL, BlowFish_128_OpenSSL,
Camellia_128_OpenSSL and CAST5_128_OpenSSL.

The different scenarios can be selected from *Main.cpp* in the *SideChannel_Analysis/src* folder.
Make sure to recompile before running with the wanted scenario(s).

**Running GDivAn**

In order to run, follow the steps:

0. Install *Radamsa*
1. Install the provided **engine-cuda_installer** according to the instructions
2. Build *AES* (*ISPASS* version)
3. Build the *Release*  -- run "make" in *Release* folder 
3. Run **SideChannel**

**Results**

The results are stored in folders for each program (AES, Cast5, etc.) and inside of these , folders for scenario type (GA, random, Radamsa).
Each time a scenario for a kernel is executed the folders are created (if they don't exist).
The result files are stored inside that run folder.

**Settings files**

1. settings.txt : should contain the values for the GA parameters (population size, iterations, etc.)

---

## Observations

The code has been developed and tested on a Linux distribution (Ubuntu 16.04) using gcc 5.4 and the kernels have been executed on a NVIDIA GeForce GTX 1060M using CUDA 8.0.

---

### Bugs & issues

For reporting bugs or any other issues please contact [Adrian Horga](mailto:adrian.horga@liu.se).

################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/gklee_helpers/GkleeData.cc \
../src/gklee_helpers/GkleeFileHandler.cc \
../src/gklee_helpers/GkleeHandler.cc \
../src/gklee_helpers/KernelInfo.cc 

CC_DEPS += \
./src/gklee_helpers/GkleeData.d \
./src/gklee_helpers/GkleeFileHandler.d \
./src/gklee_helpers/GkleeHandler.d \
./src/gklee_helpers/KernelInfo.d 

OBJS += \
./src/gklee_helpers/GkleeData.o \
./src/gklee_helpers/GkleeFileHandler.o \
./src/gklee_helpers/GkleeHandler.o \
./src/gklee_helpers/KernelInfo.o 


# Each subdirectory must supply rules for building sources it contributes
src/gklee_helpers/%.o: ../src/gklee_helpers/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++1y -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



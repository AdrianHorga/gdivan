

#include <stdio.h>
#include <stdlib.h>

__global__ static void controlled(int* values, int* result)
{

	const int tid = blockIdx.x * blockDim.x + threadIdx.x;

	if (values[tid] == 6){
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
	}else{
		atomicAdd(&result[tid], 10);;
	}
}


int main(int argc, char** argv)
{
   // int BLOCKS = 512;
   // int THREADS = 512;
    int total = THREADS * BLOCKS;
    int *values;
    int *result;

    values = (int*)malloc(sizeof(int) * total);
    result = (int*)malloc(sizeof(int) * total);    
    
    klee_make_symbolic(values, sizeof(int) * total, "values");
   /* for (int i = 0 ; i < total; i++){
        klee_assume(values[i] >= 0);
        klee_assume(values[i] < THREADS);
    }*/
    
    int * dvalues, *dresult;
    cudaMalloc((void**)&dvalues, sizeof(int) * total);
    cudaMalloc((void**)&dresult, sizeof(int) * total);
    cudaMemcpy(dvalues, values, sizeof(int) * total, cudaMemcpyHostToDevice);

			 
	controlled<<<BLOCKS, THREADS>>>(dvalues, dresult);

    
    cudaMemcpy(result, dresult, sizeof(int) * total, cudaMemcpyDeviceToHost);

    cudaFree(dvalues);
    cudaFree(dresult);  

    free(values);
    free(result);

}

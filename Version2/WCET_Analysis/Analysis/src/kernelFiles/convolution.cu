
#include <stdio.h>
#include <stdlib.h>

__global__ static void convolution(int* values, int *filter, int* result, int n, int diameter)
{
    const int tid = blockIdx.x * blockDim.x + threadIdx.x;
    
    const int filterSize = (values[tid] % filter[tid % diameter]) % diameter;
    
    if (tid < (n - filterSize)){
      int sum = 0;
      
      for (int j = 0 ; j <= filterSize; j++){
        if (diameter < blockDim.x){
            for (int k = j + 1 ; k <= filterSize; k++){
                    sum += values[tid + k] * filter[k] 
                            + values[tid + k] % (filter[k] + 1)
                            + filter[k] % (values[tid + k] + 1);
            }
        }
        sum += values[tid + j] * filter[j] 
		+ values[tid + j] % (filter[j] + 1)
		+ filter[j] % (values[tid + j] + 1);
        }
        
      result[tid] = sum;
    }   
	
}


int main(int argc, char** argv)
{
   // int BLOCKS = 512;
   // int THREADS = 512;
    int total = THREADS * BLOCKS;
    int diameter = 4;
    int *values;
    int *result;
    int *filter;

    values = (int*)malloc(sizeof(int) * total);
    result = (int*)malloc(sizeof(int) * total);    
    filter = (int*)malloc(sizeof(int) * diameter);
    
    klee_make_symbolic(values, sizeof(int) * total, "values");
    for (int i = 0 ; i < total; i++){
        klee_assume(values[i] >= 0);
        klee_assume(values[i] < 32000);
    }
    
    for (int i = diameter; i > 0; i--){
        filter[i - 1] = i;
    }
    
    int * dvalues, *dfilter, *dresult;
    cudaMalloc((void**)&dvalues, sizeof(int) * total);
    cudaMalloc((void**)&dresult, sizeof(int) * total);
    cudaMalloc((void**)&dfilter, sizeof(int) * diameter);
    cudaMemcpy(dvalues, values, sizeof(int) * total, cudaMemcpyHostToDevice);
    cudaMemcpy(dfilter, filter, sizeof(int) * diameter, cudaMemcpyHostToDevice);
	
	convolution<<<BLOCKS, THREADS>>>(dvalues, dfilter, dresult, total, diameter);

    cudaMemcpy(result, dresult, sizeof(int) * total, cudaMemcpyDeviceToHost);
    
    cudaFree(dvalues);
    cudaFree(dresult);  
    cudaFree(dfilter);  

    free(values);
    free(result);
    free(filter);

}

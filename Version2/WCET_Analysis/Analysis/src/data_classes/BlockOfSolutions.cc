/*
 * BlockOfSolutions.cc
 *
 *      Author: adrianh
 */

#include <iostream>

#include "BlockOfSolutions.h"

template <typename T>
BlockOfSolutions<T>::BlockOfSolutions(unsigned path, float divergence, int instructions):
pathNumber(path), divergence(divergence), executedInstructions(instructions){}

template <typename T>
void BlockOfSolutions<T>::addPathSolution(PathSolution<T>* solution){
	solutions.push_back(solution);
}

template <typename T>
BlockOfSolutions<T>::~BlockOfSolutions(){
	for (unsigned i = 0; i < solutions.size(); i++){
		PathSolution<T>* sol = solutions.at(i);
		//sol->printData();
		delete sol;
	}
	solutions.clear();
}

template <typename T>
void BlockOfSolutions<T>::printData(){
	std::cout << "Path number : " << pathNumber << " with divergence : " << divergence
			<< " and exec. instructions : " << executedInstructions << std::endl;
	int i = 0;
	for (auto el : solutions){
		std::cout << "Solution : " << i++ << std::endl;
		el->printData();
	}
}

template <typename T>
float BlockOfSolutions<T>::getDivergence() const{
	return (divergence);
}

template <typename T>
int BlockOfSolutions<T>::getExecutedInstructions() const{
	return (executedInstructions);
}

template <typename T>
PathSolution<T>* BlockOfSolutions<T>::pickARandomSolution(){
	unsigned pos = rand() % solutions.size();
	return (solutions[pos]);
}

template <typename T>
PathSolution<T>* BlockOfSolutions<T>::pickSolution(int pos){
	return (solutions[pos]);
}

template <typename T>
int BlockOfSolutions<T>::getNumberOfSolutions(){
	return solutions.size();
}

template class BlockOfSolutions<int>;
template class BlockOfSolutions<double>;
template class BlockOfSolutions<float>;


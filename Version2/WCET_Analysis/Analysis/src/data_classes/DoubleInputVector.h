/*
 * DoubleInputVector.h
 *
 *      Author: adrianh
 */

#ifndef DATA_CLASSES_DOUBLEINPUTVECTOR_H_
#define DATA_CLASSES_DOUBLEINPUTVECTOR_H_

#include "InputVector.h"

class DoubleInputVector:public InputVector{
public:
	std::string getType() const;
	unsigned getSize() const;
	~DoubleInputVector();

private:

};



#endif /* DATA_CLASSES_DOUBLEINPUTVECTOR_H_ */

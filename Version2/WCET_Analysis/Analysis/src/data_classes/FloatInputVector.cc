/*
 * FloatInputVector.cc
 *
 *      Author: adrianh
 */

#include "FloatInputVector.h"

std::string FloatInputVector::getType() const{
	return ("float");
}

unsigned FloatInputVector::getSize() const{
	return (sizeof(float));
}

FloatInputVector::~FloatInputVector(){}


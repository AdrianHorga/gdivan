/*
 * PathSolution.h
 *
 *      Author: adrianh
 */

#ifndef DATA_CLASSES_PATHSOLUTION_H_
#define DATA_CLASSES_PATHSOLUTION_H_

#include <map>
#include <vector>

template <typename T>
class PathSolution{
public:
	void addNewInputVector(std::string name, std::vector<T> values);
	void addNewValue(std::string name, T value);
	std::map<std::string, std::vector<T> > getInputVectors();
	char getSizeOfType();
	void printData();
	unsigned getNumberOfInputVectors();

private:
	std::map<std::string, std::vector<T> > inputVectors;

};

#endif /* DATA_CLASSES_PATHSOLUTION_H_ */

/*
 * PathSolution.cc
 *
 *      Author: adrianh
 */

#include <iostream>

#include "PathSolution.h"

template <typename T>
void PathSolution<T>::addNewInputVector(std::string name, std::vector<T> values){
	inputVectors[name] = values;
}

template <typename T>
void PathSolution<T>::addNewValue(std::string name, T value){
	inputVectors[name].push_back(value);
}

template <typename T>
char PathSolution<T>::getSizeOfType(){
	return (sizeof(T));
}

template <typename T>
void PathSolution<T>::printData(){
	for (auto el : inputVectors){
		std::cout << "Name : " << el.first << " ";
		for (auto val : el.second){
			std::cout << val << " ";
		}
		std::cout << std::endl;
	}
}

template <typename T>
unsigned PathSolution<T>::getNumberOfInputVectors(){
	return (inputVectors.size());
}

template <typename T>
std::map<std::string, std::vector<T> > PathSolution<T>::getInputVectors(){
	return (inputVectors);
}

template class PathSolution<int>;
template class PathSolution<double>;
template class PathSolution<float>;



/*
 * GKLEEData.cc
 *
 *      Author: adrianh
 */

#include <iostream>

#include "GkleeData.h"

template <typename T>
GkleeData<T>::~GkleeData(){
	for (unsigned i = 0; i < pathSolutions.size(); i++){
		//pathSolutions[i]->printData();
		delete pathSolutions[i];
	}
	pathSolutions.clear();
}

template <typename T>
void GkleeData<T>::addBlockOfSolutions(BlockOfSolutions<T>* block){
	pathSolutions.push_back(block);
}

template <typename T>
std::vector<BlockOfSolutions<T>*> GkleeData<T>::getBlocks(){
	return pathSolutions;
}

template <typename T>
void GkleeData<T>::setupForRoulette(){
	int translate = 100;
	unsigned long long total = 0;

	for (auto el : pathSolutions){
		unsigned divergence = el->getDivergence();
		total += divergence + translate;
	}

	double previousDivergence = 0;
	for (auto el : pathSolutions){
		const unsigned divergence = el->getDivergence() + translate;
		const double weight = (double)(divergence)/total;
		previousDivergence += weight;

		weights.push_back(previousDivergence);
	}

}

template <typename T>
PathSolution<T>* GkleeData<T>::pickARoulletteSolution(){
	double generatedWeight = distribution(generator);

	//
	//std::cout << "generated weight : " << generatedWeight << std::endl;
	//get the block of solutions (path number)
	unsigned pos = -1;
	bool done = false;
	while (!done){
		pos++;
		if (generatedWeight < weights[pos]){
			done = true;;
		}
	}

	//std::cout << "generated weight : " << generatedWeight << " picked position : " << pos << std::endl;
	//picked one block of solutions
	BlockOfSolutions<T>* block  = pathSolutions[pos];

	//now pick a random one
	PathSolution<T> *sol = block->pickARandomSolution();

	return (sol);
}

template <typename T>
PathSolution<T>* GkleeData<T>::pickARandomSolution(){
	double generatedWeight = distribution(generator);

	unsigned pos = rand() % pathSolutions.size();
	//picked one block of solutions
	BlockOfSolutions<T>* block  = pathSolutions[pos];

	//now pick a random one
	PathSolution<T> *sol = block->pickARandomSolution();

	return (sol);
}

template <typename T>
void GkleeData<T>::printData(){
	int i = 0;
	for (auto el : pathSolutions){
		std::cout << "-----------------------------------------------------------------------" << std::endl;
		std::cout << "Weight : " << weights[i++] << std::endl;
		el->printData();
		std::cout << "-----------------------------------------------------------------------" << std::endl;
	}
}

template class GkleeData<int>;
template class GkleeData<double>;
template class GkleeData<float>;

/*
 * GKLEEConstants.h
 *
 *      Author: adrianh
 */

#ifndef GKLEE_HELPERS_GKLEECONSTANTS_H_
#define GKLEE_HELPERS_GKLEECONSTANTS_H_

#define SETUP_FILE "gklee-setup.txt"
#define INPUT_FOLDER "./inputs/"
#define DETAIL_FILE "details.txt"
#define COVERAGE_FILE "coverage.txt"
#define TOTALS_FILE "total.txt"


#endif /* GKLEE_HELPERS_GKLEECONSTANTS_H_ */

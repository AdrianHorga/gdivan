/*
 * GKLEEFileHandler.h
 *
 *      Author: adrianh
 */

#ifndef GKLEE_HELPERS_GKLEEFILEHANDLER_H_
#define GKLEE_HELPERS_GKLEEFILEHANDLER_H_

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

#include "GkleeConstants.h"
#include "KernelInfo.h"

class GkleeFileHandler{
public:
	void writeSetupFile(unsigned const bound) const;
	unsigned readCoverageFile() const;
	template <typename T> void readDetailFile(unsigned total, KernelInfo<T> *kernelFile) const;
	unsigned readTotalFile();
	void setSetupFilePath(std::string path);
	void setDetailFilePath(std::string path);
	void setCoverageFilePath(std::string path);
	void setTotalsFilePath(std::string path);

private:

	std::string setupFilePath = std::string(SETUP_FILE);
	std::string detailFilePath = std::string(INPUT_FOLDER) + std::string(DETAIL_FILE);
	std::string coverageFilePath = std::string(INPUT_FOLDER) + std::string(COVERAGE_FILE);
	std::string totalsFilePath = std::string(INPUT_FOLDER) + std::string(TOTALS_FILE);
};



//template <> void GkleeFileHandler::readDetailFile<int>(unsigned total, KernelInfo<int> &kernelFile) const;
//template <> void GkleeFileHandler::readDetailFile<float>(unsigned total, KernelInfo<float> &kernelFile) const;
//template <> void GkleeFileHandler::readDetailFile<double>(unsigned total, KernelInfo<double> &kernelFile) const;


template<typename T>
std::string numberToString(T number){
	std::ostringstream oss;
	oss << number;
	return (oss.str());
}

template <typename T>
void GkleeFileHandler::readDetailFile(unsigned total, KernelInfo<T> *kernelFile) const{
	GkleeData<T> *data = new GkleeData<T>();

	std::ifstream gkleeDetailsFile;
	gkleeDetailsFile.open(detailFilePath.c_str());
	if (gkleeDetailsFile.is_open()){
		unsigned path = 0;
		unsigned solutions = 0;
		float divergence = 0;
		int executedInstructions = 0;

		//go through the solution files
		for (unsigned it = 0; it < total; it++){
			gkleeDetailsFile >> path >> solutions >> divergence >> executedInstructions;
			std::cout << "Path : " << path << " Solutions : " << solutions
					<< " Divergence : " << divergence << " Exec. instructions : " << executedInstructions << std::endl;
			BlockOfSolutions<T> *block = new BlockOfSolutions<T>(path, divergence, executedInstructions);
			for (unsigned i = 0; i < solutions; i++){
				std::string solutionFilePath = std::string(INPUT_FOLDER) + numberToString<>(path) + "_" + numberToString<>(i);
				std::ifstream solutionFile;

				//get each solution for the current path
				solutionFile.open(solutionFilePath.c_str());
				if (solutionFile.is_open()){
					//read the total number of input vectors
					unsigned numberOfInputs;
					solutionFile >> numberOfInputs;

					std::cout << "Input vectors to read from " << solutionFilePath << " : " << numberOfInputs << std::endl;
					//read each input vector
					PathSolution<int> *pathSolution = new PathSolution<int>();
					for (unsigned j = 0; j < numberOfInputs; j++ ){
						std::string name;
						unsigned itemsToRead;
						unsigned realSize;

						solutionFile >> name;
						solutionFile >> itemsToRead;
						char sizeOfElement = kernelFile->getInputSize(name);
						realSize = itemsToRead / sizeOfElement;

						std::cout << "Input : " << name << std::endl;
						for (unsigned k = 0; k < realSize ; k++){
							//depending on the type of "element", the representation might differ,
							//but the values bitwise are correct
							int element = 0;
							for (char l = 0; l < sizeOfElement; l++){
								unsigned int piece;
								unsigned char eightBit;
								solutionFile >> piece;
								eightBit = piece & 255;
								unsigned howMany = 8 * l;
								unsigned shifted = eightBit << howMany;
								element |= shifted;
							}
							//std::bitset<32> bitElement(element);
							//std::cout << element << "(" << bitElement << ")" << " ";
							/*if ((element >> (sizeof(element)*8 - 1) & 1) == 1){
									element = (~(element-1)) * (-1);
								}*/
							//double el = 0XFFFFFFFFFFFFFFFF & element; //
							//std::cout << element << "(" << el << ")" << " ";
							std::cout << element << " ";
							pathSolution->addNewValue(name, element);
						}
						//block->addPathSolution(pathSolution); WTF!?!?!?!
						std::cout << std::endl;
					}
					block->addPathSolution(pathSolution);
					solutionFile.close();
				}else{
					std::cerr << "Could not open file : " + solutionFilePath;
				}
			}
			data->addBlockOfSolutions(block);
		}
		gkleeDetailsFile.close();
	}else{
		std::cerr << "Could not open file : " + detailFilePath;
	}
	kernelFile->addGkleeData(data);
}


#endif /* GKLEE_HELPERS_GKLEEFILEHANDLER_H_ */

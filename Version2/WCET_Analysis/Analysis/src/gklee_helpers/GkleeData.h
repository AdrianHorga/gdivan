/*
 * GKLEEData.h
 *
 *      Author: adrianh
 */

#ifndef GKLEE_HELPERS_GKLEEDATA_H_
#define GKLEE_HELPERS_GKLEEDATA_H_



#include <vector>
#include <map>
#include <random>

#include "../data_classes/BlockOfSolutions.h"
#include "../data_classes/PathSolution.h"


template <typename T>
class GkleeData{
public:
	~GkleeData();
	void addBlockOfSolutions(BlockOfSolutions<T>* block);
	void setupForRoulette();
	PathSolution<T>* pickARoulletteSolution();
	PathSolution<T>* pickARandomSolution();
	std::vector<BlockOfSolutions<T>*> getBlocks();
	void printData();

private:
	std::vector<BlockOfSolutions<T>*> pathSolutions;
	std::vector<double> weights;

	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution;

};



#endif /* GKLEE_HELPERS_GKLEEDATA_H_ */

/*
 * BFS_tests.h
 *
 *      Author: adrianh
 */

#ifndef TEST_SCENARIOS_BFS_TESTS_H_
#define TEST_SCENARIOS_BFS_TESTS_H_

PopulationManager* gklee_ga_setup_bfs(unsigned &sizeOfChromosomes, std::string address, std::string port){

	int initialThreads;
	unsigned expectedCoverage;
	unsigned bound;
	CommunicationPattern<int> *pattern;
	std::string baseOutputFolder = "./gklee_ga/";

	std::string outputFolder = baseOutputFolder + "bfs/" + TimerHelper::getCurrentTime();
	KernelInfo<int> *kernelFile = new KernelInfo<int>("bfs", "./src/kernelFiles/bfs.cu",
			"./bfs", KernelCode::BFS, outputFolder);
	initialThreads = 2;//4; - 16 variable -- too much time //2 working one
	int N = 1024;
	int edges = (initialThreads * (initialThreads)); //(initialThreads * (initialThreads - 1));
	sizeOfChromosomes = ((N * N)/(initialThreads * initialThreads));
	expectedCoverage = 50;
	bound = 4;
	pattern = new CommunicationPatternBFS<int>(address, port, initialThreads, edges, sizeOfChromosomes * edges);
	kernelFile->setSenderPattern(pattern);


	kernelFile->setParameter("BLOCKS", "1");
	kernelFile->setParameter("THREADS", "1");
	kernelFile->addInputSize("values", sizeof(int));
	kernelFile->addInputSize("graph_edges", sizeof(int));


	unsigned threadsForExpectedCoverage;

	//running Gklee to get base chromosomes
	bool execGKLEE = true;
	runGKLEE(kernelFile, expectedCoverage, bound, &threadsForExpectedCoverage, initialThreads, execGKLEE);
	TimerHelper::stopTimer();
	std::cout << "GKLEE finished after: " << TimerHelper::getElapsedSeconds() << " seconds"<< std::endl;
	//kernelFile.printData();

	PopulationManager* manager = new PopulationManagerGklee<int>(kernelFile);

	return manager;
}

PopulationManager* random_ga_setup_bfs(unsigned &sizeOfChromosomes, std::string address, std::string port){
	RunType runtype = RunType::RANDOM;

	std::cout << ">>>>>>>>>>>>>>>>>Start of RANDOM setup>>>>>>>>>>>>>>>>>" << std::endl << std::endl;

	Job *job;

	unsigned totalSize = 1024;
	sizeOfChromosomes = totalSize * totalSize;
	job = new BFSJob("random_ga",0, 0, 1, totalSize);

	job->setupFolders();
	job->cleanFolders();
	job->createSamples(10);
	//job->runJob(step, address, port, runtype);


	PopulationManager *manager = new PopulationManagerRandom(job, address, port, runtype);

	std::cout <<  ">>>>>>>>>>>>>>>>>End of RANDOM setup>>>>>>>>>>>>>>>>>" << std::endl << std::endl;

	return manager;
}

void random_bfs(std::string address, std::string port){
	RunType runtype = RunType::RANDOM;

	double executionTime = 0;
	float targetedFitness = 0;
	int iterations = 1;//2000;
	int totalSize;
	int step = 100;

	Job *job;

	totalSize = 1024;
	job = new BFSJob("random",0, 0, iterations, totalSize);

	std::cout << "Size " << totalSize << "; Iterations " << iterations << "; Step " << step << std::endl << std::endl;

	job->setupFolders();
	job->cleanFolders();
	job->createSamples(10);
	job->runJob(step, address, port, runtype);
}

void radamsa_bfs(std::string address, std::string port){
	RunType runtype = RunType::RADAMSA;

	double executionTime = 0;
	float targetedFitness = 0;
	int iterations = 1;//2000;
	int totalSize;
	int step = 100;

	Job *job;

	totalSize = 1024;
	job = new BFSJob("radamsa",0, 0, iterations, totalSize);

	std::cout << "Size " << totalSize << "; Iterations " << iterations << "; Step " << step << std::endl << std::endl;

	job->setupFolders();
	job->cleanFolders();
	job->createSamples(10);
	job->runJob(step, address, port, runtype);
}

#endif /* TEST_SCENARIOS_BFS_TESTS_H_ */

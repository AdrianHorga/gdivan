/*
 * Job.h
 *
 *      Author: adrianh
 */

#ifndef HELPERS_JOB_H_
#define HELPERS_JOB_H_

#include <string>
#include <vector>
#include "Helpers.h"

enum RunType {RANDOM, RADAMSA};

std::string getExtesion(RunType runtype);
std::string getExtesion(RunType runtype, KernelCode code);

class Job{
public:
	Job(double maxRunningTime, float targetedTime, int maxInterations) :
		stopTime(maxRunningTime), targetTime(targetedTime), targetIterations(maxInterations) {}
	virtual ~Job();
	virtual void runJob(int steps, std::string address, std::string port, RunType runtype) = 0;
	virtual float sendData(const std::vector<int> data, std::string address, std::string port) = 0;
	virtual void createSamples(int samples) = 0;
	virtual std::string createInputFile(RunType type, std::string name) = 0;
	virtual int getType() = 0;
	virtual int getRandValue() = 0;
	virtual std::vector<int> readFile(std::string filename) = 0;
	std::vector<int> generateData(RunType type);
	virtual int* createInput(RunType type) = 0;
	void setupFolders();
	void cleanFolders();
	virtual int getInputSize() = 0;
	std::string getOutputFolder(){
		return results_folder;
	}

	std::string samples_folder = "samples";
	std::string inputs_folder = "inputs";
	std::string results_folder = "results";


protected:
	double stopTime; //if we want to stop after a certain time
	float targetTime; // what fitness we want to beat
	int targetIterations; //for how many iterations we can run
};


#endif /* HELPERS_JOB_H_ */

/*
 * Jobs.h
 *
 *      Author: adrianh
 */

#ifndef HELPERS_JOBS_H_
#define HELPERS_JOBS_H_

#include <string>
#include <vector>

#include "Job.h"
#include "TimerHelper.h"

class ControlledJob : public Job{
public:
	ControlledJob(double maxRunningTime, float targetedTime, int maxInterations, int size) :
		Job(maxRunningTime, targetedTime, maxInterations), inputSize(size) {
		std::string location = "inout/controlled/" + TimerHelper::getCurrentTime() + "/";
		samples_folder = location + "controlled_samples";
		inputs_folder = location + "controlled_inputs";
		results_folder = location + "controlled_results";
	}

	ControlledJob(std::string folder, double maxRunningTime, float targetedTime, int maxInterations, int size) :
		Job(maxRunningTime, targetedTime, maxInterations), inputSize(size) {
		std::string location = folder + "/controlled/" + TimerHelper::getCurrentTime() + "/";
		samples_folder = location + "controlled_samples";
		inputs_folder = location + "controlled_inputs";
		results_folder = location + "controlled_results";
	}
	~ControlledJob(){}
	virtual int getRandValue();
	virtual float sendData(const std::vector<int> data, std::string address, std::string port);
	virtual void runJob(int steps, std::string address, std::string port, RunType runtype);
	virtual void createSamples(int samples);
	virtual std::string createInputFile(RunType type, std::string name);
	virtual int* createInput(RunType type);
	virtual int getInputSize();
	virtual int getType();
	virtual std::vector<int> readFile(std::string filename);


private:
	static const int TYPE = KernelCode::CONTROLLED;
	int inputSize;
};

class BFSJob : public Job{
public:
	BFSJob(double maxRunningTime, float targetedTime, int maxInterations, int size) :
		Job(maxRunningTime, targetedTime, maxInterations), inputSize(size) {
		std::string location = "inout/bfs/" + TimerHelper::getCurrentTime() + "/";
		samples_folder = location + "bfs_samples";
		inputs_folder = location + "bfs_inputs";
		results_folder = location + "bfs_results";
	}
	BFSJob(std::string folder, double maxRunningTime, float targetedTime, int maxInterations, int size) :
		Job(maxRunningTime, targetedTime, maxInterations), inputSize(size) {
		std::string location = folder + "/bfs/" + TimerHelper::getCurrentTime() + "/";
		samples_folder = location + "bfs_samples";
		inputs_folder = location + "bfs_inputs";
		results_folder = location + "bfs_results";
	}
	~BFSJob(){}
	virtual float sendData(const std::vector<int> data, std::string address, std::string port);
	virtual int getRandValue();
	virtual void runJob(int steps, std::string address, std::string port, RunType runtype);
	virtual void createSamples(int samples);
	virtual std::string createInputFile(RunType type, std::string name);
	virtual int* createInput(RunType type);
	virtual int getInputSize();
	virtual int getType();
	virtual std::vector<int> readFile(std::string filename);


private:
	static const int TYPE = KernelCode::BFS;
	int inputSize;
};


class NsichneuJob : public Job{
public:
	NsichneuJob(double maxRunningTime, float targetedTime, int maxInterations, int size) :
		Job(maxRunningTime, targetedTime, maxInterations), inputSize(size) {
		std::string location = "inout/nsichneu/" + TimerHelper::getCurrentTime() + "/";
		samples_folder = location + "nsichneu_samples";
		inputs_folder = location + "nsichneu_inputs";
		results_folder = location + "nsichneu_results";
	}

	NsichneuJob(std::string folder, double maxRunningTime, float targetedTime, int maxInterations, int size) :
		Job(maxRunningTime, targetedTime, maxInterations), inputSize(size) {
		std::string location = folder + "/nsichneu/" + TimerHelper::getCurrentTime() + "/";
		samples_folder = location + "nsichneu_samples";
		inputs_folder = location + "nsichneu_inputs";
		results_folder = location + "nsichneu_results";
	}
	~NsichneuJob(){}
	virtual int getRandValue();
	virtual float sendData(const std::vector<int> data, std::string address, std::string port);
	virtual void runJob(int steps, std::string address, std::string port, RunType runtype);
	virtual void createSamples(int samples);
	virtual std::string createInputFile(RunType type, std::string name);
	virtual int* createInput(RunType type);
	virtual int getInputSize();
	virtual int getType();
	virtual std::vector<int> readFile(std::string filename);


private:
	static const int TYPE = KernelCode::NSICHNEU;
	int inputSize;
};

class LBMSailfishJob : public Job{
public:
	LBMSailfishJob(double maxRunningTime, float targetedTime, int maxInterations, int size, int width, int height) :
		Job(maxRunningTime, targetedTime, maxInterations), inputSize(size), matrixWidth(width), matrixHeight(height) {
		std::string location = "inout/lbmSailfish/" + TimerHelper::getCurrentTime() + "/";
		samples_folder = location + "lbmSailfish_samples";
		inputs_folder = location + "lbmSailfish_inputs";
		results_folder = location + "lbmSailfish_results";
	}

	LBMSailfishJob(std::string folder, double maxRunningTime, float targetedTime, int maxInterations, int size, int width, int height) :
		Job(maxRunningTime, targetedTime, maxInterations), inputSize(size), matrixWidth(width), matrixHeight(height) {
		std::string location = folder + "/lbmSailfish/" + TimerHelper::getCurrentTime() + "/";
		samples_folder = location + "lbmSailfish_samples";
		inputs_folder = location + "lbmSailfish_inputs";
		results_folder = location + "lbmSailfish_results";
	}
	~LBMSailfishJob(){}
	virtual int getRandValue();
	virtual float sendData(const std::vector<int> data, std::string address, std::string port);
	virtual void runJob(int steps, std::string address, std::string port, RunType runtype);
	virtual void createSamples(int samples);
	virtual std::string createInputFile(RunType type, std::string name);
	virtual int* createInput(RunType type);
	virtual int getInputSize();
	virtual int getType();
	virtual std::vector<int> readFile(std::string filename);


private:
	static const int TYPE = KernelCode::LBM_SAILFISH;
	int inputSize;
	int matrixWidth;
	int matrixHeight;
};

class ConvolutionJob : public Job{
public:
	ConvolutionJob(double maxRunningTime, float targetedTime, int maxInterations, int size) :
		Job(maxRunningTime, targetedTime, maxInterations), inputSize(size) {
		std::string location = "inout/convolution/" + TimerHelper::getCurrentTime() + "/";
		samples_folder = location + "convolution_samples";
		inputs_folder = location + "convolution_inputs";
		results_folder = location + "convolution_results";
	}

	ConvolutionJob(std::string folder, double maxRunningTime, float targetedTime, int maxInterations, int size) :
		Job(maxRunningTime, targetedTime, maxInterations), inputSize(size) {
		std::string location = folder + "/convolution/" + TimerHelper::getCurrentTime() + "/";
		samples_folder = location + "convolution_samples";
		inputs_folder = location + "convolution_inputs";
		results_folder = location + "convolution_results";
	}
	~ConvolutionJob(){}
	virtual int getRandValue();
	virtual float sendData(const std::vector<int> data, std::string address, std::string port);
	virtual void runJob(int steps, std::string address, std::string port, RunType runtype);
	virtual void createSamples(int samples);
	virtual std::string createInputFile(RunType type, std::string name);
	virtual int* createInput(RunType type);
	virtual int getInputSize();
	virtual int getType();
	virtual std::vector<int> readFile(std::string filename);


private:
	static const int TYPE = KernelCode::CONVOLUTION;
	int inputSize;
};


#endif /* HELPERS_JOBS_H_ */

/*
 * BGSJob.cc
 *
 *      Author: adrianh
 */

#include "Jobs.h"
#include "../remote/RemoteClient.h"

#include <iostream>
#include <fstream>


struct Node
{
	int starting;
	int no_of_edges;
};

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <random>
#include <cstdlib>
#include <ctime>
#include <climits>


//graphGen.cpp - rodinia benchmark
// These names may vary by implementation
//#define LINEAR_CONGRUENTIAL_ENGINE linear_congruential_engine
//#define LINEAR_CONGRUENTIAL_ENGINE linear_congruential
//#define UNIFORM_INT_DISTRIBUTION uniform_int_distribution
//#define UNIFORM_INT_DISTRIBUTION uniform_int

using namespace std;

#define MIN_NODES 20
#define MAX_NODES ULONG_MAX
#define MIN_EDGES 2
#define MAX_INIT_EDGES 4 // Nodes will have, on average, 2*MAX_INIT_EDGES edges
#define MIN_WEIGHT 1
#define MAX_WEIGHT 10

typedef unsigned int uint;
typedef unsigned long ulong;

struct edge; // forward declaration
typedef vector<edge> node;
struct edge {
	ulong dest;
	uint weight;
};


void generateInputFile(ulong numNodes, std::string filename){
	//cout << "Generating graph with " << numNodes << " nodes...\n";
	node * graph;
	graph = new node[numNodes];

	// Initialize random number generators
	// C RNG for numbers of edges and weights
	//srand( time( NULL ) );
	// TR1 RNG for choosing edge destinations
	//LINEAR_CONGRUENTIAL_ENGINE<ulong, 48271, 0, ULONG_MAX> gen( time( NULL ) );
	//UNIFORM_INT_DISTRIBUTION<ulong> randNode( 0, numNodes - 1 );

	// Generate graph
	uint numEdges;
	ulong nodeID;
	uint weight;
	ulong i;
	uint j;
	for ( i = 0; i < numNodes; i++ )
	{
		numEdges = rand() % (numNodes/2);//rand() / ((numNodes * (numNodes - 1)) / 2);//rand() % ( MAX_INIT_EDGES - MIN_EDGES + 1 ) + MIN_EDGES;
		for ( j = 0; j < numEdges; j++ )
		{
			nodeID = rand() % numNodes;//randNode( gen );
			weight = rand() % ( MAX_WEIGHT - MIN_WEIGHT + 1 ) + MIN_WEIGHT;
			graph[i].push_back( edge() );
			graph[i].back().dest = nodeID;
			graph[i].back().weight = weight;
			graph[nodeID].push_back( edge() );
			graph[nodeID].back().dest = i;
			graph[nodeID].back().weight = weight;
		}
	}

	// Output
	//cout << "Writing to file \"" << filename << "\"...\n";
	ofstream outf( filename );
	outf << numNodes << "\n";
	ulong totalEdges = 0;
	for ( uint i = 0; i < numNodes; i++ )
	{
		numEdges = graph[i].size();
		outf << totalEdges << " " << numEdges << "\n";
		totalEdges += numEdges;
	}
	//outf << "\n" << randNode( gen ) << "\n\n";
	outf << "\n" << 0 << "\n\n"; //starting source is zero
	outf << totalEdges << "\n";
	for ( ulong i = 0; i < numNodes; i++ )
		for ( uint j = 0; j < graph[i].size(); j++ )
			outf << graph[i][j].dest << " " << graph[i][j].weight << "\n";
	outf.close();

	delete[] graph;
}
//end of graphgen.cpp rodinia benchmark
//

std::string BFSJob::createInputFile(RunType type, std::string name){
	std::string inputFilePath;
	if (type == RunType::RANDOM){
		std::ofstream teachFile;
		inputFilePath = inputs_folder + "/random-" + name + ".input";
		teachFile.open(inputFilePath);

		for (int i = 0 ; i < inputSize; i++){
			for (int j = 0; j < inputSize; j++){
				int val = getRandValue();
				teachFile << val << " ";
			}
			teachFile << std::endl;
		}
		teachFile.close();
	}else
		if (type == RunType::RADAMSA){
			inputFilePath = inputs_folder + "/radamsa-" + name + ".input";
			std::string sampleFilePath = samples_folder + "/*.sample";
			std::string command = "radamsa -o " + inputFilePath + " " + sampleFilePath;
			system(command.c_str());
		}

	return inputFilePath;
}

int* BFSJob::createInput(RunType type){
	int *data;
	data=(int*)calloc(inputSize * inputSize, sizeof(int));
	if (type == RunType::RANDOM){
		for (int i = 0 ; i < inputSize; i++){
			for (int j = 0; j < inputSize; j++){
				int val = getRandValue();
				data[i * inputSize + j] = val;
			}
		}
	}else
		if (type == RunType::RADAMSA){
			std::string filePath = createInputFile(type, TimerHelper::getCurrentTime());
			std::ifstream file;
			file.open(filePath);

			for (int i = 0 ; i < inputSize; i++){
				for (int j = 0; j < inputSize; j++){
					int val;
					file >> val;
					data[i] = val;
				}
			}
			file.close();
		}

	return data;
}

int BFSJob::getInputSize(){
	return (inputSize * inputSize);
}

int BFSJob::getRandValue(){
	return (rand() % 2);
}

std::vector<int> BFSJob::readFile(std::string filename){
	std::vector<int> data(inputSize*inputSize);
	std::fill(data.begin(), data.end(), 0);

	std::ifstream inputFile;
	inputFile.open(filename);


	int value;
	for (int i = 0; i < inputSize; i++){
		for (int j = 0; j < inputSize; j++){
			inputFile >> value;
			if (value != 0)
				data[i * inputSize + j] = 1;
			else
				data[i * inputSize + j] = 0;
		}
	}
	inputFile.close();

	return data;
}


float BFSJob::sendData(std::vector<int> data, std::string address, std::string port){
	RemoteClient client(address, port);
	client.connectToServer();

	unsigned int nodes = inputSize;
	unsigned int edges = 0;
	Node*  graph_nodes = new Node[nodes];

	//read the matrix
	edges = 0;
	graph_nodes[0].starting = 0;
	for (int i = 0 ; i < inputSize ; i++){
		graph_nodes[i].no_of_edges = 0;
		for (int j = 0 ; j < inputSize ; j++){
			int value = data[i * inputSize + j];
			if (value == 1){
				edges++;
				graph_nodes[i].no_of_edges++;
			}
		}
		if (i > 0){
			graph_nodes[i].starting = graph_nodes[i-1].starting + graph_nodes[i-1].no_of_edges;
		}
	}

	int* graph_edges = new int[edges];
	int index = 0;
	for (unsigned int i = 0 ; i < nodes; i++){
		for (unsigned int j = 0; j < nodes; j++){
			int value = data[i * inputSize + j];
			if (value == 1){
				graph_edges[index] = j;
				index++;
			}
		}
	}

	int code = TYPE;
	client.sendData(&code, sizeof(int), 1, "Kernel code");

	int howMany;
	howMany = 2; //nodes and edges
	client.sendData(&howMany, sizeof(int), 1 , "How many input vectors will be sent");

	//send the nodes
	//how many
	client.sendData(&nodes, sizeof(int), 1, "Data size for input named : nodes");
	//send nodes
	client.sendData(graph_nodes, nodes * sizeof(Node), nodes, "Input named : nodes");

	//send the edges
	//how many
	client.sendData(&edges, sizeof(int), 1, "Data size for input named : edges");
	//send nodes
	client.sendData(graph_edges, edges * sizeof(int), edges, "Input named : edges");

	float fitness;
	client.receiveData(&fitness, sizeof(float), 1, "fitness");

	client.disconnectFromServer();

	return fitness;
}

//void BFSJob::runJob(int steps, std::string address, std::string port, RunType runtype){
//
//	RemoteClient client(address, port);
//	//RemoteClient client("130.236.182.193", "54321"); //board standard IP
//	//RemoteClient client("localhost", "54321"); //board standard IP
//
//	//int steps = 100;
//	float maxFitness = 0;
//
//	std::string extension = getExtesion(runtype);
//
//	std::ofstream plotFile;
//	plotFile.open(results_folder + "/fitness." + extension, std::ios_base::app);
//	std::ofstream stepPlotFile;
//	stepPlotFile.open(results_folder + "/step." + extension, std::ios_base::app);
//	std::ofstream maxPlotFile;
//	maxPlotFile.open(results_folder + "/max." + extension, std::ios_base::app);
//
//	int **matrix;
//	matrix = new int*[inputSize];
//	for (int i = 0 ; i < inputSize; i++){
//		matrix[i] = new int[inputSize];
//		for (int j = 0; j < inputSize; j++){
//			matrix[i][j] = 0;
//		}
//	}
//
//	unsigned int nodes = inputSize;
//	unsigned int edges = 0;
//	Node*  graph_nodes = new Node[nodes];
//
//	for (int iter = 0; iter < targetIterations; iter++){
//		std::cout << "Iteration " << iter;
//		std::string inputFilePath = createInputFile(runtype, std::to_string(iter));
//
//		//generate input file using the graphgen method from rodinia benchmark
//		//generateInputFile(inputSize, inputFilePath);
//
//		std::ifstream inputFile;
//		inputFile.open(inputFilePath);
//
//		//read the matrix
//		edges = 0;
//		graph_nodes[0].starting = 0;
//		for (int i = 0 ; i < inputSize ; i++){
//			graph_nodes[i].no_of_edges = 0;
//			for (int j = 0 ; j < inputSize ; j++){
//				int value;
//				inputFile >> value;
//				if (value != 0){
//					edges++;
//					graph_nodes[i].no_of_edges++;
//					matrix[i][j] = 1;
//				}else{
//					matrix[i][j] = 0;
//				}
//			}
//			if (i > 0){
//				graph_nodes[i].starting = graph_nodes[i-1].starting + graph_nodes[i-1].no_of_edges;
//			}
//		}
//
//		int* graph_edges = new int[edges];
//		int index = 0;
//		for (unsigned int i = 0 ; i < nodes; i++){
//			for (unsigned int j = 0; j < nodes; j++){
//				if (matrix[i][j] == 1){
//					graph_edges[index] = j;
//					index++;
//				}
//			}
//		}
//		//read graph Nodes
//		//		inputFile >> nodes;
//		//		Node*  graph_nodes = new Node[nodes];
//		//		int start, edgeno;
//		//		for( unsigned int i = 0; i < nodes; i++)
//		//		{
//		//			inputFile >> start >> edgeno;
//		//			graph_nodes[i].starting = start;
//		//			graph_nodes[i].no_of_edges = edgeno;
//		//		}
//		//		int source = 0;
//		//		inputFile >> source;
//		//		unsigned int edges = 0;
//		//		inputFile >> edges;
//		//
//		//		int* graph_edges = new int[edges];
//		//		int id,cost;
//		//		for(unsigned int i=0; i < edges ; i++)
//		//		{
//		//			inputFile >> id >> cost;
//		//			graph_edges[i] = id;
//		//		}
//		inputFile.close();
//
//		client.connectToServer();
//
//		int code = TYPE;
//		client.sendData(&code, sizeof(int), 1, "Kernel code");
//
//		int howMany;
//		howMany = 2; //nodes and edges
//		client.sendData(&howMany, sizeof(int), 1 , "How many input vectors will be sent");
//
//		//send the nodes
//		//how many
//		client.sendData(&nodes, sizeof(int), 1, "Data size for input named : nodes");
//		//send nodes
//		client.sendData(graph_nodes, nodes * sizeof(Node), nodes, "Input named : nodes");
//
//		//send the edges
//		//how many
//		client.sendData(&edges, sizeof(int), 1, "Data size for input named : edges");
//		//send nodes
//		client.sendData(graph_edges, edges * sizeof(int), edges, "Input named : edges");
//
//		float fitness;
//		client.receiveData(&fitness, sizeof(float), 1, "fitness");
//
//		if (maxFitness < fitness){
//			maxFitness = fitness;
//			TimerHelper::stopTimer();
//			double currentTime = TimerHelper::getElapsedSeconds();
//			std::cout << "; best fitness " << maxFitness << " ms" <<
//					" found after: " << currentTime << " seconds" <<std::endl;
//
//			maxPlotFile << iter << " " << maxFitness << std::endl;
//		}
//
//		//plots
//		plotFile << fitness << std::endl;
//		if (iter % steps == 0){
//			stepPlotFile << iter/steps << " " << maxFitness << std::endl;
//		}
//
//		client.disconnectFromServer();
//		//delete graph_nodes;
//		delete graph_edges;
//		std::cout << std::endl;
//	}
//
//	for (int i = 0; i < inputSize ; i++){
//		delete matrix[i];
//	}
//	delete matrix;
//	delete graph_nodes; //reuse this because the number of nodes is fixed
//
//	plotFile.close();
//	stepPlotFile.close();
//	maxPlotFile.close();
//}

void BFSJob::runJob(int steps, std::string address, std::string port, RunType runtype){

	RemoteClient client(address, port);
	//RemoteClient client("130.236.182.193", "54321"); //board standard IP
	//RemoteClient client("localhost", "54321"); //board standard IP

	//int steps = 100;
	float maxFitness = 0;

	std::string extension = getExtesion(runtype);

	std::ofstream plotFile;
	plotFile.open(results_folder + "/fitness." + extension, std::ios_base::app);
	std::ofstream stepPlotFile;
	stepPlotFile.open(results_folder + "/step." + extension, std::ios_base::app);
	std::ofstream maxPlotFile;
	maxPlotFile.open(results_folder + "/max." + extension, std::ios_base::app);

	int **matrix;
	matrix = new int*[inputSize];
	for (int i = 0 ; i < inputSize; i++){
		matrix[i] = new int[inputSize];
		for (int j = 0; j < inputSize; j++){
			matrix[i][j] = 0;
		}
	}

	unsigned int nodes = inputSize;
	unsigned int edges = 0;
	Node*  graph_nodes = new Node[nodes];

	for (int iter = 0; iter < targetIterations; iter++){
		std::cout << "Iteration " << iter;
		int *data = createInput(runtype);

		//read the matrix
		edges = 0;
		graph_nodes[0].starting = 0;
		for (int i = 0 ; i < inputSize ; i++){
			graph_nodes[i].no_of_edges = 0;
			for (int j = 0 ; j < inputSize ; j++){
				int value;
				value = data[i * inputSize + j];
				if (value != 0){
					edges++;
					graph_nodes[i].no_of_edges++;
					matrix[i][j] = 1;
				}else{
					matrix[i][j] = 0;
				}
			}
			if (i > 0){
				graph_nodes[i].starting = graph_nodes[i-1].starting + graph_nodes[i-1].no_of_edges;
			}
		}
		free(data);

		int* graph_edges = new int[edges];
		int index = 0;
		for (unsigned int i = 0 ; i < nodes; i++){
			for (unsigned int j = 0; j < nodes; j++){
				if (matrix[i][j] != 0){
					graph_edges[index] = j;
					index++;
				}
			}
		}

		client.connectToServer();

		int code = TYPE;
		client.sendData(&code, sizeof(int), 1, "Kernel code");

		int howMany;
		howMany = 2; //nodes and edges
		client.sendData(&howMany, sizeof(int), 1 , "How many input vectors will be sent");

		//send the nodes
		//how many
		client.sendData(&nodes, sizeof(int), 1, "Data size for input named : nodes");
		//send nodes
		client.sendData(graph_nodes, nodes * sizeof(Node), nodes, "Input named : nodes");

		//send the edges
		//how many
		client.sendData(&edges, sizeof(int), 1, "Data size for input named : edges");
		//send nodes
		client.sendData(graph_edges, edges * sizeof(int), edges, "Input named : edges");

		float fitness;
		client.receiveData(&fitness, sizeof(float), 1, "fitness");

		if (maxFitness < fitness){
			maxFitness = fitness;
			TimerHelper::stopTimer();
			double currentTime = TimerHelper::getElapsedSeconds();
			std::cout << "; best fitness " << maxFitness << " ms" <<
					" found after: " << currentTime << " seconds" <<std::endl;

			maxPlotFile << iter << " " << maxFitness << std::endl;
		}

		//plots
		plotFile << fitness << std::endl;
		if (((iter+1) % steps == 0) && (iter > 0)){
			stepPlotFile << iter/steps << " " << maxFitness << std::endl;
		}

		client.disconnectFromServer();
		//delete graph_nodes;
		delete graph_edges;
		std::cout << std::endl;
	}

	for (int i = 0; i < inputSize ; i++){
		delete matrix[i];
	}
	delete matrix;
	delete graph_nodes; //reuse this because the number of nodes is fixed

	plotFile.close();
	stepPlotFile.close();
	maxPlotFile.close();
}




void BFSJob::createSamples(int samples){
	//create matrix style samples
	for (int j = 0; j < samples; j++){
		std::ofstream teachFile;
		teachFile.open(samples_folder + "/teach-" + std::to_string(j) + ".sample");

		for (int i = 0 ; i < inputSize; i++){
			for (int j = 0; j < inputSize; j++){
				int val = getRandValue();
				teachFile << val << " ";
			}
			teachFile << std::endl;
		}
		teachFile.close();
	}
}

int BFSJob::getType(){
	return TYPE;
}







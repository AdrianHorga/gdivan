/*
 * ControlledJob.cc
 *
 *      Author: adrianh
 */



#include "Jobs.h"
#include "../remote/RemoteClient.h"

#include <iostream>
#include <fstream>

std::string ConvolutionJob::createInputFile(RunType type, std::string name){
	std::string inputFilePath;
	if (type == RunType::RANDOM){
		std::ofstream teachFile;
		inputFilePath = inputs_folder + "/random-" + name + ".input";
		teachFile.open(inputFilePath);

		for (int i = 0 ; i < inputSize; i++){
			int val = getRandValue();
			teachFile << val << " ";
		}
		teachFile.close();
	}else
		if (type == RunType::RADAMSA){
			inputFilePath = inputs_folder + "/radamsa-" + name + ".input";
			std::string sampleFilePath = samples_folder + "/*.sample";
			std::string command = "radamsa -o " + inputFilePath + " " + sampleFilePath;
			system(command.c_str());
		}

	return inputFilePath;
}

int* ConvolutionJob::createInput(RunType type){
	int *data;
	data=(int*)calloc(inputSize, sizeof(int));
	if (type == RunType::RANDOM){
		for (int i = 0 ; i < inputSize; i++){
			int val = getRandValue();
			data[i] = val;
		}
	}else
		if (type == RunType::RADAMSA){
			std::string filePath = createInputFile(type, TimerHelper::getCurrentTime());
			std::ifstream file;
			file.open(filePath);

			for (int i = 0 ; i < inputSize; i++){
				int val;
				file >> val;
				data[i] = val;
			}

			file.close();
		}


	return data;
}


int ConvolutionJob::getInputSize(){
	return (inputSize);
}

int ConvolutionJob::getRandValue(){
	return (rand() % 32000);
}

std::vector<int> ConvolutionJob::readFile(std::string filename){
	std::vector<int> data(inputSize);
	std::fill(data.begin(), data.end(), 0);

	std::ifstream inputFile;
	inputFile.open(filename);


	for (int i = 0; i < inputSize; i++){
		int value;
		inputFile >> value;
		data[i] = value;
	}
	inputFile.close();

	return data;
}

float ConvolutionJob::sendData(std::vector<int> data, std::string address, std::string port){
	RemoteClient client(address, port);
	client.connectToServer();

	int code = TYPE;
	client.sendData(&code, sizeof(int), 1, "Kernel code");

	int howMany = 1;
	client.sendData(&howMany, sizeof(int), 1 , "How many input vectors will be sent");

	int dataSize = inputSize;
	client.sendData(&dataSize, sizeof(int), 1, "Data size for input");

	client.sendData(data.data(), dataSize * sizeof(int), dataSize, "Input");

	float fitness;
	client.receiveData(&fitness, sizeof(float), 1, "fitness");

	client.disconnectFromServer();

	return fitness;
}

//void ControlledJob::runJob(int steps, std::string address, std::string port, RunType runtype){
//	int *data;
//
//	data = (int*)malloc(sizeof(int) * inputSize);
//
//	RemoteClient client(address, port);
//	//RemoteClient client("130.236.182.193", "54321"); //board standard IP
//	//RemoteClient client("localhost", "54321"); //board standard IP
//
//	//int steps = 100;
//	float maxFitness = 0;
//
//	std::string extension = getExtesion(runtype);
//
//	std::ofstream plotFile;
//	plotFile.open(results_folder + "/fitness." + extension, std::ios_base::app);
//	std::ofstream stepPlotFile;
//	stepPlotFile.open(results_folder + "/step." + extension, std::ios_base::app);
//	std::ofstream maxPlotFile;
//	maxPlotFile.open(results_folder + "/max." + extension, std::ios_base::app);
//
//	for (int iter = 0; iter < targetIterations; iter++){
//		std::cout << "Iteration " << iter;
//		std::string inputFilePath = createInputFile(runtype, std::to_string(iter));
//		std::ifstream inputFile;
//		inputFile.open(inputFilePath);
//
//
//		for (int i = 0; i < inputSize; i++){
//			int value;
//			inputFile >> value;
//			//value = 10;
//			data[i] = value;
//		}
//		inputFile.close();
//
//		client.connectToServer();
//
//		int code = TYPE;
//		client.sendData(&code, sizeof(int), 1, "Kernel code");
//
//		int howMany = 1;
//		client.sendData(&howMany, sizeof(int), 1 , "How many input vectors will be sent");
//
//		int dataSize = inputSize;
//		client.sendData(&dataSize, sizeof(int), 1, "Data size for input");
//
//		client.sendData(data, dataSize * sizeof(int), dataSize, "Input");
//
//		float fitness;
//		client.receiveData(&fitness, sizeof(float), 1, "fitness");
//
//		if (maxFitness < fitness){
//			maxFitness = fitness;
//			TimerHelper::stopTimer();
//			double currentTime = TimerHelper::getElapsedSeconds();
//			std::cout << "; best fitness " << maxFitness << " ms" <<
//					" found after: " << currentTime << " seconds" <<std::endl;
//
//			maxPlotFile << iter << " " << maxFitness << std::endl;
//		}
//
//		//plots
//		plotFile << fitness << std::endl;
//		if (iter % steps == 0){
//			stepPlotFile << iter/steps << " " << maxFitness << std::endl;
//		}
//
//		client.disconnectFromServer();
//		std::cout << std::endl;
//	}
//	free(data);
//
//	plotFile.close();
//	stepPlotFile.close();
//	maxPlotFile.close();
//}


void ConvolutionJob::runJob(int steps, std::string address, std::string port, RunType runtype){
	RemoteClient client(address, port);
	float maxFitness = 0;

	std::string extension = getExtesion(runtype);

	std::ofstream plotFile;
	plotFile.open(results_folder + "/fitness." + extension, std::ios_base::app);
	std::ofstream stepPlotFile;
	stepPlotFile.open(results_folder + "/step." + extension, std::ios_base::app);
	std::ofstream maxPlotFile;
	maxPlotFile.open(results_folder + "/max." + extension, std::ios_base::app);

	for (int iter = 0; iter < targetIterations; iter++){
		std::cout << "Iteration " << iter;
		int* data = createInput(runtype);

		client.connectToServer();

		int code = TYPE;
		client.sendData(&code, sizeof(int), 1, "Kernel code");

		int howMany = 1;
		client.sendData(&howMany, sizeof(int), 1 , "How many input vectors will be sent");

		int dataSize = inputSize;
		client.sendData(&dataSize, sizeof(int), 1, "Data size for input");

		client.sendData(data, dataSize * sizeof(int), dataSize, "Input");

		float fitness;
		client.receiveData(&fitness, sizeof(float), 1, "fitness");

		if (maxFitness < fitness){
			maxFitness = fitness;
			TimerHelper::stopTimer();
			double currentTime = TimerHelper::getElapsedSeconds();
			std::cout << "; best fitness " << maxFitness << " ms" <<
					" found after: " << currentTime << " seconds" <<std::endl;

			maxPlotFile << iter << " " << maxFitness << std::endl;
		}

		//plots
		plotFile << fitness << std::endl;
		if (((iter+1) % steps == 0) && (iter > 0)){
			stepPlotFile << iter/steps << " " << maxFitness << std::endl;
		}

		client.disconnectFromServer();
		free(data);
		std::cout << std::endl;
	}

	plotFile.close();
	stepPlotFile.close();
	maxPlotFile.close();
}

void ConvolutionJob::createSamples(int samples){

	for (int j = 0; j < samples; j++){
		std::ofstream teachFile;
		teachFile.open(samples_folder + "/teach-" + std::to_string(j) + ".sample");

		for (int i = 0 ; i < inputSize; i++){
			int val = getRandValue();
			teachFile << val << " ";
		}
		teachFile.close();
	}
}

int ConvolutionJob::getType(){
	return TYPE;
}



/*
 * PopulationManagerGklee.h
 *
 *      Author: adrianh
 */

#ifndef GENETIC_ALGORITHM_POPULATIONMANAGERGKLEE_H_
#define GENETIC_ALGORITHM_POPULATIONMANAGERGKLEE_H_

#include <algorithm>

#include "../gklee_helpers/GkleeData.h"
#include "PopulationManager.h"
#include "../gklee_helpers/KernelInfo.h"
#include "../genetic_algorithm/IndividualGklee.h"


template <typename T>
class PopulationManagerGklee : public PopulationManager{
public:
	//PopulationManagerGklee(GkleeData<T>* data);
	PopulationManagerGklee(KernelInfo<T> *info): PopulationManager(){
		kernelInfo = info;
		basicData = info->getData();
		outputFolder = info->getOutputFolder();
	}

	virtual void crossover(Individual* parent1, Individual* parent2, Individual* child1, Individual* child2){
		IndividualGklee<T>* p1 = dynamic_cast<IndividualGklee<T>* >(parent1);
		IndividualGklee<T>* p2 = dynamic_cast<IndividualGklee<T>* >(parent2);
		IndividualGklee<T>* ch1 = dynamic_cast<IndividualGklee<T>* >(child1);
		IndividualGklee<T>* ch2 = dynamic_cast<IndividualGklee<T>* >(child2);


		unsigned length = p1->getGenotypeSize();

		//FIXME choose crossover point appropriately
		unsigned crossoverPoint = rand() % length;
		//unsigned crossoverPoint = length / 2;

		for (unsigned i = 0 ; i < length; i++){
			PathSolution<T>* from1 = p1->getChromosome(i);
			PathSolution<T>* from2 = p2->getChromosome(i);
			if (i < crossoverPoint){
				ch1->addChromosome(from1);
				ch2->addChromosome(from2);
			}else{
				ch1->addChromosome(from2);
				ch2->addChromosome(from1);
			}
		}
		//child1 = ch1;
		//child2 = ch2;
	}

	virtual void mutate(Individual* individual){
		IndividualGklee<T> *ind = dynamic_cast<IndividualGklee<T> * >(individual);

		unsigned pointOfMutation  = rand() % ind->getGenotypeSize();

		//		PathSolution<T>* newSolution = basicData->pickARoulletteSolution();
		PathSolution<T>* newSolution = basicData->pickARandomSolution();

		ind->setChromosome(newSolution, pointOfMutation);
	}

	virtual Individual* generateIndividual(unsigned size){
		IndividualGklee<T>* ind = new IndividualGklee<T>(kernelInfo->getKernelCode(), kernelInfo->getSenderPattern());

		//std::cout << "Generate new individual" << std::endl;
		for (unsigned i = 0 ; i < size; i++){
			//PathSolution<T>* sol = basicData->pickARoulletteSolution();
			PathSolution<T>* sol = basicData->pickARandomSolution();
			ind->addChromosome(sol);
		}

		return (ind);
	}

	virtual Individual* generateRandomIndividual(unsigned size){
		IndividualGklee<T>* ind = new IndividualGklee<T>(kernelInfo->getKernelCode(), kernelInfo->getSenderPattern());

		//std::cout << "Generate new individual" << std::endl;
		for (unsigned i = 0 ; i < size; i++){
			PathSolution<T>* sol = basicData->pickARandomSolution();
			ind->addChromosome(sol);
		}

		return (ind);
	}

	//sort for descending order of instructions and descending order of divercence
	static inline bool blocksComparator(BlockOfSolutions<T>* x, BlockOfSolutions<T>* y){
		if (x->getExecutedInstructions() > y->getExecutedInstructions()){
			return true;
		}else{
			if (x->getExecutedInstructions() < y->getExecutedInstructions()){
				return false;
			}else{
				if (x->getDivergence() > y->getDivergence()){
					return true;
				}else{
					return false;
				}
			}
		}
	}

	virtual std::vector<Individual*> generateCornerCaseIndividuals(unsigned howMany, unsigned size){
		std::vector<Individual*> individuals;
		std::vector<BlockOfSolutions<T>*> blocks = basicData->getBlocks();

		std::sort(blocks.begin(), blocks.end(), blocksComparator);

		//		for (auto block : blocks){
		//			block->printData();
		//		}

		unsigned remainingIndividuals = howMany;

		for (unsigned i = 0 ; (i < blocks.size()) && (remainingIndividuals > howMany/3); i++){
			int solutionsSize = blocks[i]->getNumberOfSolutions();
			for (int j = 0; (j < solutionsSize) && (remainingIndividuals > howMany/3); j++){
				IndividualGklee<T>* ind = new IndividualGklee<T>(kernelInfo->getKernelCode(), kernelInfo->getSenderPattern());
				PathSolution<T>* newSolution = blocks[i]->pickSolution(j);
				for (unsigned j = 0 ; j < size; j++){
					ind->addChromosome(newSolution);
				}
				individuals.push_back(ind);
				remainingIndividuals--;
			}
		}

		while(remainingIndividuals > 0){
			IndividualGklee<T>* ind = new IndividualGklee<T>(kernelInfo->getKernelCode(), kernelInfo->getSenderPattern());

			int i = rand()%(blocks.size() / 2);
			int j = rand()% (blocks.size() / 2);
			PathSolution<T>* newSolution1 = blocks[i]->pickARandomSolution();
			PathSolution<T>* newSolution2 = blocks[j]->pickARandomSolution();
			for (unsigned k = 0 ; k < size/2 ; k++){
				ind->addChromosome(newSolution1);
				ind->addChromosome(newSolution2);
			}
			individuals.push_back(ind);
			remainingIndividuals--;
		}


		return individuals;
	}
	virtual std::string getOutputFolder(){
		return outputFolder;
	}


	virtual ~PopulationManagerGklee(){
		delete kernelInfo;
	}

private:
	GkleeData<T>* basicData;
	KernelInfo<T> *kernelInfo;

	std::string outputFolder = "";
};


#endif /* GENETIC_ALGORITHM_POPULATIONMANAGERGKLEE_H_ */

/*
 * IndividualRandom.h
 *
 *      Author: adrianh
 */

#ifndef GENETIC_ALGORITHM_INDIVIDUALRANDOM_H_
#define GENETIC_ALGORITHM_INDIVIDUALRANDOM_H_

#include "Individual.h"
#include "../helpers/Job.h"

//TODO implement a random based individual
class IndividualRandom : public Individual{
public:
	IndividualRandom(Job* job, int ID, std::vector<int> data, std::string address, std::string port ){
		chromosomes = data;
		theJob = job;
		id = ID;
		destAddr = address;
		destPort = port;
	}

	virtual unsigned getGenotypeSize() const{
		return chromosomes.size();
	}
	virtual void computeFitness(){
		fitness = theJob->sendData(chromosomes, destAddr, destPort);
	}
	virtual ~IndividualRandom(){

	}

	int getChromosome(int pos){
		return chromosomes[pos];
	}

	void addChromosome(int value){
		chromosomes.push_back(value);
	}

	void setChromosome(int chromosome, unsigned pos){
		chromosomes[pos] = chromosome;
	}

private:
	std::vector<int> chromosomes;
	Job* theJob;
	int id;
	std::string destAddr;
	std::string destPort;
};



#endif /* GENETIC_ALGORITHM_INDIVIDUALRANDOM_H_ */

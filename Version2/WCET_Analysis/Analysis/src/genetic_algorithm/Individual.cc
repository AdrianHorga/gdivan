/*
 * Individual.cc
 *
 *      Author: adrianh
 */

#include "Individual.h"

Individual::Individual():fitness(0){}

float Individual::getFitness() const{
	return (fitness);
}

Individual::~Individual(){}

bool Individual::isNewlyBorn(){
	return (compute);
}

void Individual::setOld(){
	compute = false;
}




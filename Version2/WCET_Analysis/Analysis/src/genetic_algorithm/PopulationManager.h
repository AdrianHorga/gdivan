/*
 * PopulationManager.h
 *
 *      Author: adrianh
 */

#ifndef GENETIC_ALGORITHM_POPULATIONMANAGER_H_
#define GENETIC_ALGORITHM_POPULATIONMANAGER_H_
#include <vector>
#include <string>

#include "Individual.h"

class PopulationManager{
public:
	virtual ~PopulationManager();
	virtual void crossover(Individual* parent1, Individual* parent2, Individual* child1, Individual* child2) = 0;
	virtual void mutate(Individual* individual) = 0;
	virtual Individual* generateIndividual(unsigned size) = 0;
	virtual Individual* generateRandomIndividual(unsigned size) = 0;
	virtual std::vector<Individual*> generateCornerCaseIndividuals(unsigned howMany, unsigned size) = 0;
	virtual std::string getOutputFolder() = 0;

private:

};



#endif /* GENETIC_ALGORITHM_POPULATIONMANAGER_H_ */

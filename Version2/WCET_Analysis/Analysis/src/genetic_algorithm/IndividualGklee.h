/*
 * IndividualGklee.h
 *
 *      Author: adrianh
 */

#ifndef GENETIC_ALGORITHM_INDIVIDUALGKLEE_H_
#define GENETIC_ALGORITHM_INDIVIDUALGKLEE_H_

#include <vector>
#include <string>
#include "Individual.h"
#include "../data_classes/PathSolution.h"
#include "../remote/RemoteClient.h"
#include "SenderPattern.h"

#include <iostream>
#include <fstream>

template <typename T>
class IndividualGklee : public Individual{
public:
	IndividualGklee(int code, CommunicationPattern<T> *sendingPattern) :
		Individual(), kernelCode(code), communication(sendingPattern){}

	virtual unsigned getGenotypeSize() const{
		return (chromosomes.size());
	}

	PathSolution<T>* getChromosome(unsigned pos){
		return (chromosomes[pos]);
	}

	void setChromosome(PathSolution<T>* chromosome, unsigned pos){
		chromosomes[pos] = chromosome;
	}

	void addChromosome(PathSolution<T>* chromosome){
		chromosomes.push_back(chromosome);
	}
	//void setClient(RemoteClient *remoteClient);
	virtual void computeFitness(){
		//TODO fill in the data from the chromosones
		//create an array to send
		std::map<std::string, std::vector<T> > inputs;
		for (PathSolution<T> *elem : chromosomes){
			std::map<std::string, std::vector<T> > parts = elem->getInputVectors();
			for (const auto& item : parts){
				//getting the name of the input
				std::string inputName = item.first;
				//pushing the part into the inputs
				std::vector<T> inputVector = item.second;
				inputs[inputName].insert(inputs[inputName].end(), inputVector.begin(), inputVector.end());
			}
		}

		fitness = communication->communicateData(inputs, kernelCode);
	}

	virtual ~IndividualGklee(){
		chromosomes.clear();
		//delete client;
	}



private:
	std::vector<PathSolution<T>* > chromosomes;
	int kernelCode;
	CommunicationPattern<T>* communication = NULL;
	//RemoteClient *client;
	//static float maxFitness;
	//static int maxFitnessIteration;
};

//template <typename T>
//float IndividualGklee<T>::maxFitness = 0;
//
//template <typename T>
//int IndividualGklee<T>::maxFitnessIteration = 0;

//template class IndividualGklee<int>;
//template class IndividualGklee<double>;
//template class IndividualGklee<float>;

#endif /* GENETIC_ALGORITHM_INDIVIDUALGKLEE_H_ */

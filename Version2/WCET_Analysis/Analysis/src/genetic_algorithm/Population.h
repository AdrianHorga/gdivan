/*
 * Population.h
 *
 *      Author: adrianh
 */

#ifndef GENETIC_ALGORITHM_POPULATION_H_
#define GENETIC_ALGORITHM_POPULATION_H_

#include <vector>
#include <random>
#include "GASettings.h"
#include "Individual.h"
#include "PopulationManager.h"

class Population{
public:
	Population(PopulationManager* manager, GASettings* settings);
	Population(PopulationManager* manager, GASettings* settings, std::string ext);
	~Population();
	void computeFitness();
	Individual* getTopIndividual();
	void sortByFitness();
	void mate();
	void initializePopulation(unsigned sizeOfChromosomes);
	void renewPopulation(unsigned sizeOfChromosomes);
	void runGeneticAlgorithm(unsigned sizeOfChromosomes);

	static std::string fitnessOutput;
	static std::string fitnessSortedOutput;
	static std::string fitnessStepOutput;
	static std::string fitnessMaxOutput;
	static std::string wcetOutput;

private:
	void initPlotFiles();
	void writeFitnessToPlotFile(float fitness);
	void writeSortedIndividualsToPlotFile(int iteration);
	void writeStepToPlotFile(int iteration);
	void writeToLogFile(int iteration, double fitness, double time);

	std::vector<Individual*> individuals;
	PopulationManager *manager;
	GASettings *populationSettings;

	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution;

	std::string extension = ".default";
	static float maxFitness;
	static int maxFitnessIteration;

};

#endif /* GENETIC_ALGORITHM_POPULATION_H_ */

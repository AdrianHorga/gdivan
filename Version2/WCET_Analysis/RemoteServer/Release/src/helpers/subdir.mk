################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CU_SRCS += \
../src/helpers/BFSHandler.cu \
../src/helpers/ControlledHandler.cu \
../src/helpers/ConvolutionHandler.cu \
../src/helpers/LBMHandler.cu \
../src/helpers/NSICHNEUHandler.cu 

OBJS += \
./src/helpers/BFSHandler.o \
./src/helpers/ControlledHandler.o \
./src/helpers/ConvolutionHandler.o \
./src/helpers/LBMHandler.o \
./src/helpers/NSICHNEUHandler.o 

CU_DEPS += \
./src/helpers/BFSHandler.d \
./src/helpers/ControlledHandler.d \
./src/helpers/ConvolutionHandler.d \
./src/helpers/LBMHandler.d \
./src/helpers/NSICHNEUHandler.d 


# Each subdirectory must supply rules for building sources it contributes
src/helpers/%.o: ../src/helpers/%.cu
	@echo 'Building file: $<'
	@echo 'Invoking: NVCC Compiler'
	/usr/local/cuda-8.0/bin/nvcc -O3 -std=c++11 -gencode arch=compute_32,code=sm_32  -odir "src/helpers" -M -o "$(@:%.o=%.d)" "$<"
	/usr/local/cuda-8.0/bin/nvcc -O3 -std=c++11 --compile --relocatable-device-code=true -gencode arch=compute_32,code=compute_32 -gencode arch=compute_32,code=sm_32  -x cu -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



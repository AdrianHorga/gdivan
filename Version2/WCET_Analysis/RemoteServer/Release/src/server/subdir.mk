################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/server/RemoteServer.cc 

CC_DEPS += \
./src/server/RemoteServer.d 

OBJS += \
./src/server/RemoteServer.o 


# Each subdirectory must supply rules for building sources it contributes
src/server/%.o: ../src/server/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: NVCC Compiler'
	/usr/local/cuda-8.0/bin/nvcc -O3 -std=c++11 -gencode arch=compute_32,code=sm_32  -odir "src/server" -M -o "$(@:%.o=%.d)" "$<"
	/usr/local/cuda-8.0/bin/nvcc -O3 -std=c++11 --compile  -x c++ -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



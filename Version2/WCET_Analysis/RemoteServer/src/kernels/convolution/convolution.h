


#include <stdio.h>
#include <stdlib.h>
#include "../../helpers/Helpers.h"



__global__ static void convolution(int* values, int *filter, int* result, int n, int diameter)
{
    const int tid = blockIdx.x * blockDim.x + threadIdx.x;

    const int filterSize = (values[tid] % filter[tid % diameter]) % diameter;

    if (tid < (n - filterSize)){
      int sum = 0;

      for (int j = 0 ; j <= filterSize; j++){
        if (diameter < blockDim.x){
            for (int k = j + 1 ; k <= filterSize; k++){
                    sum += values[tid + k] * filter[k]
                            + values[tid + k] % (filter[k] + 1)
                            + filter[k] % (values[tid + k] + 1);
            }
        }
        sum += values[tid + j] * filter[j]
		+ values[tid + j] % (filter[j] + 1)
		+ filter[j] % (values[tid + j] + 1);
        }

      result[tid] = sum;
    }

}


float runConvolution(const int *values, const int *filter, const int diameter, const int blocks,const int threads)
{
	//    int blocks = 512;
	//    int threads = 512;
	int total = threads * blocks;
	int *result;

	result = (int*)malloc(sizeof(int) * total);

	int * dvalues, *dfilter, *dresult;
	cudaMalloc((void**)&dvalues, sizeof(int) * total);
	cudaMalloc((void**)&dresult, sizeof(int) * total);
	cudaMalloc((void**)&dfilter, sizeof(int) * diameter);
	cudaMemcpy(dvalues, values, sizeof(int) * total, cudaMemcpyHostToDevice);
	cudaMemcpy(dfilter, filter, sizeof(int) * diameter, cudaMemcpyHostToDevice);

	MeasureCUDA measure;
	measure.startMeasuring();

	std::cout << "blocks * threads = " << blocks << "*" << threads << "=" << blocks*threads << std::endl;

	convolution<<<blocks, threads>>>(dvalues, dfilter, dresult, total, diameter);

	measure.stopMeasuring();
	float  time;
	time = measure.getTime();
	printf("Total Time = %f ms\n\n", time);

	cudaMemcpy(result, dresult, sizeof(int) * total, cudaMemcpyDeviceToHost);

	cudaFree(dvalues);
	cudaFree(dresult);
	cudaFree(dfilter);

	free(result);

	return time;

}

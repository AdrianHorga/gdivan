#include <iostream>
#include "Handler.h"
#include "../server/RemoteServer.h"
#include "../kernels/convolution/convolution.h"
#include <assert.h>


void ConvolutionHandler::handleKernel(RemoteRequest *request){
	std::cout << "\nEntering Convolution handler\n" << std::endl;

	//get input
	int inputs;
	int datasize;
	int *data;

	request->receiveData(&inputs, sizeof(int), 1, "Input");
	//assert(inputs == 2);
#if DEBUG_HANDLER
	std::cout << "inputs = " << inputs << std::endl;
#endif

	for (int i = 0; i < inputs; i++){
		//get the data size
		request->receiveData(&datasize, sizeof(int), 1, "Data size");

#if DEBUG_HANDLER
		std::cout << "datasize = " << datasize << std::endl;
#endif

		data = (int*)malloc(datasize * sizeof(int));

		request->receiveData(data, datasize * sizeof(int), datasize, "Array for input");
	}

#if DEBUG_HANDLER

	std::cout << "Before convolution" << std::endl;
	for (int i = 0; i < datasize; i++) {
		std::cout << data[i] << "\n";
	}
	std::cout << std::endl;
#endif

	int blocks, threads;
	threads = 512;
	blocks = (datasize - 1)/threads + 1;

	int diameter = 8;
	int *filter;
	filter = (int*)malloc(sizeof(int) * diameter);

	for (int i = diameter; i > 0; i--){
		filter[i - 1] = i;
	}

	float time = 0;
	for (int i = 0 ; i < 10; i++){
		time += runConvolution(data, filter, diameter, blocks, threads);
	}
	time = time / 10;


	free(data);
	free(filter);

#if DEBUG_HANDLER
	std::cout << "Time : " << time << std::endl;
#endif

	request->sendData(&time, sizeof(float), 1, "Execution time");
}

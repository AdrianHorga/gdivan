/*
 * Handler.h
 *
 *      Author: adrianh
 */

#ifndef HANDLER_H_
#define HANDLER_H_

#define DEBUG_HANDLER 0

#include "../server/RemoteServer.h"
#include "Helpers.h"

class Handler{
public:
	virtual void handleKernel(RemoteRequest* request)=0;
	virtual ~Handler(){};
};


class BFSHandler : public Handler{
public:
	BFSHandler() : Handler(){}
	virtual void handleKernel(RemoteRequest* request);
};

class ControlledHandler : public Handler{
public:
	ControlledHandler() : Handler(){}
	virtual void handleKernel(RemoteRequest* request);

};


class NSICHNEUHandler : public Handler{
public:
	NSICHNEUHandler() : Handler(){}
	virtual void handleKernel(RemoteRequest* request);
};

class LBMSailfishHandler : public Handler{
public:
	LBMSailfishHandler() : Handler(){}
	virtual void handleKernel(RemoteRequest* request);
};

class ConvolutionHandler : public Handler{
public:
	ConvolutionHandler() : Handler(){}
	virtual void handleKernel(RemoteRequest* request);

};

#endif /* HANDLER_H_ */

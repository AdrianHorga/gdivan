#include <iostream>
#include "Handler.h"
#include "../kernels/bfs/bfs.h"
#include "../server/RemoteServer.h"


void BFSHandler::handleKernel(RemoteRequest *request){
	std::cout << "\nEntering BFS handler\n" << std::endl;
	//get input
	int inputs = 2;
	int datasize;
	unsigned int nodes, edges;
	int* graph_edges;
	Node* graph_nodes;

	//should be 2
	request->receiveData(&inputs, sizeof(int), 1, "Input");
#if DEBUG_HANDLER
	std::cout << "inputs = " << inputs << std::endl;
#endif


	//only nodes and edge list are to be sent , so 2 inputs
	//get the data size
	//request->receiveData(&datasize, sizeof(int), 1, "Data size");
	request->receiveData(&nodes, sizeof(int), 1, "Data size");
	graph_nodes = new Node[nodes];

#if 1
	std::cout << "nodes = " << nodes << std::endl;
#endif

	request->receiveData(graph_nodes, nodes * sizeof(Node), nodes, "Array for input");


#if DEBUG_HANDLER
	std::cout << "Before bfs - nodes" << std::endl;
	for (int i = 0; i < nodes; i++) {
		std::cout << graph_nodes[i].starting << graph_nodes[i].no_of_edges << " ";
	}
	std::cout << std::endl;
#endif

	request->receiveData(&edges, sizeof(int), 1, "Data size");
	graph_edges = new int[edges];
#if 1
	std::cout << "edges = " << edges << std::endl;
#endif

	request->receiveData(graph_edges, edges * sizeof(int), edges, "Array for input");

	int blocks, threads;
	threads = 256;
	blocks = (int)ceil(nodes/(double)threads);
	std::cout << "blocks = " << blocks << " ; " << "threads = " << threads << std::endl;
	float time = 0;
	for (int i = 0 ; i < 10; i++){
		time += runBfs(graph_nodes, graph_edges, edges, blocks, threads);
	}
	time = time / 10;

#if DEBUG_HANDLER
	std::cout << "After bfs - raw data" << std::endl;
	//	for (int i = 0; i < datasize; i++) {
	//		std::cout << rawData[i] << " ";
	//	}
	std::cout << std::endl;
#endif

	delete graph_nodes;
	delete graph_edges;

#if DEBUG_HANDLER
	std::cout << "Time : " << time << std::endl;
#endif

	request->sendData(&time, sizeof(float), 1, "Execution time");
}


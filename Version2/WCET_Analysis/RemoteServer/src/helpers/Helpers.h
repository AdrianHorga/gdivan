/*
 * Helpers.h
 *
 *      Author: adrianh
 */

#ifndef HELPERS_H_
#define HELPERS_H_

enum KernelCode{CONTROLLED, BFS, NSICHNEU, LBM_SAILFISH, CONVOLUTION};

class MeasureCUDA{
public:
	void startMeasuring(){
		cudaEventCreate(&start);
		cudaEventCreate(&stop);
		cudaEventRecord(start,0);
	}

	void stopMeasuring(){
		cudaEventRecord(stop,0);
		cudaEventSynchronize(stop);
		cudaEventElapsedTime(&time, start, stop);
	}

	float getTime(){
		return (time);
	}


private:
	cudaEvent_t start, stop;
	float  time;
};




#endif /* HELPERS_H_ */

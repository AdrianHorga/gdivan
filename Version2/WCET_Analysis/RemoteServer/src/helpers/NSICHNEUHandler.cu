#include <iostream>
#include "Handler.h"
#include "../kernels/nsichneu/cuda_nsichneu.h"
#include "../server/RemoteServer.h"


void NSICHNEUHandler::handleKernel(RemoteRequest *request){
	std::cout << "\nEntering NSICHNEU handler\n" << std::endl;
	//get input
	int inputs;
	int datasize;
	int *marked[3];

	request->receiveData(&inputs, sizeof(int), 1, "Input");
#if DEBUG_HANDLER
	std::cout << "inputs = " << inputs << std::endl;
#endif

	for (int i = 0; i < inputs; i++){
		//get the data size
		request->receiveData(&datasize, sizeof(int), 1, "Data size");

#if DEBUG_HANDLER
		std::cout << "datasize = " << datasize << std::endl;
#endif

		marked[i] = (int*)malloc(datasize * sizeof(int));

		request->receiveData(marked[i], datasize * sizeof(int), datasize,
				"Array for input");
	}

#if DEBUG_HANDLER
	std::cout << "Before nsichneu" << std::endl;
	for (int i = 0; i < datasize; i++) {
		std::cout << marked[0][i] << " ";
	}
	std::cout << std::endl;
#endif

	float time =0;
	time = runNSICHNEU(datasize, marked[0], marked[1], marked[2]);



#if DEBUG_HANDLER
	std::cout << "After nsichneu" << std::endl;
	for (int i = 0; i < datasize; i++) {
		std::cout << marked[0][i] << " ";
	}
	std::cout << std::endl;
#endif

	free(marked[0]);
	free(marked[1]);
	free(marked[2]);

#if DEBUG_HANDLER
	std::cout << "Time : " << time << std::endl;
#endif

	request->sendData(&time, sizeof(float), 1, "Execution time");
}


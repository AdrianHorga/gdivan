/*
 * HandlerFactory.h
 *
 *      Author: adrianh
 */

#ifndef HANDLERFACTORY_H_
#define HANDLERFACTORY_H_

#include "Handler.h"

class HandlerFactory{
public:
	HandlerFactory(){}
	static Handler* getNewHandler(int code){
		KernelCode codeK = (KernelCode)code;

		if (codeK == KernelCode::CONTROLLED){
			return (new ControlledHandler());
		}else
			if (codeK == KernelCode::BFS){
				return (new BFSHandler());
			}else

				if (codeK == KernelCode::NSICHNEU){
					return (new NSICHNEUHandler());
				}else
					if (codeK == KernelCode::LBM_SAILFISH){
						return (new LBMSailfishHandler());
					}else
						if (codeK == KernelCode::CONVOLUTION){
							return (new ConvolutionHandler());
						}//more to come, probably

		return 0; //when all else fails
	}
};



#endif /* HANDLERFACTORY_H_ */

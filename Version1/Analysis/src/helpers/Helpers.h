/*
 * Helpers.h
 *
 *      Author: adrianh
 */

#ifndef HELPERS_HELPERS_H_
#define HELPERS_HELPERS_H_

enum KernelCode{CONTROLLED, BFS, NSICHNEU, LBM_SAILFISH};


#endif /* HELPERS_HELPERS_H_ */

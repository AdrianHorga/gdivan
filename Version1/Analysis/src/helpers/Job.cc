/*
 * Job.cc
 *
 *      Author: adrianh
 */

#include "Job.h"

#include <iostream>
#include <fstream>

std::vector<int> Job::generateData(RunType type){
	//	std::string filename = createInputFile(type, "dummy");
	//	std::vector<int> data = readFile(filename);
	//	std::string command;
	//
	//	command = "rm -f " + filename;
	//	system(command.c_str());
	int *data = createInput(type);
	std::vector<int> dataVector;
	dataVector.assign(data, data+getInputSize());
	free(data);
	return dataVector;
}

std::string getExtesion(RunType runtype){
	std::string extension = "default";
	if (runtype == RunType::RANDOM){
		extension = "random";
	}

	return extension;
}

std::string getExtesion(RunType runtype, KernelCode code){
	std::string extension = "default";

	if (runtype == RunType::RANDOM){
		extension = "random";
	}

	return extension;
}


Job::~Job(){}

void Job::setupFolders(){
	std::string command;

	command = "mkdir -p " + samples_folder;
	system(command.c_str());

	command = "mkdir -p " + inputs_folder;
	system(command.c_str());

	command = "mkdir -p " + results_folder;
	system(command.c_str());

	std::ofstream plotfile;
	plotfile.open(results_folder + "/fitness.result");
	plotfile.close();
	plotfile.open(results_folder + "/step.result");
	plotfile.close();
	plotfile.open(results_folder + "/max.result");
	plotfile.close();
}

void Job::cleanFolders(){
	std::string command;

	command = "rm -f " + samples_folder + "/*.sample";
	system(command.c_str());

	command = "rm -f " + inputs_folder + "/*.input";
	system(command.c_str());

	command = "rm -f " + results_folder + "/*.result";
	system(command.c_str());
}




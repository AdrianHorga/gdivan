/*
 * TimerHelper.h
 *
 *      Author: adrianh
 */

#ifndef HELPERS_TIMERHELPER_H_
#define HELPERS_TIMERHELPER_H_

#include <iostream>
#include <chrono>
#include <ctime>

class TimerHelper{
public:
	static void startTimer();
	static void stopTimer();
	static double getElapsedSeconds();
	static std::string getCurrentTime();

private:
	static std::chrono::time_point<std::chrono::system_clock> startTime;
	static std::chrono::time_point<std::chrono::system_clock> currentTime;
};


#endif /* HELPERS_TIMERHELPER_H_ */

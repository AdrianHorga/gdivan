/*
 * TimerHelper.cc
 *
 *      Author: adrianh
 */

#include "TimerHelper.h"
#include <array>

std::chrono::time_point<std::chrono::system_clock> TimerHelper::startTime;
std::chrono::time_point<std::chrono::system_clock> TimerHelper::currentTime;

void TimerHelper::startTimer(){
	startTime = std::chrono::system_clock::now();
}

void TimerHelper::stopTimer(){
	currentTime = std::chrono::system_clock::now();
}

double TimerHelper::getElapsedSeconds(){
	std::chrono::duration<double> elapsed_seconds = currentTime - startTime;
	double seconds = elapsed_seconds.count();
	return (seconds);
}

std::string TimerHelper::getCurrentTime(){
	 std::array<char, 64> buffer;
	 buffer.fill(0);
	 time_t rawtime;
	 time(&rawtime);
	 const auto timeinfo = localtime(&rawtime);
	 strftime(buffer.data(), sizeof(buffer), "%Y-%m-%d_%H:%M:%S", timeinfo);
	 std::string timeStr(buffer.data());

	 return timeStr;
}



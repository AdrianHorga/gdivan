/*
 * GASettings.h
 *
 *      Author: adrianh
 */

#ifndef GENETIC_ALGORITHM_GASETTINGS_H_
#define GENETIC_ALGORITHM_GASETTINGS_H_

#include <iostream>
#include <fstream>
#include <string>

class GASettings{
public:
	//GASettings();
	GASettings(int populationSize, int maximumIterations, float diversificationRate, float eliteRate, float mutationRate, int tournament, int renewPopulationStep);
	//void readSettings(std::string filename);
	int getPopulationSize() const;
	int getMaximumIterations() const;
	int getRenewStep() const;
	float getDiversificationRate() const;
	float getEliteRate() const;
	float getMutationRate() const;
	int getTournamentSize() const;

private:
	int populationSize = 100;
	int maximumIterations = 1000;
	float eliteRate = 0.1;
	float mutationRate = 0.25;
	int tournamentSize = 10;
	float specialDiversificationRate = 0.5;
	int renewPopulationStep = 5;
};

GASettings* readSettings(std::string filename);

#endif /* GENETIC_ALGORITHM_GASETTINGS_H_ */

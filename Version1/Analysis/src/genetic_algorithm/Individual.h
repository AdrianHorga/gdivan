/*
 * Individual.h
 *
 *      Author: adrianh
 */

#ifndef GENETIC_ALGORITHM_INDIVIDUAL_H_
#define GENETIC_ALGORITHM_INDIVIDUAL_H_

class Individual{
public:
	Individual();
	float getFitness() const;
	virtual ~Individual();
	virtual unsigned getGenotypeSize() const = 0;
	virtual void computeFitness() = 0;
	bool isNewlyBorn();
	void setOld();

protected:
	float fitness;
	bool compute = true;
};



#endif /* GENETIC_ALGORITHM_INDIVIDUAL_H_ */

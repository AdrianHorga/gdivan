/*
 * Population.cc
 *
 *      Author: adrianh
 */

#include "Population.h"
#include <algorithm>

#include "../helpers/TimerHelper.h"

std::string Population::fitnessOutput = "fitness";
std::string Population::fitnessSortedOutput = "sorted";
std::string Population::fitnessStepOutput = "step";
std::string Population::fitnessMaxOutput = "max";
std::string Population::wcetOutput = "wcet";

float Population::maxFitness = 0;
int Population::maxFitnessIteration = 0;


Population::Population(PopulationManager* manager, GASettings* settings): manager(manager), populationSettings(settings){
}

Population::Population(PopulationManager* manager, GASettings* settings, std::string ext) : manager(manager),
		populationSettings(settings), extension(ext){
	fitnessOutput += extension;
	//fitnessSortedOutput += extension;
	fitnessStepOutput += extension;
	fitnessMaxOutput += extension;
	wcetOutput += extension;
}

Population::~Population(){
	delete manager;
	delete populationSettings;
	//delete individuals
	for (Individual *ind : individuals){
		delete ind;
	}
	individuals.clear();
}

void Population::initPlotFiles(){
	//plotting setup
	std::string command = "mkdir -p " + manager->getOutputFolder();
	system(command.c_str());
	std::ofstream plotfile;
	std::string location = manager->getOutputFolder() + "/" + fitnessOutput;
	plotfile.open(location.c_str());
	plotfile.close();
	//	location = manager->getOutputFolder() + "/" + fitnessSortedOutput;
	//	plotfile.open(location.c_str());
	//	plotfile.close();
	location = manager->getOutputFolder() + "/" + fitnessStepOutput;
	plotfile.open(location.c_str());
	plotfile.close();
	location = manager->getOutputFolder() + "/" + fitnessMaxOutput;
	plotfile.open(location.c_str());
	plotfile.close();

	location = manager->getOutputFolder() + "/" + wcetOutput;
	plotfile.open(location.c_str());
	plotfile.close();
}

void Population::writeFitnessToPlotFile(float fitness){
	std::string location = manager->getOutputFolder() + "/" + fitnessMaxOutput;

	//write max fitness
	if (maxFitness < fitness){
		maxFitness = fitness;
		std::ofstream plotfile;
		plotfile.open(location, std::ios_base::app);
		plotfile << maxFitnessIteration << " " << maxFitness << std::endl;
		plotfile.close();
	}
	maxFitnessIteration++; //which individual has his fitness computed now
	//end write max fitness


	//write normal fitness
	location = manager->getOutputFolder() + "/" + fitnessOutput;
	std::ofstream plotfile;
	plotfile.open(location, std::ios_base::app);
	plotfile << fitness << std::endl;
	plotfile.close();
	//end write normal
}

void Population::writeSortedIndividualsToPlotFile(int iteration){
	std::ofstream plotfile;
	std::string location = manager->getOutputFolder() + "/" + fitnessSortedOutput;
	//write for plotting sorted
	plotfile.open(location, std::ios_base::app);
	for (const Individual *el : individuals){
		float fit = el->getFitness();
		plotfile << iteration << " " << fit << std::endl;
	}
	plotfile.close();
	//end write
}

void Population::writeStepToPlotFile(int iteration){
	std::ofstream plotfile;
	std::string location = manager->getOutputFolder() + "/" + fitnessStepOutput;
	//write after each population
	plotfile.open(location, std::ios_base::app);
	plotfile << iteration << " " << getTopIndividual()->getFitness() << std::endl;
	plotfile.close();
	//end write
}

void Population::computeFitness(){
	for (auto indv : individuals){
		if (indv->isNewlyBorn()){
			indv->computeFitness();
			writeFitnessToPlotFile(indv->getFitness());
		}
	}
}

Individual* Population::getTopIndividual(){
	return (individuals[0]);
}

//sort for descending order - original ">"
inline bool populationComparator(Individual* x, Individual* y){
	return (x->getFitness() > y->getFitness());
}

void Population::sortByFitness(){
	std::sort(individuals.begin(), individuals.end(), populationComparator);
}

//killing one child from crossover version
//void Population::mate(){
//	std::vector<Individual*> auxiliary;
//
//	//elitism part
//	int elite = populationSettings->getEliteRate() * populationSettings->getPopulationSize();
//	for (int i = 0 ; i < elite; i++){
//		individuals[i]->setOld();
//		auxiliary.push_back(individuals[i]);
//	}
//
//	//crossover
//	int popSize = populationSettings->getPopulationSize();
//	for (int i = elite; i < popSize ; i++) {
//		int pos1, pos2;
//		//		give more chances to elites
//		int which = rand() % 100;
//		if (which < 66){
//			if (elite < 2){
//				pos1 = rand() % popSize;
//			}else{
//				pos1 = rand() % elite; //choose an elite individual
//			}
//		}else{
//			pos1 = rand() % popSize; //choose a random individual, elite included
//		}
//
//		if (which < 50){
//			if (elite < 2){
//				pos2 = rand() % popSize;
//			}else{
//				pos2 = rand() % elite; //choose an elite individual
//			}
//		}else{
//			pos2 = rand() % popSize; //choose a random individual, elite included
//		}
//		//pos1 = rand() % popSize;
//		//pos2 = rand() % popSize;
//
//		Individual* child1 = manager->generateIndividual(0);
//		Individual* child2 = manager->generateIndividual(0);
//		manager->crossover(individuals[pos1], individuals[pos2], child1, child2);
//
//		Individual* picked;
//		int who = rand() % 2;
//		if (who == 0){
//			picked = child1;
//			delete child2;
//		}else{
//			picked = child2;
//			delete child1;
//		}
//		//picked = (who == 0)?child1:child2;
//
//
//		double mutationChance = distribution(generator);
//		if (mutationChance < populationSettings->getMutationRate()){
//			manager->mutate(picked);
//		}
//		auxiliary.push_back(picked);
//	}
//
//	//keep elite, delete the rest
//	for (int i = elite; i < individuals.size(); ++i) {
//		delete individuals[i]; // Calls ~object and deallocates *individuals[i]
//	}
//	individuals.clear();
//	//put the auxiliary in place of the current population
//	individuals.insert(individuals.begin(), auxiliary.begin(), auxiliary.end());
//
//}

void Population::mate(){
	std::vector<Individual*> auxiliary;

	//elitism part
	int elite = populationSettings->getEliteRate() * populationSettings->getPopulationSize();
	for (int i = 0 ; i < elite; i++){
		individuals[i]->setOld();
		auxiliary.push_back(individuals[i]);
	}

	//crossover
	int popSize = populationSettings->getPopulationSize();
	int remaining = popSize - elite;
	while(remaining > 0) {
		int pos1, pos2;
		int set = rand() % 2;
		if (set == 0){
			pos1 = rand() % elite;//top individuals
		}else{
			pos1 = rand() % popSize;//everyone
		}

		do{
			pos2 = rand() % popSize;
		}while (pos1 == pos2);



		Individual* child1 = manager->generateIndividual(0);
		Individual* child2 = manager->generateIndividual(0);
		manager->crossover(individuals[pos1], individuals[pos2], child1, child2);

		if (remaining > 0){
			double mutationChance = distribution(generator);
			if (mutationChance < populationSettings->getMutationRate()){
				manager->mutate(child1);
			}
			auxiliary.push_back(child1);
			remaining--;
		}else{
			delete child1;
		}

		if (remaining > 0){
			double mutationChance = distribution(generator);
			if (mutationChance < populationSettings->getMutationRate()){
				manager->mutate(child2);
			}
			auxiliary.push_back(child2);
			remaining--;
		}else{
			delete child2;
		}
	}

	//keep elite, delete the rest
	for (int i = elite; i < individuals.size(); ++i) {
		delete individuals[i]; // Calls ~object and deallocates *individuals[i]
	}
	individuals.clear();
	//put the auxiliary in place of the current population
	individuals.insert(individuals.begin(), auxiliary.begin(), auxiliary.end());

	if (individuals.size() != popSize){
		std::cout << individuals.size() << popSize << "SOMETHING IS WRONG WITH THE POPULATION SIZE AT CROSSOVER!!!" << std::endl;
		exit(-1);
	}

}

void Population::renewPopulation(unsigned sizeOfChromosomes){

	std::vector<Individual*> auxiliary;
	//elitism part
	int popSize = populationSettings->getPopulationSize();
	int elite = populationSettings->getEliteRate() * populationSettings->getPopulationSize();
	for (int i = 0 ; i < elite; i++){
		individuals[i]->setOld();
		auxiliary.push_back(individuals[i]);
	}

	//generate new random individuals
	while (auxiliary.size() != popSize){
		Individual* individual = manager->generateRandomIndividual(sizeOfChromosomes);
		auxiliary.push_back(individual);
	}

	//keep elite, delete the rest
	for (int i = elite; i < individuals.size(); ++i) {
		delete individuals[i]; // Calls ~object and deallocates *individuals[i]
	}
	individuals.clear();
	//put the auxiliary in place of the current population
	individuals.insert(individuals.begin(), auxiliary.begin(), auxiliary.end());
}

void Population::initializePopulation(unsigned sizeOfChromosomes){
	//create the population
	int popSize = populationSettings->getPopulationSize();
	//corner-case individuals
	float divRate = populationSettings->getDiversificationRate();
	int ccSize = popSize * divRate; //how many individuals are corner case

	// corner case individuals
	std::vector<Individual*> ccIndividuals = manager->generateCornerCaseIndividuals(ccSize, sizeOfChromosomes);
	ccSize = ccIndividuals.size();
	individuals.insert(individuals.begin(), ccIndividuals.begin(), ccIndividuals.end());

	//	random individuals
	for (int i = 0; i < popSize  - ccSize ; i++ ){
		Individual* individual = manager->generateIndividual(sizeOfChromosomes);
		individuals.push_back(individual);
	}
}

void Population::writeToLogFile(int iteration, double fitness, double time){
	std::ofstream plotfile;
	std::string location = manager->getOutputFolder() + "/" + wcetOutput;
	//write after each population
	plotfile.open(location, std::ios_base::app);
	plotfile << "Iteration : " << iteration << " best fitness : " << fitness << " ms "
			<< " found after " << time << " seconds" << std::endl;
	plotfile.close();
	//end write
}

void Population::runGeneticAlgorithm(unsigned sizeOfChromosomes){

	initializePopulation(sizeOfChromosomes);
	initPlotFiles();

	float fitnessMax = 0;
	int renewStep = populationSettings->getRenewStep();
	int renewIn = renewStep;

	int maxIter = populationSettings->getMaximumIterations();
	for (int i = 0; i < maxIter; i++){
		std::cout << "Iteration : " << i;
		computeFitness();

		sortByFitness();

		//print if something changed
		TimerHelper::stopTimer();
		double currentTime = TimerHelper::getElapsedSeconds();
		float topFitness = getTopIndividual()->getFitness();
		if (fitnessMax < topFitness){
			std::cout << " best fitness : " << topFitness << " ms "
					<< " found after " << currentTime << " seconds";
			writeToLogFile(i, topFitness, currentTime);
			fitnessMax = topFitness;
			renewIn = renewStep;
		}

		//		writeSortedIndividualsToPlotFile(i);

		writeStepToPlotFile(i);


		if (renewIn > 0){
			mate();
			renewIn--;
		}else{
			std::cout << " nothing changed after " <<  renewStep << " steps,"
					<< " renewing population";
			renewPopulation(sizeOfChromosomes);
			renewIn = renewStep;
		}

		std::cout << std::endl;
	}
}



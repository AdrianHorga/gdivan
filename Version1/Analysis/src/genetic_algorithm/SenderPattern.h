/*
 * SenderPattern.h
 *
 *      Author: adrianh
 */

#ifndef GENETIC_ALGORITHM_SENDERPATTERN_H_
#define GENETIC_ALGORITHM_SENDERPATTERN_H_

#include "../remote/RemoteClient.h"

#include <string>
#include <map>
#include <vector>
#include <math.h>

template <typename T>
class CommunicationPattern{
public:
	CommunicationPattern(std::string addressName, std::string portNo) : address(addressName), portNumber(portNo){}
	virtual float communicateData(std::map<std::string, std::vector<T> > inputs, int kernelCode){
		RemoteClient client(address, portNumber);
		client.connectToServer();
		int code = kernelCode;
		client.sendData(&code, sizeof(int), 1, "Kernel code");
		//std::cout << code << std::endl;

		int howMany;
		howMany = inputs.size();

		client.sendData(&howMany, sizeof(int), 1 , "How many input vectors will be sent");

		//iterate through all the inputs map to generate data to be transmitted
		for(const auto& item : inputs){
			//			std::cout << item.first << std::endl;
			//send size of the input
			int dataSize = item.second.size();
			client.sendData(&dataSize, sizeof(int), 1, "Data size for input named : " + item.first);
			//		std::cout << item.first << std::endl;

			int sizeOfData = sizeof(T) * item.second.size();
			int noOfElements = item.second.size();
			//std::cout << " size of T " << sizeof(T) << std::endl;
			T* data = (T*)malloc(sizeOfData);
			//copy the input into the array
			int j = 0;
			for (const auto &el : item.second){
				data[j] = el;
				//data[j] = j;
				//data[j] = item.second.size() - j; //specific values for testing
				j++;
			}

			//			std::ofstream dataFile;
			//			long long int add = reinterpret_cast<uintptr_t>(this);
			//			dataFile.open("individuals/" + std::to_string(add));
			//			//print the data to send
			//			int repeat = 0;
			//			for (j=0; j < dataSize; j++){
			//				dataFile << data[j] << " ";
			//				repeat++;
			//				if (repeat == 4){
			//					dataFile<< std::endl;
			//					repeat = 0;
			//				}
			//			}
			//			dataFile << std::endl;
			//			dataFile.close();

			//send the input
			client.sendData(data, sizeOfData, noOfElements, "Input named : " + item.first);

			free(data);
		}

		float fit;
		client.receiveData(&fit, sizeof(float), 1, "fitness");
		//std::cout << "Received fitness : " << fit << std::endl;

		client.disconnectFromServer();

		return fit;
	}
	virtual ~CommunicationPattern(){}


protected:
	std::string address;
	std::string portNumber;
};

//Structure to hold a node information
struct Node
{
	int starting;
	int no_of_edges;
};

template <typename T>
class CommunicationPatternBFS : public CommunicationPattern<T>{
public:
	CommunicationPatternBFS(std::string addressName, std::string portNo, unsigned int threads, unsigned int edges, unsigned int chromosomes) :
		CommunicationPattern<T>(addressName, portNo), smallThreads(threads), smallEdges(edges), totalSize(chromosomes) {}
	virtual float communicateData(std::map<std::string, std::vector<T> > inputs, int kernelCode){
		RemoteClient client(CommunicationPattern<T>::address, CommunicationPattern<T>::portNumber);
		client.connectToServer();
		int code = kernelCode;
		client.sendData(&code, sizeof(int), 1, "Kernel code");
		//std::cout << code << std::endl;

		int howMany;
		howMany = 2; //nodes and edges
		client.sendData(&howMany, sizeof(int), 1 , "How many input vectors will be sent");

		//compute the sizes
		double total = totalSize * ((smallThreads * smallThreads)/((double)smallEdges));
		unsigned int nodes = sqrt(total);
		//unsigned lineSize = (smallThreads - 1) * nodes / smallThreads;

		//need a nodes X nodes size matrix
		int **matrix = new int*[nodes];
		for (unsigned int i = 0 ; i < nodes; i++){
			matrix[i] = new int[nodes];
		}

		for (unsigned int i = 0 ; i < nodes; i++){
			for (unsigned int j = 0; j < nodes; j++){
				matrix[i][j] = 0;
			}
		}

		float fit;

		//only one input
		for(const auto& item : inputs){
			std::vector<T> elements = item.second;
			unsigned int index = 0;
			unsigned int xOffset = 0;
			unsigned int yOffset = 0;
			while (index < elements.size()){
				for (unsigned int i = 0 ; i < smallThreads; i++){
					//for (unsigned int j = 0 ; j < smallThreads - 1; j++){
					for (unsigned int j = 0 ; j < smallThreads; j++){
						unsigned int whichNode = elements[index];
						matrix[xOffset + i][yOffset + whichNode] = 1;

						index++;
					}
				}
				int newY = yOffset + smallThreads;
				int remainder = newY / nodes;
				yOffset = newY % nodes;
				xOffset += remainder * smallThreads;
			}



			//			for (unsigned int i = 0 ; i < elements.size(); i++){
			//				std::cout << elements[i] << " ";
			//			}
			//			std::cout << std::endl;
			//
			//			for (unsigned int i = 0 ; i < nodes; i++){
			//				for (unsigned int j = 0; j < nodes; j++){
			//					std::cout << matrix[i][j] << " ";
			//				}
			//				std::cout << std::endl;
			//			}

		}
		//		for (unsigned int i = 0 ; i < nodes; i++){
		//			for (unsigned int j = 0 ; j < nodes; j++){
		//				matrix[i][j] = 1;
		//			}
		//		}

		//see the edges total and the nodes' start positions
		Node*  graph_nodes = new Node[nodes];
		for (unsigned int i = 0 ; i < nodes; i++){
			graph_nodes[i].no_of_edges = 0;
			graph_nodes[i].starting = 0;
		}
		int* graph_edges;
		unsigned int edges = 0;
		for (unsigned int i = 0 ; i < nodes; i++){
			unsigned int nodeEdges = 0;
			for (unsigned int j = 0; j < nodes; j++){
				if (matrix[i][j] == 1){
					edges++;
					nodeEdges++;
				}
			}
			graph_nodes[i].no_of_edges = nodeEdges;
			if (i > 0){
				graph_nodes[i].starting = graph_nodes[i-1].starting + graph_nodes[i-1].no_of_edges;
			}
		}

		graph_edges = new int[edges];
		int index = 0;
		for (unsigned int i = 0 ; i < nodes; i++){
			for (unsigned int j = 0; j < nodes; j++){
				if (matrix[i][j] == 1){
					graph_edges[index] = j;
					index++;
				}
			}
		}

		//std::cout << "nodes= " << nodes << " edges= " << edges << std::endl;

		//send the nodes
		//how many
		client.sendData(&nodes, sizeof(int), 1, "Data size for input named : nodes");
		//send nodes
		client.sendData(graph_nodes, nodes * sizeof(Node), nodes, "Input named : nodes");

		//send the edges
		//how many
		client.sendData(&edges, sizeof(int), 1, "Data size for input named : edges");
		//send nodes
		client.sendData(graph_edges, edges * sizeof(int), edges, "Input named : edges");


		//		std::cout << "Node.edges Node.starting" << std::endl;
		//		for (unsigned int i = 0 ; i < nodes; i++){
		//			std::cout << graph_nodes[i].no_of_edges << " " << graph_nodes[i].starting << std::endl;
		//		}
		//		std::cout << std::endl;
		//
		//		std::cout << "Edges" << std::endl;
		//		for (unsigned int i = 0 ; i < edges; i++){
		//			std::cout << graph_edges[i] << std::endl;
		//		}
		//		std::cout << std::endl;

		client.receiveData(&fit, sizeof(float), 1, "fitness");
		//std::cout << "Received fitness : " << fit << std::endl;

		client.disconnectFromServer();

		for (unsigned int i = 0 ; i < nodes; i++){
			delete matrix[i];
		}
		delete matrix;
		delete graph_edges;
		delete graph_nodes;

		return fit;
	}

	virtual ~CommunicationPatternBFS(){}
private:
	unsigned int smallThreads;
	unsigned int smallEdges;
	unsigned int totalSize;
};

template <typename T>
class CommunicationPatternLBM : public CommunicationPattern<T>{
public:
	CommunicationPatternLBM(std::string addressName, std::string portNo, unsigned int threads, unsigned int width, unsigned int height) :
		CommunicationPattern<T>(addressName, portNo), smallThreads(threads), matrixWidth(width), matrixHeight(height) {}
	virtual float communicateData(std::map<std::string, std::vector<T> > inputs, int kernelCode){
		RemoteClient client(CommunicationPattern<T>::address, CommunicationPattern<T>::portNumber);
		client.connectToServer();
		int code = kernelCode;
		client.sendData(&code, sizeof(int), 1, "Kernel code");
		//std::cout << code << std::endl;

		int howMany;
		howMany = inputs.size();

		client.sendData(&howMany, sizeof(int), 1 , "How many input vectors will be sent");

		//iterate through all the inputs map to generate data to be transmitted
		for(const auto& item : inputs){
			//			std::cout << item.first << std::endl;
			//send size of the input
			int dataSize = item.second.size();
			client.sendData(&dataSize, sizeof(int), 1, "Data size for input named : " + item.first);
			//		std::cout << item.first << std::endl;

			unsigned int sizeOfData = sizeof(T) * item.second.size();
			unsigned int noOfElements = item.second.size();
			//std::cout << " size of T " << sizeof(T) << std::endl;
			T* data = (T*)malloc(sizeOfData);
			//copy the input into the array
			int j = 0;
			for (const auto &el : item.second){
				data[j] = el;
				j++;
			}

			client.sendData(&matrixWidth, sizeof(int), 1 , "Matrix width");
			client.sendData(&matrixHeight, sizeof(int), 1 , "Matrix height");

			T* arrangedData = (T*)malloc(sizeOfData);
			unsigned int index = 0;
			unsigned int xOffset = 0;
			unsigned int yOffset = 0;

			while (index < noOfElements){
				for(unsigned int y = 0; y < smallThreads; y++){
					for (unsigned int x = 0 ; x < smallThreads; x++){
						arrangedData[(yOffset + y) * matrixWidth + xOffset + x] = data[index];
						index++;
					}
				}
				int newX = xOffset + smallThreads;
				int remainder = newX / matrixWidth;
				xOffset = newX % matrixWidth;
				yOffset += remainder * smallThreads;
			}


			//send the input
			client.sendData(arrangedData, sizeOfData, noOfElements, "Input named : " + item.first);

			free(arrangedData);
			free(data);
		}

		float fit;
		client.receiveData(&fit, sizeof(float), 1, "fitness");
		//std::cout << "Received fitness : " << fit << std::endl;

		client.disconnectFromServer();

		return fit;
	}

	virtual ~CommunicationPatternLBM(){}
private:
	unsigned int smallThreads;
	unsigned int matrixWidth;
	unsigned int matrixHeight;

};


#endif /* GENETIC_ALGORITHM_SENDERPATTERN_H_ */

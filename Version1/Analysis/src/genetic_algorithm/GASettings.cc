/*
 * GASettings.cc
 *
 *      Author: adrianh
 */
#include "GASettings.h"

//GASettings::GASettings(){}

GASettings::GASettings(int populationSize, int maximumIterations,
		float diversificationRate, float eliteRate, float mutationRate, int tournament, int renewPopulationStep):
		populationSize(populationSize), maximumIterations(maximumIterations), specialDiversificationRate(diversificationRate),
		eliteRate(eliteRate), mutationRate(mutationRate), tournamentSize(tournament), renewPopulationStep(renewPopulationStep){
}

GASettings* readSettings(std::string filename){
	std::ifstream infile;
	int populationSize;
	int maximumIterations;
	float diversificationRate;
	float eliteRate;
	float mutationRate;
	int tournamentSize;
	int renewPopulationStep;
	std::string parameterName;

	infile.open(filename.c_str());

	if (infile.is_open()){
		infile >> parameterName >> populationSize;
		//		std::cout<< "populationSize = " << populationSize << "\n";
		std::cout<< parameterName << " = " << populationSize << "\n";

		infile >> parameterName >> maximumIterations;
		//		std::cout<< "maximumIterations = " << maximumIterations << "\n";
		std::cout<< parameterName << " = " << maximumIterations << "\n";

		infile >>  parameterName >> diversificationRate;
		std::cout<< parameterName << " = " << diversificationRate << "\n";

		infile >>  parameterName >> eliteRate;
		std::cout<< parameterName << " = " << eliteRate << "\n";

		infile >>parameterName >> mutationRate;
		std::cout<< parameterName << " = " << mutationRate << "\n";

		infile >> parameterName >> tournamentSize;
		std::cout<< parameterName << " = " << tournamentSize << "\n";

		infile >>parameterName >> renewPopulationStep;
		std::cout<< parameterName << " = " << renewPopulationStep << "\n";

		infile.close();
	}else{
		std::cerr << "Could not open file : " + filename;
	}

	return (new GASettings(populationSize, maximumIterations, diversificationRate, eliteRate, mutationRate, tournamentSize, renewPopulationStep));
}

int GASettings::getPopulationSize() const{
	return (populationSize);
}

int GASettings::getMaximumIterations() const{
	return (maximumIterations);
}

int GASettings::getRenewStep() const{
	return (renewPopulationStep);
}

float GASettings::getDiversificationRate() const{
	return (specialDiversificationRate);
}

float GASettings::getEliteRate() const{
	return (eliteRate);
}

float GASettings::getMutationRate() const{
	return (mutationRate);
}

int GASettings::getTournamentSize() const{
	return (tournamentSize);
}



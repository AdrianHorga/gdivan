/*
 * PopulationManagerRandom.h
 *
 *      Author: adrianh
 */

#ifndef GENETIC_ALGORITHM_POPULATIONMANAGERRANDOM_H_
#define GENETIC_ALGORITHM_POPULATIONMANAGERRANDOM_H_

#include "PopulationManager.h"
#include "../helpers/Job.h"
#include "IndividualRandom.h"

class PopulationManagerRandom : public PopulationManager{
public:
	PopulationManagerRandom(Job* job, std::string address, std::string port, RunType runtype);
	virtual void crossover(Individual* parent1, Individual* parent2, Individual* child1, Individual* child2);
	virtual void mutate(Individual* individual);
	virtual Individual* generateIndividual(unsigned size);
	virtual Individual* generateRandomIndividual(unsigned size);
	virtual std::vector<Individual*> generateCornerCaseIndividuals(unsigned howMany, unsigned size);
	virtual ~PopulationManagerRandom(){}
	virtual std::string getOutputFolder(){
		return job->getOutputFolder();
	}

private:
	Job *job;
	std::string address;
	std::string port;
	RunType runType;
	static int generatedIndividuals;
};

#endif /* GENETIC_ALGORITHM_POPULATIONMANAGERRANDOM_H_ */

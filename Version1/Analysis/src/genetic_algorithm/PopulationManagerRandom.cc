/*
 * PopulationManagerRandom.cc
 *
 *      Author: adrianh
 */

#include "PopulationManagerRandom.h"
#include <iostream>

int PopulationManagerRandom::generatedIndividuals = 0;

PopulationManagerRandom::PopulationManagerRandom(Job *generatorJob, std::string addressDest, std::string portDest, RunType runtype): PopulationManager(){
	job = generatorJob;
	address = addressDest;
	port = portDest;
	runType = runtype;
}

void PopulationManagerRandom::crossover(Individual* parent1, Individual* parent2, Individual* child1, Individual* child2){
	IndividualRandom* p1 = dynamic_cast<IndividualRandom*>(parent1);
	IndividualRandom* p2 = dynamic_cast<IndividualRandom*>(parent2);
	IndividualRandom* ch1 = dynamic_cast<IndividualRandom*>(child1);
	IndividualRandom* ch2 = dynamic_cast<IndividualRandom*>(child2);


	unsigned length = p1->getGenotypeSize();

	unsigned crossoverPoint = rand() % length;
	//unsigned crossoverPoint = length / 2;

	for (unsigned i = 0 ; i < length; i++){
		int from1 = p1->getChromosome(i);
		int from2 = p2->getChromosome(i);
		if (i < crossoverPoint){
			ch1->addChromosome(from1);
			ch2->addChromosome(from2);
		}else{
			ch1->addChromosome(from2);
			ch2->addChromosome(from1);
		}
	}
	//child1 = ch1;
	//child2 = ch2;
}

void PopulationManagerRandom::mutate(Individual* individual){
	IndividualRandom *ind = dynamic_cast<IndividualRandom * >(individual);

	unsigned pointOfMutation  = rand() % ind->getGenotypeSize();

	int newSolution = job->getRandValue();

	ind->setChromosome(newSolution, pointOfMutation);
}

Individual* PopulationManagerRandom::generateRandomIndividual(unsigned size){
	int ID = generatedIndividuals++;


	//	std::string fileName = job->createInputFile(runType, std::to_string(ID));
	//	std::vector<int> data = job->readFile(fileName);
	//std::cout << "Generate new individual" << std::endl;
	std::vector<int> data;
	if (size != 0){
		data = job->generateData(runType);
	}
	IndividualRandom* ind = new IndividualRandom(job, ID, data, address, port);
	return (ind);
}

Individual* PopulationManagerRandom::generateIndividual(unsigned size){
	return generateRandomIndividual(size);
}

std::vector<Individual*> PopulationManagerRandom::generateCornerCaseIndividuals(unsigned howMany, unsigned size){
	std::vector<Individual*> individuals;
	//TODO maybe in the future generate specific individuals
	return individuals;
}


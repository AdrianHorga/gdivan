/*
 * RemoteClient.cpp
 *
 *      Author: adrianh
 */


#include "RemoteClient.h"

void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return (&(((struct sockaddr_in*)sa)->sin_addr));
	}

	return (&(((struct sockaddr_in6*)sa)->sin6_addr));
}

int RemoteClient::connectToServer(){
	struct addrinfo hints, *servinfo, *p;
	int rv;
	char s[INET6_ADDRSTRLEN];


	memset(&hints, 0, sizeof hints);
	//hints.ai_family = AF_UNSPEC;
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;

	if ((rv = getaddrinfo(address.c_str(), port.c_str(), &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return (-1);
	}

	// loop through all the results and connect to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("client: socket");
			continue;
		}

		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			perror("client: connect");
			close(sockfd);
			continue;
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "client: failed to connect\n");
		return (-2);
	}

	inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
			s, sizeof s);
#if DEBUG
	printf("client: connecting to %s\n", s);
#endif

	freeaddrinfo(servinfo); // all done with this structure

	return (sockfd);
}

int RemoteClient::sendData(void *data, int size, int noOfElements, string message) const{
	int numbytes = 0;

#if DEBUG
	cout << "Sending - " << message << endl;
#endif

	char *pointerData = (char*)data;
	int bytesSent = 0;
	int bytesLeft = size;
	while (bytesLeft != 0){
		if ((numbytes = send(sockfd, &pointerData[bytesSent], bytesLeft, 0)) == -1){
			string errMsg = "Error at sending : " + message;
			perror(errMsg.c_str());
			return (-1);
		}
		//			cout << "numbytes: " << numbytes << "; bytesSent: " << bytesSent << std::endl;
		bytesSent += numbytes;
		bytesLeft -= numbytes;
	}

#if DEBUG
	cout << "Sent - " << message << endl;
#endif

	return (1);
}

int RemoteClient::receiveData(void *data, int size, int noOfElements, string message) const {
	int numbytes = 0;

#if DEBUG
	cout << "Receiving - " << message << endl;
#endif

	char *pointerData = (char*)data;

	int bytesRead = 0;
	int bytesLeft = size;
	while (bytesLeft != 0){
		if ((numbytes = recv(sockfd, &pointerData[bytesRead], bytesLeft, 0)) == -1){
			string errMsg = "Error at receiving : " + message;
			perror(errMsg.c_str());
			return (-1);
		}
		//			cout << "numbytes: " << numbytes << "; bytesRead: " << bytesRead << std::endl;
		bytesRead += numbytes;
		bytesLeft -= numbytes;
	}
#if DEBUG
	cout << "Received - " << message << endl;
#endif

	//printf("Data received is : %f", ((float*)data)[0]);
	return (numbytes);
}

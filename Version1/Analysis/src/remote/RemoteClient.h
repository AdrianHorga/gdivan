/*
 * RemoteClient.h
 *
 *      Author: adrianh
 */

#ifndef REMOTE_REMOTECLIENT_H_
#define REMOTE_REMOTECLIENT_H_

#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string>
#include <iostream>

using namespace std;

#define MAX_DATA_ALLOWED 8192

#define DEBUG 0

class RemoteClient{
public:
	RemoteClient(string addressName, string portNo): address(addressName), port(portNo){
		sockfd = -1;
	}
	int connectToServer();
	int disconnectFromServer(){ return (close(sockfd));}
	int sendData(void *data, int size, int noOfElements, string message) const;
	int receiveData(void *data, int size, int noOfElements, string message) const;
private:
	string address;
	string port;
	int sockfd;

};



#endif /* REMOTE_REMOTECLIENT_H_ */

/*
 * KernelInfo.h
 *
 *      Author: adrianh
 */

#ifndef KERNELFILES_KERNELINFO_H_
#define KERNELFILES_KERNELINFO_H_

#include "../genetic_algorithm/SenderPattern.h"
#include "../helpers/Helpers.h"

#include <string>
#include <map>


#include "GkleeData.h"

template <typename T>
class KernelInfo{
public:
	KernelInfo(std::string executableName, std::string sourceLocation,
					std::string executableLocation, KernelCode code, std::string outputFolder);
	std::string getKernelFileLocation() const;
	std::string getExecutableLocation() const;
	std::string getExecutableName() const;
	std::string getFormattedParameters() const;
	void setParameter(std::string parameter, std::string value);
	void addInputSize(std::string inputName, char size);
	char getInputSize(std::string inputName) const;
	void addGkleeData(GkleeData<T>* data);
	~KernelInfo();
	GkleeData<T>* getData() const;
	void printData() const;
	const int getKernelCode();
	CommunicationPattern<T>* getSenderPattern();
	void setSenderPattern(CommunicationPattern<T>* pattern);
	std::string getOutputFolder();


private:
	std::string executableName;
	std::string fileLocation;
	std::string executableLocation;
	std::string outputFolder = "";

	std::map<std::string, std::string> parametersToRunWith;
	std::map<std::string, char> inputsSize;

	GkleeData<T> *gkleeData = nullptr;

	KernelCode kernelCode;
	CommunicationPattern<T>* senderPattern = NULL;
};


#endif /* KERNELFILES_KERNELINFO_H_ */

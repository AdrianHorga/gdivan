/*
 * GkleeSteps.h
 *
 *      Author: adrianh
 */

#ifndef GKLEE_HELPERS_GKLEESTEPS_H_
#define GKLEE_HELPERS_GKLEESTEPS_H_

#include <string>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <map>

#include "KernelInfo.h"
#include "GkleeFileHandler.h"
#include "GkleeHandler.h"

using namespace std;

template <typename T>
void runGKLEE(KernelInfo<T> *kernelFile, unsigned expectedCoverage, unsigned bound,
		unsigned *threadsForExpectedCoverage, int initialThreads, bool execGKLEE){
	bool done = false;
	unsigned threadsToRun = initialThreads;
	GkleeFileHandler gkleeFilehandler;
	GkleeHandler	gkleeHandler;
	cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Start of GKLEE run>>>>>>>>>>>>>>>>>" << endl;

	*threadsForExpectedCoverage = initialThreads;
	while(!done && execGKLEE){
		system("pwd");
		gkleeFilehandler.writeSetupFile(bound);

		string command;

		//compile
		std::ostringstream oss;
		oss<< threadsToRun;
		kernelFile->setParameter("THREADS", oss.str());
		gkleeHandler.compileKernelWithGklee(kernelFile->getExecutableName(),
				kernelFile->getKernelFileLocation(), kernelFile->getFormattedParameters());


		//remove previous files so GKLEE can write the details when it appends the info
		gkleeHandler.removeFilesFromInputsFolder();


		//execute with branch coverage so that the actual coverage is computed
		gkleeHandler.runKernelWithGklee(kernelFile->getExecutableLocation(), kernelFile->getExecutableName()+".log");


		//read the details from the gklee execution and see if the actual coverage is the same as the expected coverage
		unsigned actualCoverage;
		actualCoverage = gkleeFilehandler.readCoverageFile();


		*threadsForExpectedCoverage = threadsToRun;
		//done = true;

		if (actualCoverage < expectedCoverage){
			threadsToRun *= 2;
		}else{
			done = true;
			*threadsForExpectedCoverage = threadsToRun;
		}
	}

	unsigned total = gkleeFilehandler.readTotalFile();
	gkleeFilehandler.readDetailFile<T>(total, kernelFile);
	cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>End of GKLEE run>>>>>>>>>>>>>>>>>" << endl;

}



#endif /* GKLEE_HELPERS_GKLEESTEPS_H_ */

/*
 * KernelInfo.cc
 *
 *      Author: adrianh
 */

#include "KernelInfo.h"

template <typename T>
KernelInfo<T>::KernelInfo(std::string executableName, std::string sourceLocation,
							std::string executableLocation, KernelCode code, std::string output):
								executableName(executableName), fileLocation(sourceLocation),
								executableLocation(executableLocation), kernelCode(code),
								outputFolder(output){
}

template <typename T>
std::string KernelInfo<T>::getKernelFileLocation() const{
	return (fileLocation);
}

template <typename T>
std::string KernelInfo<T>::getExecutableLocation() const{
	return (executableLocation);
}

template <typename T>
std::string KernelInfo<T>::getExecutableName() const{
	return (executableName);
}

template <typename T>
std::string KernelInfo<T>::getFormattedParameters() const{
	std::string format = "";
	for (auto const& ent : parametersToRunWith){
		std::string param = "-D" + ent.first + "=" + ent.second + " ";
		format += param;
	}
	return (format);
}

template <typename T>
void KernelInfo<T>::setParameter(std::string parameter, std::string value){
	parametersToRunWith[parameter] = value;
}

template <typename T>
void KernelInfo<T>::addInputSize(std::string inputName, char size){
	inputsSize[inputName] = size;
}

template <typename T>
char KernelInfo<T>::getInputSize(std::string inputName) const{
	return (inputsSize.at(inputName));
}

template <typename T>
void KernelInfo<T>::addGkleeData(GkleeData<T>* data){
	gkleeData = data;
	gkleeData->setupForRoulette();
}

template <typename T>
KernelInfo<T>::~KernelInfo(){
	delete gkleeData;
	delete senderPattern;
}

template <typename T>
GkleeData<T>* KernelInfo<T>::getData() const {
	return (gkleeData);
}

template <typename T>
void KernelInfo<T>::printData() const{
	gkleeData->printData();
}

template <typename T>
const int KernelInfo<T>::getKernelCode(){
	return kernelCode;
}

template <typename T>
CommunicationPattern<T>* KernelInfo<T>::getSenderPattern(){
	return senderPattern;
}

template <typename T>
void KernelInfo<T>::setSenderPattern(CommunicationPattern<T>* pattern){
	senderPattern = pattern;
}

template <typename T>
std::string KernelInfo<T>::getOutputFolder(){
	return outputFolder;
}

template class KernelInfo<int>;
template class KernelInfo<double>;
template class KernelInfo<float>;


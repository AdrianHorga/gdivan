/*
 * GKLEEFileHandler.cc
 *
 *      Author: adrianh
 */

#include "GkleeFileHandler.h"

#include "GkleeData.h"

void GkleeFileHandler::writeSetupFile(unsigned const bound) const{
	std::ofstream gkleeSetupFile;

	gkleeSetupFile.open(setupFilePath.c_str());
	if (gkleeSetupFile.is_open()){
		gkleeSetupFile << bound;
		gkleeSetupFile.close();
	}else{
		std::cerr << "Could not open file : " + setupFilePath;
	}
}

unsigned GkleeFileHandler::readCoverageFile() const{
	std::ifstream gkleeCoverageFile;
	unsigned cov = -1;

	gkleeCoverageFile.open(coverageFilePath.c_str());
	if (gkleeCoverageFile.is_open()){
		gkleeCoverageFile >> cov;
		gkleeCoverageFile.close();
	}else{
		std::cerr << "Could not open file : " + coverageFilePath;
	}
	return (cov);
}


unsigned GkleeFileHandler::readTotalFile(){
	std::ifstream gkleeTotalFile;
	unsigned total = -1;

	gkleeTotalFile.open(totalsFilePath.c_str());
	if (gkleeTotalFile.is_open()){
		gkleeTotalFile >> total;
		gkleeTotalFile.close();
	}else{
		std::cerr << "Could not open file : " + totalsFilePath;
	}
	return (total);
}

void GkleeFileHandler::setSetupFilePath(std::string path){
	setupFilePath = path;
}

void GkleeFileHandler::setDetailFilePath(std::string path){
	detailFilePath = path;
}

void GkleeFileHandler::setCoverageFilePath(std::string path){
	coverageFilePath = path;
}

void GkleeFileHandler::setTotalsFilePath(std::string path){
	totalsFilePath = path;
}

//template <> void GkleeFileHandler::readDetailFile<int>(unsigned total, KernelInfo<int> &kernelFile) const;
//template <> void GkleeFileHandler::readDetailFile<float>(unsigned total, KernelInfo<float> &kernelFile) const;
//template <> void GkleeFileHandler::readDetailFile<double>(unsigned total, KernelInfo<double> &kernelFile) const;


/*
 * GKLEEHandler.h
 *
 *      Author: adrianh
 */

#ifndef GKLEE_HELPERS_GKLEEHANDLER_H_
#define GKLEE_HELPERS_GKLEEHANDLER_H_

#include <string>
//#include <iostream>
//#include <fstream>
//#include <sstream>
//#include <stdlib.h>
//#include <map>
//#include <math.h>
//
//#include <bitset>
//#include "../gklee_helpers/KernelInfo.h"
//#include "GKLEEData.h"
//#include "GKLEEConstants.h"



class GkleeHandler{
public:
	void compileKernelWithGklee(std::string executablePath, std::string filePath, std::string extraOptions);
	void removeFilesFromPath(std::string path);
	void removeFilesFromInputsFolder();
	void runKernelWithGklee(std::string executablePath, std::string logPath);

private:
	const std::string compileCommand = "gklee-nvcc";
	const std::string runCommand = "gklee -bc-cov";
};





#endif /* GKLEE_HELPERS_GKLEEHANDLER_H_ */

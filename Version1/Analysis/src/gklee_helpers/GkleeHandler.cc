/*
 * GKLEEHandler.cc
 *
 *      Author: adrianh
 */

//#include <stdlib.h>

#include "GkleeHandler.h"

void GkleeHandler::compileKernelWithGklee(std::string executablePath,
		std::string filePath, std::string extraOptions){
	std::string command = compileCommand + " " + "-o " + executablePath
			+ " " + filePath + " " + extraOptions;
	system(command.c_str());
}

void GkleeHandler::removeFilesFromPath(std::string path){
	std::string command = "rm " + path;
	system(command.c_str());
}

void GkleeHandler::removeFilesFromInputsFolder(){
	std::string command = "rm -f inputs/*";
	system(command.c_str());
}

void GkleeHandler::runKernelWithGklee(std::string executablePath, std::string logPath){
	std::string command = runCommand + " " + executablePath + " >" + logPath;
	system(command.c_str());
}



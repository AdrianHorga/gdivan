/*
 * Controlled_tests.h
 *
 *      Author: adrianh
 */

#ifndef TEST_SCENARIOS_CONTROLLED_TESTS_H_
#define TEST_SCENARIOS_CONTROLLED_TESTS_H_


PopulationManager* gklee_ga_setup_controlled(unsigned &sizeOfChromosomes, std::string address, std::string port){

	int initialThreads;
	//unsigned sizeOfChromosomes; //= 16384;//65536;//16384;//65536;//8192;
	unsigned expectedCoverage;
	unsigned bound;
	CommunicationPattern<int> *pattern;
	std::string baseOutputFolder = "./gklee_ga/";


	std::string outputFolder = baseOutputFolder + "gklee_controlled/" + TimerHelper::getCurrentTime();
	KernelInfo<int> *kernelFile = new KernelInfo<int>("gklee_controlled", "./src/kernelFiles/gklee_controlled.cu",
			"./gklee_controlled", KernelCode::CONTROLLED, outputFolder);
	initialThreads = 2;
	sizeOfChromosomes = 16384;
	expectedCoverage = 100;
	bound = 1;
	pattern = new CommunicationPattern<int>(address, port);
	kernelFile->setSenderPattern(pattern);

	kernelFile->setParameter("BLOCKS", "1");
	kernelFile->setParameter("THREADS", "1");
	kernelFile->addInputSize("values", sizeof(int));
	kernelFile->addInputSize("graph_edges", sizeof(int));


	unsigned threadsForExpectedCoverage;

	//running Gklee to get base chromosomes
	bool execGKLEE = true;
	runGKLEE(kernelFile, expectedCoverage, bound, &threadsForExpectedCoverage, initialThreads, execGKLEE);
	TimerHelper::stopTimer();
	std::cout << "GKLEE finished after: " << TimerHelper::getElapsedSeconds() << " seconds"<< std::endl;
	//kernelFile.printData();

	PopulationManager* manager = new PopulationManagerGklee<int>(kernelFile);

	return manager;
}

PopulationManager* random_ga_setup_controlled(unsigned &sizeOfChromosomes, std::string address, std::string port){
	RunType runtype = RunType::RANDOM;

	std::cout << ">>>>>>>>>>>>>>>>>Start of RANDOM setup>>>>>>>>>>>>>>>>>" << std::endl << std::endl;

	Job *job;
	//	totalSize = 16384 * 4;
	//	job = new BitonicLargeJob(executionTime, targetedFitness, iterations, totalSize);

	unsigned totalSize = 16384 * 2;
	sizeOfChromosomes = totalSize;
	job = new ControlledJob("random_ga",0, 0, 1, totalSize);

	job->setupFolders();
	job->cleanFolders();
	job->createSamples(10);
	//job->runJob(step, address, port, runtype);


	PopulationManager *manager = new PopulationManagerRandom(job, address, port, runtype);

	std::cout <<  ">>>>>>>>>>>>>>>>>End of RANDOM setup>>>>>>>>>>>>>>>>>" << std::endl << std::endl;

	return manager;
}

void random_controlled(std::string address, std::string port){
	RunType runtype = RunType::RANDOM;

	double executionTime = 0;
	float targetedFitness = 0;
	int iterations = 2000;
	int totalSize;
	int step = 100;

	Job *job;

	totalSize = 16384 * 2;
	job = new ControlledJob("random",0, 0, iterations, totalSize);

	std::cout << "Size " << totalSize << "; Iterations " << iterations << "; Step " << step << std::endl << std::endl;

	job->setupFolders();
	job->cleanFolders();
	job->createSamples(10);
	job->runJob(step, address, port, runtype);
}

#endif /* TEST_SCENARIOS_CONTROLLED_TESTS_H_ */

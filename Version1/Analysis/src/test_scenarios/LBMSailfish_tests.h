/*
 * LBMSailfish_tests.h
 *
 *      Author: adrianh
 */

#ifndef TEST_SCENARIOS_LBM_SAILFISH_TESTS_H_
#define TEST_SCENARIOS_LBM_SAILFISH_TESTS_H_

#include "../helpers/TimerHelper.h"
#include "../genetic_algorithm/SenderPattern.h"

PopulationManager* gklee_ga_setup_lbmSailfish(unsigned &sizeOfChromosomes, std::string address, std::string port){

	int initialThreads;
	//unsigned sizeOfChromosomes; //= 16384;//65536;//16384;//65536;//8192;
	unsigned expectedCoverage;
	unsigned bound;
	CommunicationPattern<int> *pattern;
	std::string baseOutputFolder = "./gklee_ga/";


	std::string outputFolder = baseOutputFolder + "lbmSailfish/" + TimerHelper::getCurrentTime();
	KernelInfo<int> *kernelFile = new KernelInfo<int>("lbmSailfish", "./src/kernelFiles/lbmSailfish.cu",
			"./lbmSailfish", KernelCode::LBM_SAILFISH, outputFolder);
	initialThreads = 2;
	int width = 512 * initialThreads;
	int height = 512 * initialThreads;
	sizeOfChromosomes = width * height * initialThreads * initialThreads;
	expectedCoverage = 50;
	bound = 1;
	pattern = new CommunicationPatternLBM<int>(address, port, initialThreads, width, height);
	kernelFile->setSenderPattern(pattern);

	kernelFile->setParameter("BLOCKS", std::to_string(initialThreads));
	kernelFile->setParameter("THREADS", std::to_string(initialThreads));
	kernelFile->addInputSize("map", sizeof(int));


	unsigned threadsForExpectedCoverage;

	//running Gklee to get base chromosomes
	bool execGKLEE = true;
	runGKLEE(kernelFile, expectedCoverage, bound, &threadsForExpectedCoverage, initialThreads, execGKLEE);
	TimerHelper::stopTimer();
	std::cout << "GKLEE finished after: " << TimerHelper::getElapsedSeconds() << " seconds"<< std::endl;
	//kernelFile.printData();



	PopulationManager* manager = new PopulationManagerGklee<int>(kernelFile);

	return manager;
}

PopulationManager* random_ga_setup_lbmSailfish(unsigned &sizeOfChromosomes, std::string address, std::string port){
	RunType runtype = RunType::RANDOM;

	std::cout << ">>>>>>>>>>>>>>>>>Start of RANDOM setup>>>>>>>>>>>>>>>>>" << std::endl << std::endl;

	Job *job;

	int width = 512 * 2;
	int height = 512 * 2;
	unsigned totalSize = width * height;
	sizeOfChromosomes = totalSize;
	job = new LBMSailfishJob("random_ga",0, 0, 1, totalSize, width, height);

	job->setupFolders();
	job->cleanFolders();
	job->createSamples(10);
	//job->runJob(step, address, port, runtype);


	PopulationManager *manager = new PopulationManagerRandom(job, address, port, runtype);

	std::cout <<  ">>>>>>>>>>>>>>>>>End of RANDOM setup>>>>>>>>>>>>>>>>>" << std::endl << std::endl;

	return manager;
}

void random_lbmSailfish(std::string address, std::string port){
	RunType runtype = RunType::RANDOM;

	int iterations = 2000;
	int totalSize;
	int step = 100;

	Job *job;

	int width = 512 * 2;
	int height = 512 * 2;
	totalSize = width * height;
	job = new LBMSailfishJob("random",0, 0, iterations, totalSize, width, height);

	std::cout << "Size " << totalSize << "; Iterations " << iterations << "; Step " << step << std::endl << std::endl;

	job->setupFolders();
	job->cleanFolders();
	job->createSamples(10);
	job->runJob(step, address, port, runtype);
}



#endif /* TEST_SCENARIOS_LBMSAILFISH_TESTS_H_ */

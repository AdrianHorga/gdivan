//============================================================================
// Name        : Main.cpp
// Author      : Adrian Horga
// Version     :
// Copyright   : Your copyright notice
// Description : Main file for running the Genetic Algorithm
//============================================================================

#include <iostream>
#include <string>

#include "gklee_helpers/GkleeSteps.h"
//#include "genetic_algorithm/GA.h"
#include "genetic_algorithm/GASettings.h"
#include "gklee_helpers/KernelInfo.h"
#include "genetic_algorithm/Population.h"
#include "genetic_algorithm/PopulationManager.h"
#include "genetic_algorithm/PopulationManagerGklee.h"
#include "gklee_helpers/GkleeHandler.h"
#include "genetic_algorithm/SenderPattern.h"

#include "remote/RemoteClient.h"

#include "helpers/TimerHelper.h"

#include "helpers/Job.h"
#include "helpers/Jobs.h"
#include "genetic_algorithm/PopulationManagerRandom.h"

#include "test_scenarios/BFS_tests.h"
#include "test_scenarios/Controlled_tests.h"
#include "test_scenarios/NSICHNEU_tests.h"
#include "test_scenarios/LBMSailfish_tests.h"



using namespace std;

void run_test(std::string extension, const std::string& address, const std::string& port,
		PopulationManager* (*setup_function)(unsigned&, const std::string, const std::string)) {
	unsigned sizeOfChromosomes;
	//get data blocks from GKLEE and run the algorithm
	//read total.txt & details.txt to create the data blocks for GA
	GASettings* settings;
	settings = readSettings("settings.txt");
	PopulationManager* manager = setup_function(sizeOfChromosomes, address,port);
	Population* pop = new Population(manager, settings, extension);
	pop->runGeneticAlgorithm(sizeOfChromosomes);
	delete pop;
}

int main(int argc, char **argv) {
	std::string address = ""; //put the remote address
	std::string laddress = "localhost";
	std::string port = "54321";

	cout << "Starting time : " << TimerHelper::getCurrentTime() << endl;

	cout << ">>>>>>>>>>>>>>>>>Start of Algorithm>>>>>>>>>>>>>>>>>" << endl;

	TimerHelper::startTimer();

	//controlled artificial
		run_test(".gkleeGA", laddress, port, gklee_ga_setup_controlled);
	//      run_test(".randomGA", laddress, port, random_ga_setup_controlled);
	//	random_controlled(laddress, port);
	//
	//	//bfs
	//      run_test(".gkleeGA", laddress, port, gklee_ga_setup_bfs);
	//	run_test(".randomGA", address, port, random_ga_setup_bfs);
	//	random_bfs(address, port);
	//
	//	//nsichneu
	//	run_test(".gkleeGA", laddress, port, gklee_ga_setup_nsichneu);
	//	run_test(".randomGA", laddress, port, random_ga_setup_nsichneu);
	//	random_nsichneu(laddress, port);
	//
	//	//lbm sailfish
	//	run_test(".gkleeGA", laddress, port, gklee_ga_setup_lbmSailfish);
	//	run_test(".randomGA", laddress, port, random_ga_setup_lbmSailfish);
	//	random_lbmSailfish(laddress, port);


	TimerHelper::stopTimer();
	cout << "Execution elapsed time : " << TimerHelper::getElapsedSeconds() << " seconds " << endl;

	cout << ">>>>>>>>>>>>>>>>>End of Algorithm>>>>>>>>>>>>>>>>>" << endl;

	cout << "Finish time : " << TimerHelper::getCurrentTime() << endl;



	return 0;
}





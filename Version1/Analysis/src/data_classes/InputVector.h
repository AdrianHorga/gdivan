/*
 * InputVector.h
 *
 *      Author: adrianh
 */

#ifndef DATA_CLASSES_INPUTVECTOR_H_
#define DATA_CLASSES_INPUTVECTOR_H_

#include <string>

class InputVector{
public:
	virtual std::string getType() const = 0;
	virtual unsigned getSize() const = 0;
	virtual ~InputVector(){}

private:
};


#endif /* DATA_CLASSES_INPUTVECTOR_H_ */

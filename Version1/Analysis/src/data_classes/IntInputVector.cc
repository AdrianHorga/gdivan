/*
 * IntInputVector.cc
 *
 *      Author: adrianh
 */

#include "IntInputVector.h"

std::string IntInputVector::getType() const{
	return ("int");
}

unsigned IntInputVector::getSize() const{
	return (sizeof(int));
}

IntInputVector::~IntInputVector(){}




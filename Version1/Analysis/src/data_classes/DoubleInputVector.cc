/*
 * DoubleInputVector.cc
 *
 *      Author: adrianh
 */

#include "DoubleInputVector.h"

std::string DoubleInputVector::getType() const{
	return ("double");
}

unsigned DoubleInputVector::getSize() const{
	return (sizeof(double));
}

DoubleInputVector::~DoubleInputVector(){}



/*
 * FloatInputVector.h
 *
 *      Author: adrianh
 */

#ifndef DATA_CLASSES_FLOATINPUTVECTOR_H_
#define DATA_CLASSES_FLOATINPUTVECTOR_H_

#include "InputVector.h"

class FloatInputVector:public InputVector{
public:
	virtual std::string getType() const;
	unsigned getSize() const;
	~FloatInputVector();

private:

};


#endif /* DATA_CLASSES_FLOATINPUTVECTOR_H_ */

/*
 * BlockOfSolutions.h
 *
 *      Author: adrianh
 */

#ifndef DATA_CLASSES_BLOCKOFSOLUTIONS_H_
#define DATA_CLASSES_BLOCKOFSOLUTIONS_H_

#include <vector>

#include "PathSolution.h"

template <typename T>
class BlockOfSolutions{
public:
	BlockOfSolutions(unsigned path, float divergence, int instructions);
	void addPathSolution(PathSolution<T>* solution);
	~BlockOfSolutions();
	void printData();
	float getDivergence() const;
	int getExecutedInstructions() const;
	PathSolution<T>* pickARandomSolution();
	PathSolution<T>* pickSolution(int i);
	int getNumberOfSolutions();

private:
	std::vector<PathSolution<T>* > solutions;
	unsigned pathNumber;
	unsigned divergence; //0 - 100
	unsigned executedInstructions;
};



#endif /* DATA_CLASSES_BLOCKOFSOLUTIONS_H_ */

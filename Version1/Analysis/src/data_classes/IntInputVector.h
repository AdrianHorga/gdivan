/*
 * IntInputVector.h
 *
 *      Author: adrianh
 */

#ifndef DATA_CLASSES_INTINPUTVECTOR_H_
#define DATA_CLASSES_INTINPUTVECTOR_H_

#include "InputVector.h"

class IntInputVector:public InputVector{
public:
	virtual std::string getType() const;
	unsigned getSize() const;
	~IntInputVector();

private:

};



#endif /* DATA_CLASSES_INTINPUTVECTOR_H_ */

################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/data_classes/BlockOfSolutions.cc \
../src/data_classes/DoubleInputVector.cc \
../src/data_classes/FloatInputVector.cc \
../src/data_classes/IntInputVector.cc \
../src/data_classes/PathSolution.cc 

CC_DEPS += \
./src/data_classes/BlockOfSolutions.d \
./src/data_classes/DoubleInputVector.d \
./src/data_classes/FloatInputVector.d \
./src/data_classes/IntInputVector.d \
./src/data_classes/PathSolution.d 

OBJS += \
./src/data_classes/BlockOfSolutions.o \
./src/data_classes/DoubleInputVector.o \
./src/data_classes/FloatInputVector.o \
./src/data_classes/IntInputVector.o \
./src/data_classes/PathSolution.o 


# Each subdirectory must supply rules for building sources it contributes
src/data_classes/%.o: ../src/data_classes/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++1y -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../src/helpers/BFSJob.cc \
../src/helpers/ControlledJob.cc \
../src/helpers/Job.cc \
../src/helpers/LBMSailfishJob.cc \
../src/helpers/NSICHNEUJob.cc \
../src/helpers/TimerHelper.cc 

CC_DEPS += \
./src/helpers/BFSJob.d \
./src/helpers/ControlledJob.d \
./src/helpers/Job.d \
./src/helpers/LBMSailfishJob.d \
./src/helpers/NSICHNEUJob.d \
./src/helpers/TimerHelper.d 

OBJS += \
./src/helpers/BFSJob.o \
./src/helpers/ControlledJob.o \
./src/helpers/Job.o \
./src/helpers/LBMSailfishJob.o \
./src/helpers/NSICHNEUJob.o \
./src/helpers/TimerHelper.o 


# Each subdirectory must supply rules for building sources it contributes
src/helpers/%.o: ../src/helpers/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++1y -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>

#include <stdio.h>

#define GEO_FLUID 0
#define GEO_WALL 1
#define GEO_INFLOW 2

struct Dist {
	float *fC, *fE, *fW, *fS, *fN, *fSE, *fSW, *fNE, *fNW;
};

struct SimState {
	int *map, *dmap;

	// macroscopic quantities on the video card
	float *dvx, *dvy, *drho;

	// macroscopic quantities in RAM
	float *vx, *vy, *rho;

	float *lat[9];
	Dist d1, d2;
};

#define CUDA_SAFE_CALL_NO_SYNC( call) {                                    \
		cudaError err = call;                                                    \
		if( cudaSuccess != err) {                                                \
			fprintf(stderr, "Cuda error in file '%s' in line %i : %s.\n",        \
					__FILE__, __LINE__, cudaGetErrorString( err) );              \
					exit(EXIT_FAILURE);                                                  \
		} }

#define CUDA_SAFE_CALL( call)     CUDA_SAFE_CALL_NO_SYNC(call);                                            \




void output(int snum, float *vx, float *vy, float *rho, int LAT_W, int LAT_H)
{
	int x, y, i;
	char name[128];
	FILE *fp;

	sprintf(name, "out%05d.dat", snum);
	fp = fopen(name, "w");

	i = 0;
	sprintf(name, "t%d", snum);
	for (y = 0; y < LAT_H; y++) {
		for (x = 0; x < LAT_W; x++) {
			fprintf(fp, "%d %d %f %f %f\n", x, y, rho[i], vx[i], vy[i]);
			i++;
		}
		fprintf(fp, "\n");
	}
	fclose(fp);
}

const float visc = 0.01f;				// viscosity
__constant__ float tau;					// relaxation time
__constant__ int latH;			// lattice height

__global__ void LBMCollideAndPropagate_reworked(int *map, Dist cd, Dist od, float *orho, float *ovx, float *ovy, int LAT_W, int LAT_H)
{
	int tix = blockIdx.x * blockDim.x + threadIdx.x;
	int tiy = blockIdx.x * blockDim.x + threadIdx.y;
	int gi = tiy * blockDim.x * gridDim.x + tix; 

	// equilibrium distributions
	float feq_C, feq_N, feq_S, feq_E, feq_W, feq_NE, feq_NW, feq_SE, feq_SW;
	float fi_N, fi_S, fi_E, fi_W, fi_C, fi_NE, fi_NW, fi_SE, fi_SW;
	float rho;
	float2 v;

	// cache the distribution in local variables
	fi_C = cd.fC[gi];
	fi_E = cd.fE[gi];
	fi_W = cd.fW[gi];
	fi_S = cd.fS[gi];
	fi_N = cd.fN[gi];
	fi_NE = cd.fNE[gi];
	fi_NW = cd.fNW[gi];
	fi_SE = cd.fSE[gi];
	fi_SW = cd.fSW[gi];

	// macroscopic quantities for the current cell
	rho = fi_C + fi_E + fi_W + fi_S + fi_N + fi_NE + fi_NW + fi_SE + fi_SW;
	if (map[gi] == GEO_INFLOW) {
		v.x = 0.1f;
		v.y = 0.0f;
	} else {
		v.x = (fi_E + fi_SE + fi_NE - fi_W - fi_SW - fi_NW) / rho;
		v.y = (fi_N + fi_NW + fi_NE - fi_S - fi_SW - fi_SE) / rho;
	}

	if (orho != NULL) {
		orho[gi] = rho;
		ovx[gi] = v.x;
		ovy[gi] = v.y;
	}

	// relaxation
	float Cusq = -1.5f * (v.x*v.x + v.y*v.y);

	feq_C = rho * (1.0f + Cusq) * 4.0f/9.0f;
	feq_N = rho * (1.0f + Cusq + 3.0f*v.y + 4.5f*v.y*v.y) / 9.0f;
	feq_E = rho * (1.0f + Cusq + 3.0f*v.x + 4.5f*v.x*v.x) / 9.0f;
	feq_S = rho * (1.0f + Cusq - 3.0f*v.y + 4.5f*v.y*v.y) / 9.0f;
	feq_W = rho * (1.0f + Cusq - 3.0f*v.x + 4.5f*v.x*v.x) / 9.0f;
	feq_NE = rho * (1.0f + Cusq + 3.0f*(v.x+v.y) + 4.5f*(v.x+v.y)*(v.x+v.y)) / 36.0f;
	feq_SE = rho * (1.0f + Cusq + 3.0f*(v.x-v.y) + 4.5f*(v.x-v.y)*(v.x-v.y)) / 36.0f;
	feq_SW = rho * (1.0f + Cusq + 3.0f*(-v.x-v.y) + 4.5f*(v.x+v.y)*(v.x+v.y)) / 36.0f;
	feq_NW = rho * (1.0f + Cusq + 3.0f*(-v.x+v.y) + 4.5f*(-v.x+v.y)*(-v.x+v.y)) / 36.0f;

	if (map[gi] == GEO_FLUID) {
		fi_C += (feq_C - fi_C) / tau;
		fi_E += (feq_E - fi_E) / tau;
		fi_W += (feq_W - fi_W) / tau;
		fi_S += (feq_S - fi_S) / tau;
		fi_N += (feq_N - fi_N) / tau;
		fi_SE += (feq_SE - fi_SE) / tau;
		fi_NE += (feq_NE - fi_NE) / tau;
		fi_SW += (feq_SW - fi_SW) / tau;
		fi_NW += (feq_NW - fi_NW) / tau;
	} else if (map[gi] == GEO_INFLOW) {
		fi_C  = feq_C;
		fi_E  = feq_E;
		fi_W  = feq_W;
		fi_S  = feq_S;
		fi_N  = feq_N;
		fi_SE = feq_SE;
		fi_NE = feq_NE;
		fi_SW = feq_SW;
		fi_NW = feq_NW;
	} else if (map[gi] == GEO_WALL) {
		float t;
		t = fi_E;
		fi_E = fi_W;
		fi_W = t;

		t = fi_NW;
		fi_NW = fi_SE;
		fi_SE = t;

		t = fi_NE;
		fi_NE = fi_SW;
		fi_SW = t;

		t = fi_N;
		fi_N = fi_S;
		fi_S = t;
	}

	od.fC[gi] = fi_C;

	// N + S propagation (global memory)
	if (tiy > 0)			od.fS[gi - LAT_W] = fi_S;
	if (tiy < LAT_H - 1)	        od.fN[gi + LAT_W] = fi_N;


	// E propagation in global memory (at block boundary)
	if (tix < LAT_W - 1) {
		od.fE[gi + 1] = fi_E;
		if (tiy > 0)             od.fSE[gi - LAT_W + 1] = fi_SE;
		if (tiy < LAT_H - 1)     od.fNE[gi + LAT_W + 1] = fi_NE;
	}


	// W propagation in global memory (at block boundary)
	if (tix > 0 ) { 
		od.fW[gi - 1] = fi_W;
		if (tiy > 0)		od.fSW[gi - LAT_W - 1] = fi_SW;
		if (tiy < LAT_H-1)      od.fNW[gi + LAT_W - 1] = fi_NW;
	}
}




void SimInit(struct SimState *state, int LAT_W, int LAT_H, const int size_i, const int size_f)
{
	int i;
	// setup relaxation time
	float tmp = (6.0f*visc + 1.0f)/2.0f;
	cudaMemcpyToSymbol(tau, &tmp, sizeof(float));
	cudaMemcpyToSymbol(latH, &LAT_H, sizeof(int));

	state->map = (int*)calloc(LAT_W*LAT_H, sizeof(int));
	int total = LAT_H * LAT_W;
	klee_make_symbolic(state->map, sizeof(int) * total, "map");
	for (int i = 0 ; i < total; i++){
		klee_assume(state->map[i] >= 0);
		klee_assume(state->map[i] < 3);
	}

	cudaMalloc((void**)&state->dmap, size_i);

	cudaMalloc((void**)&state->dvx, size_f);
	cudaMalloc((void**)&state->dvy, size_f);
	cudaMalloc((void**)&state->drho, size_f);

	state->vx = (float*)malloc(size_f);
	state->vy = (float*)malloc(size_f);
	state->rho = (float*)malloc(size_f);

	cudaMalloc((void**)&state->d1.fC, size_f);
	cudaMalloc((void**)&state->d1.fE, size_f);
	cudaMalloc((void**)&state->d1.fW, size_f);
	cudaMalloc((void**)&state->d1.fN, size_f);
	cudaMalloc((void**)&state->d1.fS, size_f);
	cudaMalloc((void**)&state->d1.fNE, size_f);
	cudaMalloc((void**)&state->d1.fSE, size_f);
	cudaMalloc((void**)&state->d1.fNW, size_f);
	cudaMalloc((void**)&state->d1.fSW, size_f);

	cudaMalloc((void**)&state->d2.fC, size_f);
	cudaMalloc((void**)&state->d2.fE, size_f);
	cudaMalloc((void**)&state->d2.fW, size_f);
	cudaMalloc((void**)&state->d2.fN, size_f);
	cudaMalloc((void**)&state->d2.fS, size_f);
	cudaMalloc((void**)&state->d2.fNE, size_f);
	cudaMalloc((void**)&state->d2.fSE, size_f);
	cudaMalloc((void**)&state->d2.fNW, size_f);
	cudaMalloc((void**)&state->d2.fSW, size_f);

	for (i = 0; i < 9; i++) {
		state->lat[i] = (float*)malloc(size_f);
	}

	for (i = 0; i < LAT_W*LAT_H; i++) {
		state->lat[0][i] = 4.0/9.0;
		state->lat[1][i] = state->lat[2][i] = state->lat[3][i] = state->lat[4][i] = 1.0/9.0;
		state->lat[5][i] = state->lat[6][i] = state->lat[7][i] = state->lat[8][i] = 1.0/36.0;
	}

	cudaMemcpy(state->dmap, state->map, size_i, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d1.fC, state->lat[0], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d1.fN, state->lat[1], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d1.fS, state->lat[2], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d1.fE, state->lat[3], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d1.fW, state->lat[4], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d1.fNE, state->lat[5], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d1.fNW, state->lat[6], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d1.fSE, state->lat[7], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d1.fSW, state->lat[8], size_f, cudaMemcpyHostToDevice);

	cudaMemcpy(state->d2.fC, state->lat[0], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d2.fN, state->lat[1], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d2.fS, state->lat[2], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d2.fE, state->lat[3], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d2.fW, state->lat[4], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d2.fNE, state->lat[5], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d2.fNW, state->lat[6], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d2.fSE, state->lat[7], size_f, cudaMemcpyHostToDevice);
	cudaMemcpy(state->d2.fSW, state->lat[8], size_f, cudaMemcpyHostToDevice);
}

void SimCleanup(struct SimState *state)
{
	int i;

	free(state->map);
	cudaFree(state->dmap);
	for (i = 0; i < 8; i++) {
		free(state->lat[i]);
	}

	cudaFree(state->dvx);
	cudaFree(state->dvy);
	cudaFree(state->drho);

	cudaFree(state->d1.fC);
	cudaFree(state->d1.fE);
	cudaFree(state->d1.fW);
	cudaFree(state->d1.fS);
	cudaFree(state->d1.fN);
	cudaFree(state->d1.fNE);
	cudaFree(state->d1.fNW);
	cudaFree(state->d1.fSE);
	cudaFree(state->d1.fSW);

	cudaFree(state->d2.fC);
	cudaFree(state->d2.fE);
	cudaFree(state->d2.fW);
	cudaFree(state->d2.fS);
	cudaFree(state->d2.fN);
	cudaFree(state->d2.fNE);
	cudaFree(state->d2.fNW);
	cudaFree(state->d2.fSE);
	cudaFree(state->d2.fSW);
}

void SimUpdate(int iter, struct SimState state, int LAT_W, int LAT_H, int BLOCK_SIZE, int size_f)
{
	dim3 grid;
	dim3 block(BLOCK_SIZE, BLOCK_SIZE, 1);
	grid.x = LAT_W/BLOCK_SIZE;
	grid.y = LAT_H/BLOCK_SIZE;

	/* printf("grid.x=%d\n", grid.x);
        printf("grid.y=%d\n", grid.y);
        int bl = BLOCK_SIZE;
        printf("BLOCK_SIZE=%d\n", bl);*/


	// A-B access pattern with swapped distributions d1, d2
	if (iter % 2 == 0) {
		LBMCollideAndPropagate_reworked<<<grid, block>>>(state.dmap, state.d1, state.d2, state.drho, state.dvx, state.dvy, LAT_W, LAT_H);
		cudaError_t error = cudaGetLastError();
		CUDA_SAFE_CALL(error);
	} else {
		LBMCollideAndPropagate_reworked<<<grid, block>>>(state.dmap, state.d2, state.d1, state.drho, state.dvx, state.dvy, LAT_W, LAT_H);
		cudaError_t error = cudaGetLastError();
		CUDA_SAFE_CALL(error);
	}


}

void SimUpdateMap(struct SimState state, const int size_i)
{
	cudaMemcpy(state.dmap, state.map, size_i, cudaMemcpyHostToDevice);
}

int main(int argc, char **argv)
{
	int LAT_H, LAT_W, BLOCK_SIZE;
	BLOCK_SIZE = THREADS;
	LAT_W = BLOCKS;
	LAT_H = BLOCKS;

	const int size_i = LAT_W*LAT_H*sizeof(int);
	const int size_f = LAT_W*LAT_H*sizeof(float);

	struct SimState state;


	SimInit(&state, LAT_W, LAT_H, size_i, size_f);

	int iter = 0;

	bool quit = false;

	while (!quit) {
		SimUpdate(iter, state, LAT_W, LAT_H, BLOCK_SIZE, size_f);
		SimUpdateMap(state, size_i);
		iter++;
		if (iter == 1){
			quit = true;
		}
	}

	SimCleanup(&state);
	return 0;
}

**GDivAn**

**GDivAn** is an measurement based execution time analysis tool for GPU kernels (CUDA programs) that uses symbolic execution(SE) and genetic algorithm(GA) to navigate the possible input space and direct the search toward the worst case execution time(WCET).

The provided code is split into 3 parts:

1. GKLEE
2. Analysis
3. RemoteServer

---

## 1. GKLEE - modified version
[GKLEE](http://formalverification.cs.utah.edu/GKLEE/) is a symbolic analyser and test generator tailored for CUDA C++ programs.

We have modified the version from [https://github.com/Geof23/Gklee](https://github.com/Geof23/Gklee) in order to get extra information from the SE phase.

Follow the instructions from the *GKLEE* folder in order to install it.

---

## 2. Analysis - GA phase

The *Analysis* folder contains the code for using the SE+GA, random+GA and random scenarios for the kernels.

The code has been developed using an IDE (Eclipse IDE for C/C++ Developers - Version: Neon.3 Release (4.6.3)).

Therefore the code can be loaded as an Eclipse project and can be run from there accordingly.
The code can also be run from a terminal window. The Makefile and the executable files are found in the *Release* folder.

The execution of the kernels on real hardware is done by the **RemoteServer**.

---

## 3. RemoteServer - executing on remote GPU

The *RemoteServer* folder contains the code for running a server that listens to requests from **Analysis** to run the coresponding kernels on the actual hardware (GPU).

The code has been developed using an IDE (Nsight Eclipse Edition Version: 8.0).

Therefore the code can be loaded as an Nsight project and can be run from there accordingly.
The code can also be run from a terminal window. The Makefile and the executable files are found in the *Release* folder.

The *RemoteServer* folder can be deployed on the local machine or on another machine that has a CUDA compatible GPU.

---

## Usage of GDivAn

GDivAn currently provides 3 types scenarios (GKLEE+GA, random+GA, random) for analyzing 4 kernels: Artficial, BFS, LBM and NSICHNEU.

The different scenarios can be selected from *Main.cpp* in the *Analysis/src* folder.
Make sure to recompile before running with the wanted scenario(s).

**Running GDivAn**

In order to run, follow the steps:

1. Install the provided **GKLEE** according to the instructions
2. Run **RemoteServer**
3. Run **Analysis**

**Results**

The results are stored in folders for each scenario type (GKLEE+GA, random+GA or random) and inside of these folders, a folder for each kernel name is created.
Each time a scenario for a kernel is executed the folders are created (if they don't exist), and a folder with the starting time is created for that particular run.
The result files are stored inside that run folder.

**Settings files**

1. gklee-setup.txt : should contain a single value representing how many different solutions should GKLEE provide for each different path detected. Gklee will generate as many as possible until the set value for each path.
2. settings.txt : should contain the values for the GA parameters (population size, iterations, etc.)


---

## Observations

The code has been developed and tested on a Linux distribution (Ubuntu 16.04) using gcc 5.4 and CUDA 8.0.
The **RemoteServer** has been executed on a NVIDIA Tegra K1 (Jetson TK1) using CUDA 6.5. 
The frequencies of the GPU were fixed to 72 MHz (GPU core clock) and 204 MHz (GPU memory clock).

---

### Bugs & issues

For reporting bugs or any other issues please contact [Adrian Horga](mailto:adrian.horga@liu.se).
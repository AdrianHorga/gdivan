/*
 * Server.cc
 *
 *      Author: adrianh
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <iostream>

#include "RemoteServer.h"

void sigchld_handler(int s){
	// waitpid() might overwrite errno, so we save and restore it:
	int saved_errno = errno;

	while(waitpid(-1, NULL, WNOHANG) > 0);

	errno = saved_errno;
}


// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa){
	if (sa->sa_family == AF_INET) {
		return (&(((struct sockaddr_in*)sa)->sin_addr));
	}

	return (&(((struct sockaddr_in6*)sa)->sin6_addr));
}

RemoteServer::RemoteServer(std::string port, int connectionLimit):
																																																											port(port), connectionLimit(connectionLimit), sockfd(-1){
}

int RemoteServer::setup(){

	struct addrinfo hints, *servinfo, *p;
	struct sigaction sa;
	int rv;
	int yes = 1;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	//hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

	if ((rv = getaddrinfo(NULL, port.c_str(), &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		exit(1);
	}

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("server: socket");
			continue;
		}

		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
				sizeof(int)) == -1) {
			perror("setsockopt");
			exit(1);
		}

		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("server: bind");
			continue;
		}

		break;
	}

	freeaddrinfo(servinfo); // all done with this structure

	if (p == NULL)  {
		fprintf(stderr, "server: failed to bind\n");
		exit(1);
	}

	if (listen(sockfd, connectionLimit) == -1) {
		perror("listen");
		exit(1);
	}

	sa.sa_handler = sigchld_handler; // reap all dead processes
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if (sigaction(SIGCHLD, &sa, NULL) == -1) {
		perror("sigaction");
		exit(1);
	}

	return (sockfd);
}

int RemoteServer::acceptNewConnection() const{
	return (acceptNewConnection(sockfd));
}

int RemoteServer::acceptNewConnection(int socketFileDescriptor) const{

	socklen_t sin_size;
	struct sockaddr_storage their_addr; // connector's address information
	char s[INET6_ADDRSTRLEN];
	int new_fd;

	sin_size = sizeof their_addr;
	new_fd = accept(socketFileDescriptor, (struct sockaddr *)&their_addr, &sin_size);
	if (new_fd == -1) {
		perror("accept");
		return (-1);
	}

	inet_ntop(their_addr.ss_family,
			get_in_addr((struct sockaddr *)&their_addr),
			s, sizeof s);
	printf("server: got connection from %s\n", s);

	return (new_fd);
}

RemoteRequest::RemoteRequest(int socket):sockfd(socket){}

int RemoteRequest::sendData(void *data, int size, int noOfElements, string message) const{
	int numbytes = 0;

#if DEBUG_SERVER
	cout << "Sending - " << message << endl;
#endif

	char *pointerData = (char*)data;

	int bytesSent = 0;
	int bytesLeft = size;
	while (bytesLeft != 0){
		if ((numbytes = send(sockfd, &pointerData[bytesSent], bytesLeft, 0)) == -1){
			string errMsg = "Error at sending : " + message;
			perror(errMsg.c_str());
			return (-1);
		}
		//			cout << "numbytes: " << numbytes << "; bytesSent: " << bytesSent << std::endl;
		bytesSent += numbytes;
		bytesLeft -= numbytes;
	}


#if DEBUG_SERVER
	cout << "Sent - " << message << endl;
#endif

	return (1);
}

int RemoteRequest::receiveData(void *data, int size, int noOfElements, string message) const{
	int numbytes = 0;

#if DEBUG_SERVER
	cout << "Receiving - " << message << "; size=" << size << "; elements=" << noOfElements<< endl;
#endif

	char *pointerData = (char*)data;

	int bytesRead = 0;
	int bytesLeft = size;
	while (bytesLeft != 0){
		if ((numbytes = recv(sockfd, &pointerData[bytesRead], bytesLeft, 0)) == -1){
			string errMsg = "Error at receiving : " + message;
			perror(errMsg.c_str());
			return (-1);
		}
		//			cout << "numbytes: " << numbytes << "; bytesRead: " << bytesRead << std::endl;
		bytesRead += numbytes;
		bytesLeft -= numbytes;
	}

#if DEBUG_SERVER
	cout << "Received - " << message << "; bytesRead=" << bytesRead << endl;
#endif

	//printf("Data received is : %f", ((float*)data)[0]);
	return (numbytes);
}




//int RemoteRequest::sendData(void* data, int dataSize, std::string message) const{
//	int numbytes;
//#if DEBUG_SERVER
//	std::cout << "Sending - " << message << std::endl;
//#endif
//
//	if ((numbytes = send(sockfd, data, dataSize, 0)) == -1)
//		std::cerr << "Not able to send - " << message << std::endl;
//
//#if DEBUG_SERVER
//	std::cout << "Sent - " << message << std::endl;
//#endif
//
//	return (numbytes);
//}
//
//
//int RemoteRequest::receiveData(void* data, int dataSize, std::string message) const{
//	int numbytes;
//
//#if DEBUG_SERVER
//	std::cout << "Receiving - " << message << std::endl;
//#endif
//
//	if ((numbytes = recv(sockfd, data, dataSize, 0)) == -1)
//		std::cerr << "Not able to send - " << message << std::endl;
//
//#if DEBUG_SERVER
//	std::cout << "Received - " << message << std::endl;
//#endif
//
//	return (numbytes);
//}

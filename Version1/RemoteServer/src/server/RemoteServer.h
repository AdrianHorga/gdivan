/*
 * Server.h
 *
 *      Author: adrianh
 */

#ifndef SERVER_REMOTESERVER_H_
#define SERVER_REMOTESERVER_H_

#include <string>

#define MAX_DATA_ALLOWED 8192

#define DEBUG_SERVER 0

using namespace std;

class RemoteServer{
public:
	RemoteServer(std::string port, int connectionLimit);
	int setup();
	int acceptNewConnection(int socketFileDescriptor) const;
	int acceptNewConnection() const;
private:
	std::string port;
	int connectionLimit;
	int sockfd;
};

class RemoteRequest{
public:
	RemoteRequest(int socket);
	int sendData(void *data, int size, int noOfElements, string message) const;
	int receiveData(void *data, int size, int noOfElements, string message) const;
private:
	int sockfd;
};



#endif /* SERVER_REMOTESERVER_H_ */

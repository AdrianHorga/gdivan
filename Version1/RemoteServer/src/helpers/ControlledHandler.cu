#include <iostream>
#include "Handler.h"
#include "../server/RemoteServer.h"
#include "../kernels/controlled/controlled.h"


void ControlledHandler::handleKernel(RemoteRequest *request){
	//get input
	int inputs;
	int datasize;
	int *data;

	request->receiveData(&inputs, sizeof(int), 1, "Input");

#if DEBUG_HANDLER
	std::cout << "inputs = " << inputs << std::endl;
#endif

	for (int i = 0; i < inputs; i++){
		//get the data size
		request->receiveData(&datasize, sizeof(int), 1, "Data size");

#if DEBUG_HANDLER
		std::cout << "datasize = " << datasize << std::endl;
#endif

		data = (int*)malloc(datasize * sizeof(int));

		request->receiveData(data, datasize * sizeof(int), datasize, "Array for input");
	}

#if DEBUG_HANDLER

	std::cout << "Before controlled" << std::endl;
	for (int i = 0; i < datasize; i++) {
		std::cout << data[i] << "\n";
	}
	std::cout << std::endl;
#endif

	int blocks, threads;
	threads = 256;
	blocks = (datasize - 1)/threads + 1;

	float time = 0;
	for (int i = 0 ; i < 10; i++){
		time += runControlled(data, blocks, threads);
	}
	time = time / 10;


	free(data);

#if DEBUG_HANDLER
	std::cout << "Time : " << time << std::endl;
#endif

	request->sendData(&time, sizeof(float), 1, "Execution time");
}

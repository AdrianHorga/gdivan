#include <iostream>
#include <math.h>
#include "Handler.h"
#include "../server/RemoteServer.h"
#include "../kernels/lbm-sailfish/lbm.h"

//BitonicHandler::BitonicHandler() : Handler(){}

void LBMSailfishHandler::handleKernel(RemoteRequest *request){
	//get input
	int inputs;
	int datasize;
	int *data;

	request->receiveData(&inputs, sizeof(int), 1, "Input");

#if DEBUG_HANDLER
	std::cout << "inputs = " << inputs << std::endl;
#endif

	int LAT_W, LAT_H;

	for (int i = 0; i < inputs; i++){
		//get the data size
		request->receiveData(&datasize, sizeof(int), 1, "Data size");

		request->receiveData(&LAT_W, sizeof(int), 1, "LAT_W");
		request->receiveData(&LAT_H, sizeof(int), 1, "LAT_H");
		std::cout << "LAT_W = " << LAT_W << std::endl;
		std::cout << "LAT_H = " << LAT_H << std::endl;

#if DEBUG_HANDLER
		std::cout << "datasize = " << datasize << std::endl;
#endif

		data = (int*)malloc(datasize * sizeof(int));

		request->receiveData(data, datasize * sizeof(int), datasize, "Array for input");
	}

#if DEBUG_HANDLER

	std::cout << "Before controlled" << std::endl;
	for (int i = 0; i < datasize; i++) {
		std::cout << data[i] << "\n";
	}
	std::cout << std::endl;
#endif


	//	int rad = sqrt(datasize);
	//	LAT_W = rad;
	//	LAT_H = rad;

	int threads, blocks;
	if (LAT_H < 8){
		threads = LAT_H;
	}else{
		threads = 8;
	}

	blocks = LAT_W;

	float time = 0;
	time = runLBM(data, threads, blocks);



	free(data);

#if DEBUG_HANDLER
	std::cout << "Time : " << time << std::endl;
#endif

	request->sendData(&time, sizeof(float), 1, "Execution time");
}

#include <iostream>
#include <stdlib.h>
#include <unistd.h>

#include "server/RemoteServer.h"
#include "helpers/Handler.h"
#include "helpers/HandlerFactory.h"

#define PORT "54321"  // the port users will be connecting to

#define BACKLOG 10	 // how many pending connections queue will hold

int main(int argc, char **argv) {
	int sockfd, new_fd;  // listen on sock_fd, new connection on new_fd

	std::string port = PORT;
	RemoteServer server(port, BACKLOG);
	sockfd = server.setup();

	std::cout << "Server: waiting for connections..." << std::endl;


	while (1) {  // main accept() loop

		new_fd = server.acceptNewConnection(sockfd);

		if (!fork()) { // this is the child process
			close(sockfd); // child doesn't need the listener
			std::cout << "--------------------------------------start" << std::endl;
			RemoteRequest request(new_fd);

			int type;
			request.receiveData(&type, sizeof(int), 1, "Kernel code");
			std::cout << "type = " << type << std::endl;

			Handler* handler;
			handler = HandlerFactory::getNewHandler(type);
			handler->handleKernel(&request);

			close(new_fd);
			std::cout << "--------------------------------------end" << std::endl << std::endl;
			exit(0);
		}
		close(new_fd);  // parent doesn't need this
	}

	return (0);
}




#include <stdio.h>
#include <stdlib.h>
#include "../../helpers/Helpers.h"

__global__ static void controlled(int* values, int* result)
{

	const int tid = blockIdx.x * blockDim.x + threadIdx.x;

	if (values[tid] == 6){
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
		atomicAdd(&result[blockIdx.x], values[tid]);
	}else{
		atomicAdd(&result[tid], 10);;
	}
}



float runControlled(const int *values,const int blocks,const int threads)
{
	//    int blocks = 512;
	//    int threads = 512;
	int total = threads * blocks;
	int *result;

	result = (int*)malloc(sizeof(int) * total);
	int * dvalues, *dresult;
	cudaMalloc((void**)&dvalues, sizeof(int) * total);
	cudaMalloc((void**)&dresult, sizeof(int) * total);
	cudaMemcpy(dvalues, values, sizeof(int) * total, cudaMemcpyHostToDevice);



	MeasureCUDA measure;
	measure.startMeasuring();

	controlled<<<blocks, threads>>>(dvalues, dresult);

	measure.stopMeasuring();
	float  time;
	time = measure.getTime();
	printf("Total Time = %f ms\n\n", time);


	cudaMemcpy(result, dresult, sizeof(int) * total, cudaMemcpyDeviceToHost);

	cudaFree(dvalues);
	cudaFree(dresult);

	free(result);

	return time;

}

#ifndef ADRIANSTATISTICS_H
#define ADRIANSTATISTICS_H

#include <vector>
#include <map>
#include <set>
#include <string>
#include <iostream>
#include <stddef.h>

#include "llvm/Instruction.h"
#include "Searcher.h"


using namespace std;

//adrianh added static variable for counting paths
static int pathsInTotal = 0;
//adrianh added a singleton class for keeping track of statistics over multiple path iterations

class StatisticsForGA {
public:
	static StatisticsForGA* getInstance();
	~StatisticsForGA(){
		instanceFlag = false;
	}
	int pathsInTotal = 0;
	vector<int> divergenceValuePerPath;

	vector<float> divergenceSumPerPath;
	vector<int> barrierIntervals;

	void addStaticBranch(std::string kernelName, llvm::Instruction *instr){
		//		perKernelStaticBranches[kernelName].insert(instr);
		staticBranches.insert(instr);
	}

	void addTrueBranch(int kernelNum, llvm::Instruction *instr){
		//		perKernelTrueBranches[kernelNum].insert(instr);
		trueBranches.insert(instr);
	}

	void addFalseBranch(int kernelNum, llvm::Instruction *instr){
		//		perKernelFalseBranches[kernelNum].insert(instr);
		falseBranches.insert(instr);
	}

	void addInstruction(std::string kernelName, llvm::Instruction *instr){
		kernelInstructions[kernelName].push_back(instr);
	}

	void addKernelName(int kernelNum, std::string name){
		kernelName[kernelNum] = name;
	}

	//	int getKernelStaticBranches(std::string kernelName){
	//		return perKernelStaticBranches[kernelName].size();
	//	}

	int getStaticBranches(){
		return staticBranches.size();
	}

	//	int getKernelTrueBranches(std::string kernelName){
	//		return perKernelStaticBranches[kernelName].size();
	//	}

	int getTrueBranches(){
		return trueBranches.size();
	}

	//	int getKernelFalseBranches(std::string kernelName){
	//		return perKernelStaticBranches[kernelName].size();
	//	}

	int getFalseBranches(){
		return falseBranches.size();
	}

	bool wasKernelCalledBefore(std::string kernelName){
		return (kernelInstructions.count(kernelName));
	}

	//	//we don't need threadID for now, just the total number of instructions
	//	void increaseInstructionCountForPath(int path, int howmuch, int threadID){
	//		auto it = perPathExecutedInstructions.find(path);
	//		if (it == perPathExecutedInstructions.end()){
	//			perPathExecutedInstructions[path] = howmuch;
	//		}else{
	//			perPathExecutedInstructions[path] += howmuch;
	//		}
	//	}
	//
	//	//we don't need threadID for now, just the total number of instructions per path
	//	int getExecutedInstructionsForPath(int path, int threadID){
	//		return perPathExecutedInstructions[path];
	//	}
	//
	//	void setSearcher(klee::Searcher *s){
	//		searcher = s;
	//	}
	//
	//	klee::Searcher* getSearcher(){
	//		return searcher;
	//	}


private:
	static bool instanceFlag;
	static StatisticsForGA *singleton;

	//runtime instructions
	//	std::map<int, set<llvm::Instruction*> > perKernelTrueBranches;
	//	std::map<int, set<llvm::Instruction*> > perKernelFalseBranches;
	std::set<llvm::Instruction*> trueBranches;
	std::set<llvm::Instruction*> falseBranches;
	//	std::map<int, int> perPathExecutedInstructions;
	//static instructions
	std::map<std::string, vector<llvm::Instruction*> > kernelInstructions;
	//	std::map<std::string, set<llvm::Instruction*> > perKernelStaticBranches; //only the ones with 2 successors
	std::set<llvm::Instruction*> staticBranches;
	std::map<int , std::string> kernelName;

	//searcher
	//	klee::Searcher *searcher;
};

#endif
